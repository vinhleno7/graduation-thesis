import React from "react";
import { useParams } from "react-router-dom";
import NotFound from "../components/NotFound";
import { useSelector } from "react-redux";

const generatePage = (pageName) => {
  const component = () => require(`../pages/${pageName}`).default;

  try {
    return React.createElement(component());
  } catch (e) {
    return <NotFound />;
  }
};

const PageRender = () => {
  const { page, id } = useParams();
  const { auth } = useSelector((state) => state);
  let pageName = "";

  // if (page === "verify") {
  //   if (id) {
  //     pageName = `${page}/[id]`;
  //   } else {
  //     pageName = `${page}`;
  //   }
  // }

  if (auth.token && page !== "admin") {
    if (id) {
      pageName = `${page}/[id]`;
    } else {
      pageName = `${page}`;
    }
  }

  if (auth.user && auth.user.role === "admin") {
    if (auth.token) {
      if (id) {
        pageName = `${page}/[id]`;
      } else {
        pageName = `${page}`;
      }
    }
  }

  return generatePage(pageName);
};

export default PageRender;
