import React from "react";
import LeftSide from "../../components/message/LeftSide";
import Direct from "../../images/icons8-twitter-circled.svg";

const Message = () => {
  document.title = "Tin nhắn  •  LV Social";
  return (
    <div className="message d-flex">
      <div className="col-md-4 border-right px-0">
        <LeftSide />
      </div>

      <div className="col-md-8 px-0 right_mess">
        <div
          className="d-flex justify-content-center
                align-items-center flex-column h-100"
        >
          <img src={Direct} alt="Direct" />
          <h4>Tin nhắn của bạn</h4>
          <p style={{ color: "rgba(var(--f52,142,142,142),1)" }}>
            Gửi hình ảnh, tin nhắn riêng tư cho bạn bè.
          </p>
        </div>
      </div>
    </div>
  );
};

export default Message;
