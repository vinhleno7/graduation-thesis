import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { verify } from "../redux/actions/authAction";
import BG from "../images/phone-frame.png";
import Slide1 from "../images/slide (1).png";
import Slide2 from "../images/slide (2).png";
import Slide3 from "../images/slide (3).png";
import Logo from "../images/lv social logo .png";
import { useParams } from "react-router-dom";

const Verify = () => {
  const { auth } = useSelector((state) => state);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    setTimeout(() => {
      if (auth.token) history.push("/");
    }, 2000);
  }, [auth.token, history]);

  let slideContent = document.getElementById("slide-content");
  let sliceIndex = 0;
  const slide = () => {
    let sliceItems = slideContent ? slideContent.querySelectorAll("img") : "";
    sliceItems && sliceItems.forEach((e) => e && e.classList.remove("active"));
    sliceIndex = sliceIndex + 1 === sliceItems.length ? 0 : sliceIndex + 1;
    if (sliceItems[sliceIndex]) sliceItems[sliceIndex].classList.add("active");
  };

  setInterval(slide, 4000);

  const { id } = useParams();

  useEffect(() => {
    if (id) {
      dispatch(verify(id));
    }
  }, [id]);

  return (
    <div className="d-flex justify-content-center align-items-center w-100">
      <div className="slide-container" style={{ background: `${BG}` }}>
        <div className="slide-content" id="slide-content">
          <img src={Slide1} alt="slide image" className="active" />
          <img src={Slide2} alt="slide image" />
          <img src={Slide3} alt="slide image" />
        </div>
      </div>
      <div className="auth_page">
        <div className="d-flex justify-content-center w-100 mb-3">
          <img src={Logo} alt="Logo" style={{ width: "80%" }} />
          <h3>Tài khoản chưa xác minh.</h3>
        </div>
      </div>
    </div>
  );
};

export default Verify;
