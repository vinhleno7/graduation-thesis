import React from "react";
import Sidebar from "../../components/admin/sidebar";
const Admin = () => {
  document.title = "Trang quản lý  •  LV Social";
  return (
    <div className="admin mt-1">
      <h1
        style={{ borderLeft: "3px solid rgb(8, 141, 205)", color: "#da0404" }}
      >
        Trang quản lý
      </h1>
      <Sidebar />
    </div>
  );
};

export default Admin;
