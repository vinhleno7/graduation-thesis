import React, { useState, useEffect } from "react";

import Info from "../../components/profile/Info";
import Posts from "../../components/profile/Posts";
import Saved from "../../components/profile/Saved";
import Album from "../../components/profile/Album";

import { useSelector, useDispatch } from "react-redux";
import LoadIcon from "../../images/loading.gif";
import { getProfileUsers } from "../../redux/actions/profileAction";
import { useParams } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTableCells,
  faBookmark,
  faImages
} from "@fortawesome/free-solid-svg-icons";

const Profile = () => {
  const { profile, auth } = useSelector((state) => state);
  const dispatch = useDispatch();

  const { id } = useParams();

  const [tab, setTab] = useState("post");

  useEffect(() => {
    if (profile.ids.every((item) => item !== id)) {
      dispatch(getProfileUsers({ id, auth }));
    }
  }, [id, auth, dispatch, profile.ids]);

  profile.users.forEach((item) => {
    if (item._id === id) {
      document.title = `${item.fullname} (@${item.username}) • Ảnh và video trên LV Social`;
    }
  });

  return (
    <div className="profile">
      <Info auth={auth} profile={profile} dispatch={dispatch} id={id} />
      <div className="profile_tab">
        <button
          className={tab === "post" ? "active" : ""}
          onClick={() => setTab("post")}
        >
          <FontAwesomeIcon icon={faTableCells} style={{ marginRight: "2px" }} />{" "}
          Bài viết
        </button>
        {auth.user._id === id && (
          <button
            className={tab === "save" ? "active" : ""}
            onClick={() => setTab("save")}
          >
            <FontAwesomeIcon icon={faBookmark} style={{ marginRight: "6px" }} />
            Bài lưu
          </button>
        )}
        <button
          className={tab === "album" ? "active" : ""}
          onClick={() => setTab("album")}
        >
          <FontAwesomeIcon icon={faImages} style={{ marginRight: "6px" }} />
          Album
        </button>
      </div>

      {profile.loading ? (
        <img
          className="d-block mx-auto "
          style={{ width: "50px", height: "50px", marginTop: "50px" }}
          src={LoadIcon}
          alt="loading"
        />
      ) : (
        <>
          {tab === "save" && <Saved auth={auth} dispatch={dispatch} />}
          {tab === "post" && (
            <Posts auth={auth} profile={profile} dispatch={dispatch} id={id} />
          )}

          {tab === "album" && (
            <Album auth={auth} profile={profile} dispatch={dispatch} id={id} />
          )}
        </>
      )}
    </div>
  );
};

export default Profile;
