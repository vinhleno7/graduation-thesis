import React, { useState, useEffect } from "react";

import { Link, useHistory } from "react-router-dom";
import {
  login,
  forgot_password,
  reset_password
} from "../redux/actions/authAction";
import { useDispatch, useSelector } from "react-redux";
import BG from "../images/phone-frame.png";
import Slide1 from "../images/slide (1).png";
import Slide2 from "../images/slide (2).png";
import Slide3 from "../images/slide (3).png";
import Logo from "../images/lv social logo .png";
import { GLOBALTYPES } from "../redux/actions/globalTypes";

const Login = () => {
  const initialState = { email: "", password: "" };
  const [userData, setUserData] = useState(initialState);
  const { email, password } = userData;
  const [typeCfPass, setTypeCfPass] = useState(false);

  const [typePass, setTypePass] = useState(false);
  const [newPass, setNewPass] = useState("");
  const [newConformPass, setNewConformPass] = useState("");
  const { auth, alert } = useSelector((state) => state);
  const dispatch = useDispatch();
  const history = useHistory();

  const [openForgotPassword, setOpenForgotPassword] = useState(false);

  useEffect(() => {
    if (auth.token) history.push("/");
  }, [auth.token, history]);

  const handleChangeInput = (e) => {
    const { name, value } = e.target;
    setUserData({ ...userData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(login(userData));
  };
  let slideContent = document.getElementById("slide-content");
  let sliceIndex = 0;
  const slide = () => {
    let sliceItems = slideContent ? slideContent.querySelectorAll("img") : "";
    sliceItems && sliceItems.forEach((e) => e && e.classList.remove("active"));
    sliceIndex = sliceIndex + 1 === sliceItems.length ? 0 : sliceIndex + 1;
    if (sliceItems[sliceIndex]) sliceItems[sliceIndex].classList.add("active");
  };

  setInterval(slide, 4000);

  function validateEmail(email) {
    // eslint-disable-next-line
    const re =
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  const [emailForgot, setEmailForgot] = useState("");

  const handleOpenForgotModel = (e) => {
    e.preventDefault();
    setOpenForgotPassword(true);
  };

  const handleChangeEmailForgot = (e) => {
    const { value } = e.target;

    setEmailForgot(value);
  };

  const handleForgotPassword = (e) => {
    e.preventDefault();
    dispatch(forgot_password(emailForgot));
  };

  const [openReset, setOpenReset] = useState(false);

  useEffect(() => {
    setOpenReset(true);
    if (!auth.id) setOpenReset(false);
  }, [auth.id]);

  const handleChangeNewPass = (e) => {
    const { value } = e.target;

    setNewPass(value);
  };

  const handleChangeConformNewPass = (e) => {
    const { value } = e.target;

    setNewConformPass(value);
  };

  const handleResetPassWord = (e) => {
    e.preventDefault();
    const token = auth?.id;
    dispatch(reset_password({ newPass, newConformPass, token }));

    setTimeout(() => {
      setOpenReset(false);
    }, 2000);
    dispatch({ type: GLOBALTYPES.RESET, payload: false });
  };
  return (
    <div className="d-flex justify-content-center align-items-center w-100">
      <div className="slide-container" style={{ background: `${BG}` }}>
        <div className="slide-content" id="slide-content">
          <img src={Slide1} alt="slide image" className="active" />
          <img src={Slide2} alt="slide image" />
          <img src={Slide3} alt="slide image" />
        </div>
      </div>
      {openForgotPassword ? (
        <div className="auth_page">
          <form>
            <div className="d-flex justify-content-center w-100 mb-3">
              <img src={Logo} alt="Logo" style={{ width: "80%" }} />
            </div>
            <h3>Quên mật khẩu</h3>
            <div className="form-group">
              <label htmlFor="exampleInputEmaill">Địa chỉ email</label>
              <input
                type="email"
                className="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                onChange={handleChangeEmailForgot}
                value={emailForgot}
                name="emailForgot"
              />
              <small className="form-text text-danger">
                {emailForgot.length > 1 && !validateEmail(emailForgot)
                  ? "Email is wrong format"
                  : ""}
              </small>
              <small id="emailHelp" className="form-text text-muted">
                Nhập vào địa chỉ email của bạn để tạo lại mật khẩu.
              </small>
            </div>

            {emailForgot ? (
              <button
                onClick={handleForgotPassword}
                disabled={email && password ? false : true}
                className="myCustomButton"
                style={{ "--clr": "#1e9bff", width: "100%" }}
              >
                {" "}
                <span>Gửi mail</span>
                <i></i>
              </button>
            ) : (
              <div
                className="myCustomButton text-center"
                style={{ "--clr": "red", width: "100%" }}
              >
                {" "}
                <span>Vui lòng điền địa chỉ email</span>
                <i></i>
              </div>
            )}
            <small
              id="emailHelp"
              onClick={() => setOpenForgotPassword(false)}
              className="form-text text-right text-info"
              style={{ fontWeight: "bolder", cursor: "pointer" }}
            >
              Quay lại đăng nhập
            </small>
          </form>
        </div>
      ) : openReset ? (
        <div className="auth_page">
          <form>
            <div className="d-flex justify-content-center w-100 mb-3">
              <img src={Logo} alt="Logo" style={{ width: "80%" }} />
            </div>
            <h3>Tạo lại mật khẩu</h3>

            <div className="form-group">
              <label htmlFor="exampleInputPassword1">Mật khẩu mới</label>

              <div className="pass">
                <input
                  type={typePass ? "text" : "password"}
                  className="form-control"
                  id="exampleInputPassword1"
                  onChange={handleChangeNewPass}
                  value={newPass}
                  name="newPass"
                  style={{ background: `${alert.password ? "#fd2d6a14" : ""}` }}
                />

                <small onClick={() => setTypePass(!typePass)}>
                  {typePass ? "Ẩn" : "Hiện"}
                </small>
              </div>

              <small className="form-text text-danger">
                {newPass.length > 1 && newPass.length < 6
                  ? "New Password must be at least 6 characters."
                  : ""}
              </small>
            </div>

            <div className="form-group">
              <label htmlFor="newConformPass">Xác nhận mật khẩu</label>

              <div className="pass">
                <input
                  type={typeCfPass ? "text" : "password"}
                  className="form-control"
                  id="newConformPass"
                  onChange={handleChangeConformNewPass}
                  value={newConformPass}
                  name="newConformPass"
                  style={{
                    background: `${alert.newConformPass ? "#fd2d6a14" : ""}`
                  }}
                />

                <small onClick={() => setTypeCfPass(!typeCfPass)}>
                  {typeCfPass ? "Ẩn " : "Hiện"}
                </small>
              </div>

              <small className="form-text text-danger">
                {newConformPass.length > 1 && newConformPass.length < 6
                  ? "Conform Password must be at least 6 characters."
                  : newPass.length > 6 &&
                    newConformPass.length > 6 &&
                    newConformPass !== newPass
                  ? "New Password and conform password is not match."
                  : ""}
              </small>
            </div>
            {newPass && newConformPass ? (
              <button
                onClick={handleResetPassWord}
                className="myCustomButton"
                style={{ "--clr": "#1e9bff", width: "100%" }}
              >
                {" "}
                <span>Tạo lại mật khẩu</span>
                <i></i>
              </button>
            ) : (
              <div
                className="myCustomButton text-center"
                style={{ "--clr": "red", width: "100%" }}
              >
                {" "}
                <span>Vui lòng điền thông tin bên trên</span>
                <i></i>
              </div>
            )}
            <small
              id="emailHelp"
              onClick={() => setOpenReset(false)}
              className="form-text text-right text-info"
              style={{ fontWeight: "bolder", cursor: "pointer" }}
            >
              Quay lại đăng nhập
            </small>
          </form>
        </div>
      ) : (
        <div className="auth_page">
          <form onSubmit={handleSubmit}>
            <div className="d-flex justify-content-center w-100 mb-3">
              <img src={Logo} alt="Logo" style={{ width: "80%" }} />
            </div>
            <h3>Đăng nhập</h3>
            <div className="form-group">
              <label htmlFor="exampleInputEmaill">Địa chỉ email</label>
              <input
                type="email"
                className="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                onChange={handleChangeInput}
                value={email}
                name="email"
              />

              <small id="emailHelp" className="form-text text-muted">
                Chúng tôi sẽ không bao giờ chia sẻ địa chỉ của bạn với người
                khác.
              </small>
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputPassword1">Mật khẩu</label>
              <div className="pass">
                <input
                  type={typePass ? "text" : "password"}
                  className="form-control"
                  id="exampleInputPassword1"
                  onChange={handleChangeInput}
                  value={password}
                  name="password"
                />

                <small onClick={() => setTypePass(!typePass)}>
                  {typePass ? "Ẩn" : "Hiện"}
                </small>
              </div>
            </div>
            <div className="form-group form-check d-flex justify-content-between">
              <div>
                <input
                  type="checkbox"
                  className="form-check-input"
                  id="exampleCheck1"
                />
                <label className="form-check-label" htmlFor="exampleCheck1">
                  Nhớ tài khoản
                </label>
              </div>

              <div>
                <label
                  onClick={(e) => handleOpenForgotModel(e)}
                  style={{ color: "#dc143c", cursor: "pointer" }}
                >
                  Quên mật khẩu
                </label>
              </div>
            </div>
            {/* <button
          type="submit"
          className="btn btn-dark w-100"
          disabled={email && password ? false : true}
        >
          Login
        </button> */}

            {email && password ? (
              <button
                type="submit"
                disabled={email && password ? false : true}
                className="myCustomButton"
                style={{ "--clr": "#1e9bff", width: "100%" }}
              >
                {" "}
                <span>Đăng nhập</span>
                <i></i>
              </button>
            ) : (
              <div
                className="myCustomButton text-center"
                style={{ "--clr": "red", width: "100%" }}
              >
                {" "}
                <span>Làm ơn điền thông tin</span>
                <i></i>
              </div>
            )}

            <p className="my-2">
              Bạn không có tài khoản ư?{" "}
              <Link to="/register" style={{ color: "crimson" }}>
                Đăng ký ngay nào
              </Link>
            </p>
          </form>
        </div>
      )}
    </div>
  );
};

export default Login;
