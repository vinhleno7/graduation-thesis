import React, { useEffect } from "react";
import Post from "../components/home/Post";
import { useSelector } from "react-redux";
import LoadIcon from "../images/loading.gif";
import RightSideBar from "../components/home/RightSideBar";
import Stories from "../components/Story/Stories";

let scroll = 0;

const Home = () => {
  const { homePosts } = useSelector((state) => state);
  document.title = "Trang chủ  •  LV Social";
  window.addEventListener("scroll", () => {
    if (window.location.pathname === "/") {
      scroll = window.pageYOffset;
      return scroll;
    }
  });

  useEffect(() => {
    setTimeout(() => {
      window.scrollTo({ top: scroll, behavior: "smooth" });
    }, 100);
  }, []);

  return (
    <div className="home row mx-0" style={{ marginBottom: "68px" }}>
      <div className="col-md-7">
        <Stories />
        {homePosts.loading ? (
          <img
            src={LoadIcon}
            style={{ width: "50px", height: "50px" }}
            alt="loading"
            className="d-block mx-auto"
          />
        ) : homePosts.result === 0 && homePosts.posts.length === 0 ? (
          <p
            className="text-center "
            style={{ fontWeight: 300, fontSize: "2rem" }}
          >
            {" "}
            Bạn chưa có bài viết nào cả
          </p>
        ) : (
          <Post />
        )}
      </div>
      <div className="col-md-4 position-relative myRightSuggestion">
        <div className="position-fixed suggestion-mb">
          <RightSideBar />
        </div>
      </div>
    </div>
  );
};

export default Home;
