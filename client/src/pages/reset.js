import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import { GLOBALTYPES } from "../redux/actions/globalTypes";
import { toast } from "react-toastify";

toast.configure();

const Reset = () => {
  const { id } = useParams();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({ type: GLOBALTYPES.RESET, payload: { id } });
    toast.success("Làm ơn điền những thông tin này để tạo lại mật khẩu !");
  }, [id]);
  return (
    <div>
      <h1>Điền mật khẩu mới của bạn.</h1>
    </div>
  );
};

export default Reset;
