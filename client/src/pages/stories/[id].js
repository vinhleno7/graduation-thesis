import { useParams } from "react-router-dom";
import React, { useEffect, useState } from "react";
import Stories from "stories-react";
import "stories-react/dist/index.css";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { getStories } from "../../redux/actions/storyAction";
import { deleteStory } from "../../redux/actions/storyAction";
import moment from "moment";
import "moment/locale/vi";

import StoryList from "../../components/Story/Stories";
import { likeStory } from "../../redux/actions/storyAction";
import { unLikeStory } from "../../redux/actions/storyAction";
import { onConfirm } from "react-confirm-pro";

moment.locale("vi");

const Head = (props) => {
  const { auth, socket } = useSelector((state) => state);
  const dispatch = useDispatch();
  const [isLike, setIsLike] = useState(false);

  const CustomUIDelete = ({ onSubmit, onCancel }) => (
    <div className="confirm">
      <h1>
        <strong>Delete Story ?</strong>
      </h1>
      <p>
        Are you sure you want to <strong>delete</strong> this story?
      </p>
      <button onClick={onCancel}>Cancel</button>
      <button onClick={onSubmit}>Delete</button>
    </div>
  );

  const handleDeleteStory = (id) => {
    onConfirm({
      onSubmit: () => {
        dispatch(deleteStory({ id, auth, socket }));
      },
      onCancel: () => {
        // alert("Cancel");
      },
      customUI: CustomUIDelete,
      className: "my-custom-ui-container"
    });
  };

  const handleLikeStory = (id) => {
    setIsLike(true);
    dispatch(likeStory({ id, auth, socket }));
  };

  const handleUnLikeStory = (id) => {
    setIsLike(false);
    dispatch(unLikeStory({ id, auth }));
  };

  useEffect(() => {
    setIsLike(false);
    if (props.arrayLikes.find((item) => item === auth.user._id)) {
      setIsLike(true);
    }
  }, [props.arrayLikes, auth.user._id]);
  return (
    <div
      style={{
        position: "relative",
        padding: "10px 15px",
        display: "flex",
        alignItems: "center",
        alignContent: "center",
        // backgroundColor: "#9d9d9d3b",
        width: "100%"
      }}
    >
      <Link style={{ zIndex: 2 }} to={`/profile/${props.userId}`}>
        <div>
          <img
            src={props.url}
            alt="Avatar"
            style={{
              verticalAlign: "middle",
              width: "40px",
              height: "40px",
              borderRadius: "50%"
            }}
          />
        </div>
      </Link>
      <Link style={{ zIndex: 2 }} to={`/profile/${props.userId}`}>
        <div
          style={{
            color: "white",
            fontWeight: "500",
            fontSize: "14px"
            // marginLeft: "12px"
          }}
        >
          <p
            style={{
              color: "white",
              fontWeight: "500",
              fontSize: "14px",
              margin: 0,
              marginLeft: "12px"
            }}
          >
            {props.name}
          </p>
          <p
            style={{
              color: "white",
              fontWeight: "400",
              fontSize: "8px",
              margin: 0,
              marginLeft: "12px"
            }}
          >
            {`${props.time}`}
          </p>
        </div>
      </Link>
      {auth.user._id === props.userId && (
        <button
          type="button"
          style={{ cursor: "pointer", zIndex: 2 }}
          className="ml-5 btn btn-danger"
          onClick={() => handleDeleteStory(props.id)}
        >
          Xóa
        </button>
      )}
      {!isLike ? (
        <div className="stage" onClick={() => handleLikeStory(props.id)}>
          {" "}
          <div className="heart"></div>
        </div>
      ) : (
        <div className="stage " onClick={() => handleUnLikeStory(props.id)}>
          {" "}
          <div className="heart story_liked"></div>
        </div>
      )}
    </div>
  );
};

const Story = () => {
  document.title = "Câu chuyện  •  LV Social";
  const { id } = useParams();
  const { story, auth } = useSelector((state) => state);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getStories(auth.token));
  }, [dispatch, auth.token]);

  const myFunction = (v) => {
    let myArr = [];
    for (const item of v) {
      item.images.forEach((img) => {
        const myArrStoryObj = {
          type:
            img.url.includes("mp4") ||
            img.url.includes("mp3") ||
            img.url.includes("mov")
              ? "video"
              : "image",
          duration: 6000,
          url: img.url,
          header: (
            <Head
              id={item._id}
              arrayLikes={item.likes}
              name={
                story.stories.filter((person) => person._id === item.user)[0]
                  .username
              }
              userId={
                story.stories.filter((person) => person._id === item.user)[0]
                  ._id
              }
              url={
                story.stories.filter((person) => person._id === item.user)[0]
                  .avatar
              }
              time={moment(item.createdAt).fromNow()}
            />
          )
        };
        myArr.push(myArrStoryObj);
      });
    }
    return myArr;
  };

  return (
    <div
      id="story_container"
      className="d-flex justify-content-center align-items-center"
      style={{
        overflow: "hidden",
        width: "100%",
        height: "100vh",
        position: "absolute",
        top: "0",
        left: "0",
        background: "black"
      }}
    >
      <div className="d-flex flex-column align-items-center justify-content-center"></div>
      {story.stories.length > 0 && (
        <div
          className="d-flex justify-content-center"
          style={{
            width: "400px",
            height: "600px",
            margin: "50px 0"
            // overflow: "scroll"
          }}
        >
          <div style={{ margin: " 0 0 0 0" }}>
            {myFunction(
              story.stories.filter((person) => person.username === id)[0]
                .stories
            ).length > 0 ? (
              <Stories
                width="400px"
                height="600px"
                pauseStoryWhenInActiveWindow={true}
                stories={myFunction(
                  story.stories.filter((person) => person.username === id)[0]
                    .stories
                )}
              />
            ) : (
              <h1>No stories</h1>
            )}

            <StoryList />
          </div>
        </div>
      )}
    </div>
  );
};

export default Story;
