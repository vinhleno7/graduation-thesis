import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  getDiscoverPosts,
  DISCOVER_TYPES
} from "../redux/actions/discoverAction";
import LoadIcon from "../images/loading.gif";
import PostThumb from "../components/PostThumb";
import LoadMoreBtn from "../components/LoadMoreBtn";
import { getDataAPI } from "../utils/fetchData";
import Search from "../components/header/Search";
const Discover = () => {
  const { auth, discover } = useSelector((state) => state);

  const dispatch = useDispatch();

  const [load, setLoad] = useState(false);

  useEffect(() => {
    if (!discover.firstLoad) {
      dispatch(getDiscoverPosts(auth.token));
    }
  }, [dispatch, auth.token, discover.firstLoad]);

  const handleLoadMore = async () => {
    setLoad(true);
    const res = await getDataAPI(
      `post_discover?limit=${discover.page * 9}`,
      auth.token
    );

    dispatch({
      type: DISCOVER_TYPES.GET_POSTS,
      payload: { ...res.data, page: discover.page + 1 }
    });
    setLoad(false);
  };
  document.title = "Khám phá  •  LV Social";
  return (
    <div className="myDiscover">
      <Search />
      <p>Khám phá thế giới LV-Social</p>
      {discover.loading ? (
        <img
          src={LoadIcon}
          style={{ width: "50px", height: "50px" }}
          alt="loading"
          className="d-block mx-auto my-4"
        />
      ) : (
        <PostThumb posts={discover.posts} result={discover.result} />
      )}

      {load && (
        <img
          src={LoadIcon}
          style={{ width: "50px", height: "50px" }}
          alt="loading"
          className="d-block mx-auto"
        />
      )}

      {!discover.loading && (
        <LoadMoreBtn
          result={discover.result}
          page={discover.page}
          load={load}
          handleLoadMore={handleLoadMore}
        />
      )}
    </div>
  );
};

export default Discover;
