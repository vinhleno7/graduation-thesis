import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory, Link } from "react-router-dom";
import { register } from "../redux/actions/authAction";
import BG from "../images/phone-frame.png";
import Slide1 from "../images/slide (1).png";
import Slide2 from "../images/slide (2).png";
import Slide3 from "../images/slide (3).png";
import Logo from "../images/lv social logo .png";

import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
  borderRadius: 5,
  outline: "none"
};

const Register = () => {
  const { auth, alert } = useSelector((state) => state);
  const dispatch = useDispatch();
  const history = useHistory();

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const initialState = {
    fullname: "",
    username: "",
    email: "",
    password: "",
    cf_password: "",
    gender: "male"
  };
  const [userData, setUserData] = useState(initialState);
  const { fullname, username, email, password, cf_password } = userData;

  const [typePass, setTypePass] = useState(false);
  const [typeCfPass, setTypeCfPass] = useState(false);

  // useEffect(() => {
  //   if (auth.token) history.push("/");
  // }, [auth.token, history]);

  const handleChangeInput = (e) => {
    const { name, value } = e.target;
    setUserData({ ...userData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(register(userData));

    setTimeout(() => {
      setOpen(true);
    }, 2000);
  };

  let slideContent = document.getElementById("slide-content");
  let sliceIndex = 0;
  const slide = () => {
    let sliceItems = slideContent ? slideContent.querySelectorAll("img") : "";
    sliceItems && sliceItems.forEach((e) => e && e.classList.remove("active"));
    sliceIndex = sliceIndex + 1 === sliceItems.length ? 0 : sliceIndex + 1;
    if (sliceItems[sliceIndex]) sliceItems[sliceIndex].classList.add("active");
  };

  setInterval(slide, 4000);

  return (
    <div className="d-flex justify-content-center align-items-center w-100">
      <div className="slide-container" style={{ background: `${BG}` }}>
        <div className="slide-content" id="slide-content">
          <img src={Slide1} alt="slide image" className="active" />
          <img src={Slide2} alt="slide image" />
          <img src={Slide3} alt="slide image" />
        </div>
      </div>
      <div className="auth_page">
        <form onSubmit={handleSubmit}>
          <div className="d-flex justify-content-center w-100 mb-3">
            <img src={Logo} alt="Logo" style={{ width: "80%" }} />
          </div>
          <h3>Đăng ký</h3>
          <div className="form-group">
            <label htmlFor="fullname">Họ và tên</label>
            <input
              type="text"
              className="form-control"
              id="fullname"
              name="fullname"
              onChange={handleChangeInput}
              value={fullname}
              style={{ background: `${alert.fullname ? "#fd2d6a14" : ""}` }}
            />

            <small className="form-text text-danger">
              {alert.fullname ? alert.fullname : ""}
            </small>
          </div>

          <div className="form-group">
            <label htmlFor="username">Tên tài khoản</label>
            <input
              type="text"
              className="form-control"
              id="username"
              name="username"
              onChange={handleChangeInput}
              value={username.toLowerCase().replace(/ /g, "")}
              style={{ background: `${alert.username ? "#fd2d6a14" : ""}` }}
            />

            <small className="form-text text-danger">
              {alert.username ? alert.username : ""}
            </small>
          </div>

          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Địa chỉ email</label>
            <input
              type="email"
              className="form-control"
              id="exampleInputEmail1"
              name="email"
              onChange={handleChangeInput}
              value={email}
              style={{ background: `${alert.email ? "#fd2d6a14" : ""}` }}
            />

            <small className="form-text text-danger">
              {alert.email ? alert.email : ""}
            </small>
          </div>

          <div className="form-group">
            <label htmlFor="exampleInputPassword1">Mật khẩu</label>

            <div className="pass">
              <input
                type={typePass ? "text" : "password"}
                className="form-control"
                id="exampleInputPassword1"
                onChange={handleChangeInput}
                value={password}
                name="password"
                style={{ background: `${alert.password ? "#fd2d6a14" : ""}` }}
              />

              <small onClick={() => setTypePass(!typePass)}>
                {typePass ? "Ẩn" : "Hiện"}
              </small>
            </div>

            <small className="form-text text-danger">
              {alert.password ? alert.password : ""}
            </small>
          </div>

          <div className="form-group">
            <label htmlFor="cf_password">Xác nhận mật khẩu</label>

            <div className="pass">
              <input
                type={typeCfPass ? "text" : "password"}
                className="form-control"
                id="cf_password"
                onChange={handleChangeInput}
                value={cf_password}
                name="cf_password"
                style={{
                  background: `${alert.cf_password ? "#fd2d6a14" : ""}`
                }}
              />

              <small onClick={() => setTypeCfPass(!typeCfPass)}>
                {typeCfPass ? "Ẩn" : "Hiện"}
              </small>
            </div>

            <small className="form-text text-danger">
              {alert.cf_password ? alert.cf_password : ""}
            </small>
          </div>

          <div className="row justify-content-between mx-0 mb-1">
            <label htmlFor="male">
              Nam:{" "}
              <input
                type="radio"
                id="male"
                name="gender"
                value="male"
                defaultChecked
                onChange={handleChangeInput}
              />
            </label>

            <label htmlFor="female">
              Nữ:{" "}
              <input
                type="radio"
                id="female"
                name="gender"
                value="female"
                onChange={handleChangeInput}
              />
            </label>

            <label htmlFor="other">
              Giới tính thứ 3:{" "}
              <input
                type="radio"
                id="other"
                name="gender"
                value="other"
                onChange={handleChangeInput}
              />
            </label>
          </div>

          {/* <button type="submit" className="btn btn-dark w-100">
          Register
        </button> */}
          <button
            type="submit"
            className="myCustomButton"
            style={{ "--clr": "#1e9bff", width: "100%" }}
          >
            {" "}
            <span>Đăng ký</span>
            <i></i>
          </button>

          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500
            }}
          >
            <Fade in={open}>
              <Box sx={style}>
                <div>
                  <h2 style={{ marginBottom: "20px" }}>
                    Bước 2: Xác minh địa chỉ mail
                  </h2>
                  <a
                    href={`https://mail.google.com/`}
                    target="_blank"
                    rel="noreferrer"
                    className="myCustomButton"
                    style={{ "--clr": "#1e9bff", width: "100%" }}
                  >
                    {" "}
                    <span>{auth.msg}</span>
                    <i></i>
                  </a>
                </div>
              </Box>
            </Fade>
          </Modal>

          <p className="my-2">
            Bạn đã có tài khoản rồi ư ?{" "}
            <Link to="/" style={{ color: "crimson" }}>
              Đăng nhập thôi nào
            </Link>
          </p>
        </form>
      </div>
    </div>
  );
};

export default Register;
