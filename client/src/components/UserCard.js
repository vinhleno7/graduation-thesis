import React, { Fragment } from "react";
import Avatar from "./Avatar";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

const UserCard = ({
  children,
  user,
  border,
  handleClose,
  setShowFollowers,
  setShowFollowing,
  size,
  msg
}) => {
  const { auth } = useSelector((state) => state);
  const handleCloseAll = () => {
    if (handleClose) handleClose();
    if (setShowFollowers) setShowFollowers(false);
    if (setShowFollowing) setShowFollowing(false);
  };

  const showMsg = (user) => {
    return (
      <>
        <div>
          {user.sender === auth.user._id
            ? `Bạn: ${
                user.text.length < 45
                  ? user.text
                  : user.text.slice(0, 45) + "..."
              }`
            : `${user.username}: ${
                user.text.length < 45
                  ? user.text
                  : user.text.slice(0, 45) + "..."
              }`}
        </div>
        {user.media?.length > 0 && (
          <div>
            {user.media?.length} <i className="fas fa-image" />
          </div>
        )}

        {user.call && (
          <span className="material-icons ml-2">
            {user.call.times === 0
              ? user.call.video
                ? "videocam_off"
                : "phone_disabled"
              : user.call.video
              ? "video_camera_front"
              : "call"}
          </span>
        )}
      </>
    );
  };

  return (
    <div
      className={`d-flex p-2 align-items-center justify-content-between w-100 ${border}`}
    >
      <div>
        <Link
          to={`/profile/${user._id}`}
          onClick={handleCloseAll}
          className="d-flex align-items-center"
        >
          <Avatar src={user.avatar} size={`${size} ? ${size} : "big-avatar"`} />

          <div
            className="infoUser ml-2"
            style={{ transform: "translateY(-2px)" }}
          >
            <span className="d-block" style={{ fontWeight: 500 }}>
              {user?.username?.length > 16
                ? user.username.substring(0, 15) + "..."
                : user.username}
            </span>
            {auth.user._id === user._id && (
              <small style={{ opacity: 0.7 }}>{user.fullname}</small>
            )}

            <small style={{ opacity: 0.7, display: "flex", width: "150px" }}>
              {user?.sameHobbies ? (
                <>
                  <span>
                    {`Có chung ${user.sameHobbies.length} sở thích `}
                    {user.sameHobbies.map((hb) => (
                      <Fragment key={hb.name}>
                        <span style={{ color: "blue", marginLeft: "2px" }}>
                          {hb.name}{" "}
                        </span>
                        {user.sameHobbies.length > 1 && ","}
                      </Fragment>
                    ))}
                  </span>
                </>
              ) : (
                ""
              )}
            </small>

            <small style={{ opacity: 0.7, display: "flex" }}>
              {msg && showMsg(user)}
            </small>
          </div>
        </Link>
      </div>
      {children}
    </div>
  );
};

export default UserCard;
