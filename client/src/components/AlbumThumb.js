import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import React, { useState, useEffect } from "react";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import ImageGallery from "react-image-gallery";
import HeadAlbum from "./HeadAlbum";
import "react-image-gallery/styles/css/image-gallery.css";
import LoadIcon from "../images/loading.gif";
import { GLOBALTYPES } from "../redux/actions/globalTypes";
import LikeButton from "./LikeButton";
import { likeAlbum, unLikeAlbum } from "../redux/actions/albumAction";
import Likes from "./profile/Likes";

const AlbumThumb = ({ albums, result }) => {
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 1000,
    height: "90vh",
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4
  };
  const { status, theme, socket, auth } = useSelector((state) => state);
  const dispatch = useDispatch();
  const [album, setAlbum] = useState({});
  const [imgList, setImgList] = useState([]);
  const [showLikes, setShowLikes] = useState(false);

  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);

  const handleClose = () => {
    setOpen(false);
  };

  const [isLike, setIsLike] = useState(false);
  const [loadLike, setLoadLike] = useState(false);

  useEffect(() => {
    if (status.onEditAlbum) handleClose();
  }, [status]);

  // ~LIKE
  useEffect(() => {
    if (album.likes?.find((like) => like._id === auth.user._id)) {
      setIsLike(true);
    } else {
      setIsLike(false);
    }
  }, [album.likes, auth.user._id]);

  const handleLike = async () => {
    if (loadLike) return;
    // setIsLike(true);
    setLoadLike(true);
    setAlbum({ ...album, likes: [...album.likes, auth.user] });
    await dispatch(likeAlbum({ album, auth, socket }));
    setLoadLike(false);
  };
  const handleUnLike = async () => {
    if (loadLike) return;
    // setIsLike(false);
    setLoadLike(true);
    setAlbum({
      ...album,
      likes: album.likes.filter((like) => like._id !== auth.user._id)
    });
    await dispatch(unLikeAlbum({ album, auth, socket }));

    setLoadLike(false);
  };

  const handleClickAlbum = (album) => {
    setAlbum(album);
    let myArr = [];
    let myObj = {};
    album.images.map((img) => {
      myObj = {
        original: img.url,
        thumbnail: img.url,
        description: album.name
      };
      myArr.push(myObj);
    });
    setImgList(myArr);

    handleOpen();
  };

  if (result === 0)
    return (
      <p className="text-center " style={{ fontWeight: 300, fontSize: "2rem" }}>
        {" "}
        Không có album nào cả.
      </p>
    );

  const handleOpenListLikes = () => {};
  return (
    <div className="post_thumb">
      {albums.length > 0 ? (
        albums.map((album) => (
          <div key={album._id} onClick={() => handleClickAlbum(album)}>
            <div className="post_thumb_display">
              {album.images[0].url.match(/video/i) ? (
                <video
                  controls
                  src={album.images[0].url}
                  alt={album.images[0].url}
                  style={{ filter: theme ? "invert(1)" : "invert(0)" }}
                />
              ) : (
                <img
                  src={album.images[0].url}
                  alt={album.images[0].url}
                  style={{ filter: theme ? "invert(1)" : "invert(0)" }}
                />
              )}

              <div className="post_thumb_menu">
                <i className="far fa-heart">{album.likes.length}</i>
                <i className="far fa-comment">{album.comments.length}</i>
              </div>
            </div>
          </div>
        ))
      ) : (
        <div className="d-flex justify-content-center">
          <img
            src={LoadIcon}
            style={{ width: "50px", height: "50px" }}
            alt="Loading..."
            className="loading"
          />
        </div>
      )}

      <Modal
        sx={{ height: "800px", overflow: "hidden" }}
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={open}>
          <Box sx={style} className="detail__album">
            <h1
              style={{ borderLeft: "3px solid #7272c4", margin: "0 0 16px 0" }}
            >
              {" "}
              Chi tiết album
            </h1>
            {/* <UserCard user={album.user} size="big-avatar" /> */}
            <HeadAlbum
              album={album}
              closeFun={handleClose}
              openFun={handleOpen}
            />
            <div
              className="d-flex align-items-center"
              style={{
                marginTop: "16px",
                borderLeft: "3px solid #ddd",
                paddingLeft: "8px"
              }}
            >
              <span>{album.name}</span>
            </div>

            <ImageGallery
              items={imgList}
              showBullets
              showIndex
              useWindowKeyDown
            />
            <div
              className="d-flex mx-0 align-items-center div__album"
              style={{ zIndex: "999", marginTop: "70px", fontSize: "2.5rem" }}
            >
              <LikeButton
                isLike={isLike}
                handleLike={handleLike}
                handleUnLike={handleUnLike}
              />
              <h6
                style={{ margin: "0 8px", cursor: "pointer" }}
                onClick={() => setShowLikes(true)}
              >
                {album.likes?.length} Lượt thích
              </h6>
            </div>
            {showLikes && (
              <Likes users={album.likes} setShowLikes={setShowLikes} />
            )}
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};

export default AlbumThumb;
