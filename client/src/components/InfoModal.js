import React from "react";
import Avatar from "../components/Avatar";
import FollowBtn from "../components/FollowBtn";
import { useSelector } from "react-redux";
const InfoModal = ({ user, top }) => {
  const { auth } = useSelector((state) => state);
  return (
    <div className={top ? `info_modal topAbsolute ` : `info_modal`}>
      <div
        key={user._id}
        className="info_container justify-content-center align-items-center ml-2"
      >
        <Avatar src={user.avatar} size="half-avatar" />

        <div className="info_content mt-2">
          <div className="info_content_title">
            <h6 className=" font-weight-bolder ">{user.username}</h6>{" "}
            <span
              style={{
                width: "120px",
                fontSize: "0.75rem"
              }}
            >
              {auth.user._id !== user._id && <FollowBtn user={user} />}
            </span>
          </div>
          <h6 className="m-0" style={{ width: "fit-content" }}>
            {user.fullname}
          </h6>
          <p className="text-danger m-0">{user.mobile}</p>
          <h6 className="m-0">{user.email}</h6>
          <a href={user.website} target="_blank" rel="noreferrer">
            {user.website}
          </a>
          {user.story && (
            <p>
              {user.story.length < 60
                ? user.story
                : user.story.slice(0, 60) + "..."}
            </p>
          )}
        </div>
      </div>
      <hr style={{ margin: "4px 0" }} />
      <div className="count_follow">
        <span className="mr-4">
          <span
            style={{
              textAlign: "center",
              display: "block",
              fontWeight: "bolder"
            }}
          >
            {user.followers.length}
          </span>{" "}
          Người theo dõi
        </span>

        <span className="ml-4">
          <span
            style={{
              textAlign: "center",
              display: "block",
              fontWeight: "bolder"
            }}
          >
            {user.following.length}
          </span>{" "}
          Đang theo dõi
        </span>
      </div>
    </div>
  );
};

export default InfoModal;
