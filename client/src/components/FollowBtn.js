import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  unfollow,
  getRequestFl,
  sendRequestFl,
  PROFILE_TYPES
} from "../redux/actions/profileAction";

const FollowBtn = ({ user }) => {
  const [followed, setFollowed] = useState(false);
  const [sendRequest, setSendRequest] = useState(false);

  const { auth, profile, socket } = useSelector((state) => state);
  const dispatch = useDispatch();

  const [load, setLoad] = useState(false);

  useEffect(() => {
    if (auth.user.following.find((item) => item._id === user._id)) {
      setFollowed(true);
      setSendRequest(false);
    }
    return () => {
      setFollowed(false);
    };
  }, [auth.user.following, user._id]);

  useEffect(() => {
    if (profile.requestFl.includes(user._id)) {
      setSendRequest(true);
    } else {
      setSendRequest(false);
    }
  }, [profile, auth]);

  useEffect(() => {
    if (!auth.user.following.find((item) => item._id === user._id)) {
      const userId = user._id;
      const token = auth.token;
      dispatch(getRequestFl({ userId, token }));
    }
  }, [auth.user.following, user._id]);

  const handleFollow = async () => {
    if (load) return;

    setLoad(true);
    dispatch({
      type: PROFILE_TYPES.GET_REQUEST_FL,
      payload: user._id
    });
    await dispatch(sendRequestFl({ userId: user._id, auth, socket }));
    setLoad(false);
  };

  const handleUnFollow = async () => {
    if (load) return;

    setFollowed(false);
    setLoad(true);

    await dispatch(unfollow({ users: profile.users, user, auth, socket }));
    setLoad(false);
  };
  return (
    <>
      {sendRequest ? (
        <button
          // onClick={handleRemoveFl}
          className="myCustomButton"
          style={{
            "--clr": "#1e9bff",
            width: "140px"
          }}
        >
          <span>Đã yêu cầu</span>

          <i></i>
        </button>
      ) : followed ? (
        <button
          onClick={handleUnFollow}
          className="myCustomButton"
          style={{
            "--clr": "#ff1867",
            width: "180px"
          }}
        >
          {" "}
          <span>Hủy theo dõi</span>
          <i></i>
        </button>
      ) : (
        <button
          onClick={handleFollow}
          className="myCustomButton"
          style={{
            "--clr": "#1e9bff",
            width: "140px"
          }}
        >
          {" "}
          <span>Theo dõi</span>
          <i></i>
        </button>
      )}
    </>
  );
};

export default FollowBtn;
