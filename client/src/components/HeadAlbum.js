import React, { useState } from "react";
import Avatar from "./Avatar";
import { Link, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import moment from "moment";
import InfoModal from "./InfoModal";
import { GLOBALTYPES } from "../redux/actions/globalTypes";
import { deleteAlbum } from "../redux/actions/albumAction";
import { BASE_URL } from "../utils/config";
import { onConfirm } from "react-confirm-pro";

const HeadAlbum = ({ album, closeFun, openFun }) => {
  const [infoModal, setInfoModal] = useState(false);
  const { auth, socket } = useSelector((state) => state);
  const dispatch = useDispatch();

  // const history = useHistory();

  const handleHover = () => {
    setTimeout(() => {
      setInfoModal(true);
    }, 400);
  };
  const handleUnHover = () => {
    setTimeout(() => {
      setInfoModal(false);
    }, 400);
  };

  const handleEditAlbum = () => {
    dispatch({
      type: GLOBALTYPES.EDIT_ALBUM,
      payload: { ...album, onEditAlbum: true }
    });
  };

  const CustomUIDelete = ({ onSubmit, onCancel }) => (
    <div className="confirm">
      <h1>
        <strong>Xóa album ?</strong>
      </h1>
      <p>
        Bạn có chắc muốn <strong>xóa</strong> album này không ?
      </p>
      <button onClick={onCancel}>Hủy bỏ</button>
      <button onClick={onSubmit}>Xóa</button>
    </div>
  );

  const onClickDelete = () => {
    closeFun();
    onConfirm({
      onSubmit: () => {
        dispatch(deleteAlbum({ album, auth, socket }));
      },
      onCancel: () => {
        openFun();
        // alert("Cancel");
      },
      customUI: CustomUIDelete,
      className: "my-custom-ui-container"
    });
  };

  const handleCopyLink = () => {
    navigator.clipboard.writeText(`${BASE_URL}/album/${album._id}`);
  };

  return (
    <div className="card_header d-flex justify-content-between">
      <div
        className="d-flex align-items-center"
        onMouseEnter={handleHover}
        onMouseLeave={handleUnHover}
      >
        {infoModal && <InfoModal user={album.user} />}
        <Link to={`/profile/${album.user._id}`}>
          <Avatar src={album.user.avatar} size="big-avatar" />
        </Link>
        <div className="card_name ml-2">
          <h6 className="m-0">
            <Link to={`/profile/${album.user._id}`} className="text-dark">
              {album.user.username}
            </Link>
          </h6>
          <div className="d-flex align-items-center">
            <small className="text-muted mr-1">
              {moment(album.createdAt).fromNow()}
            </small>
            {album.visibility === "public" && (
              <ion-icon name="earth"></ion-icon>
            )}
            {album.visibility === "friend" && (
              <ion-icon name="people"></ion-icon>
            )}
            {album.visibility === "private" && (
              <ion-icon name="lock-closed"></ion-icon>
            )}
          </div>
        </div>
      </div>
      <div className="nav-item dropdown">
        <span
          className="material-icons btn-toggle p-2"
          id="moreLink"
          data-toggle="dropdown"
        >
          more_horiz
        </span>

        <div
          className="dropdown-menu"
          style={{ background: "black", padding: " 12px 12px 10px 12px" }}
        >
          {auth.user._id === album.user._id && (
            <>
              <div
                className="dropdown-item "
                style={{ padding: "0px 0px 4px 0" }}
                onClick={handleEditAlbum}
              >
                <button
                  className="myCustomButton"
                  style={{ "--clr": "#1e9bff", width: "100% " }}
                >
                  {" "}
                  <span>Chỉnh sửa </span>
                  <i></i>
                </button>
              </div>{" "}
              <div
                className="dropdown-item"
                style={{ padding: "0px 0px 4px 0" }}
                onClick={onClickDelete}
              >
                <button
                  className="myCustomButton"
                  style={{ "--clr": "#ff1867", width: "100% " }}
                >
                  {" "}
                  <span>Xóa </span>
                  <i></i>
                </button>
              </div>{" "}
            </>
          )}
          <div
            className="dropdown-item"
            style={{ padding: "0px 0px 4px 0" }}
            onClick={handleCopyLink}
          >
            <button
              className="myCustomButton"
              style={{ "--clr": "#6eff3e", width: "100% " }}
            >
              {" "}
              <span>Copy Link </span>
              <i></i>
            </button>
          </div>{" "}
        </div>
      </div>
    </div>
  );
};

export default HeadAlbum;
