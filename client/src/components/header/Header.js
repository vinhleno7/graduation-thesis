import React from "react";
import { Link, useHistory, useLocation } from "react-router-dom";
import Menu from "./Menu";
import Search from "./Search";
import { useSelector } from "react-redux";
import Logo from "../../images/lv social logo .png";
import MenuMobile from "./MenuMobile";

const Header = () => {
  const { theme } = useSelector((state) => state);
  const { pathname } = useLocation();
  const history = useHistory();
  const handleGoToHome = () => {
    if (pathname === "/") {
      window.scrollTo({ top: 0 });
    } else {
      history.push("/");
      window.scrollTo({ top: 0 });
    }
  };
  return (
    <>
      <div
        className="header"
        style={{
          filter: `${theme ? `invert(1)` : `invert(0)`}`
        }}
      >
        <nav
          className="navbar navbar-expand-lg navbar-light  align-middle"
          style={{ justifyContent: "space-evenly" }}
        >
          <img
            onClick={handleGoToHome}
            style={{ width: "10%", height: "10%", cursor: "pointer" }}
            className=" p-0 m-0 lv_social_logo"
            src={Logo}
            alt="logo"
          />
          {/* <h1
            className="navbar-brand text-uppercase p-0 m-0"
            style={{
              filter: `${theme ? `invert(1)` : `invert(0)`}`
            }}
            onClick={() => window.scrollTo({ top: 0 })}
          >
            LV Social
          </h1> */}
          <Search />
          <Menu />
        </nav>
      </div>
      <div className="menu__mobile">
        <MenuMobile />
      </div>
    </>
  );
};

export default Header;
