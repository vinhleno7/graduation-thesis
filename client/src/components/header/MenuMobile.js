import React, { useState, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import activeMess from "../../images/active-messenger.svg";
import Mess from "../../images/messenger.svg";

import activeHome from "../../images/active-home.svg";
import Home from "../../images/home.svg";

import activeDiscover from "../../images/active-discover.svg";
import Discover from "../../images/discover.svg";
import activeNotify from "../../images/Active-Notify.svg";
import Notify from "../../images/Notify.svg";
import activePlus from "../../images/active-Plus.svg";
import gifNotify from "../../images/ezgif.com-gif-maker.gif";
import Plus from "../../images/Plus.svg";

import { logout } from "../../redux/actions/authAction";
import { GLOBALTYPES } from "../../redux/actions/globalTypes";
import Avatar from "../Avatar";

import NotifyModal from "../NotifyModal";
import Logo from "../../images/vb.svg.png";
import LogoWhite from "../../images/vt.png";

// NOTIFY
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

const style = {
  position: "absolute",
  top: "50%",
  border: "none !important",
  outline: "none !important",
  borderRadius: "12px",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4
};

const MenuMobile = () => {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const { auth, theme, status, notify } = useSelector((state) => state);
  const dispatch = useDispatch();
  const { pathname } = useLocation();
  const [breakPoint, setBreakPoint] = useState(false);

  const handleAdd = () => {
    dispatch({ type: GLOBALTYPES.STATUS, payload: true });
  };

  useEffect(() => {
    setBreakPoint(false);
    if (status || open) setBreakPoint(true);
  }, [status, open]);

  const isActive = (pn) => {
    if (breakPoint) {
      return;
    }

    if (pn === pathname) return "active";
  };

  const isIncludes = (pn) => {
    if (breakPoint) {
      return;
    }
    return pathname.includes(pn) ? "active" : "";
  };

  return (
    <div className="navigation" style={{ width: "100vw " }}>
      <ul>
        <li className={`${isActive("/") || isActive("/admin")}  list`}>
          <Link to="/">
            <span className="icon">
              {isActive("/") || isActive("/admin") ? (
                <img
                  style={{ width: "24px" }}
                  className=" p-0 m-0 lv_social_logo_black"
                  src={Logo}
                  alt="logo"
                />
              ) : (
                <svg
                  style={{ verticalAlign: "baseline" }}
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  height="24"
                  viewBox="0 0 100 100"
                  width="24"
                >
                  <path
                    d="M99.25 48.66V10.28c0-.59-.75-.86-1.12-.39l-41.92 52.4a.627.627 0 00.49 1.02h30.29c.82 0 1.59-.37 2.1-1.01l9.57-11.96c.38-.48.59-1.07.59-1.68zM1.17 50.34L32.66 89.7c.51.64 1.28 1.01 2.1 1.01h30.29c.53 0 .82-.61.49-1.02L1.7 9.89c-.37-.46-1.12-.2-1.12.39v38.38c0 .61.21 1.2.59 1.68z"
                    fill="#fff"
                  ></path>
                </svg>
              )}
            </span>
            <span className="text "> Trang chủ </span>
          </Link>
        </li>

        <li className={`${isIncludes("/message")} list`}>
          <Link to="/message">
            <span className="icon">
              {isIncludes("/message") ? (
                <ion-icon name="chatbubble"></ion-icon>
              ) : (
                <ion-icon name="chatbubble-outline"></ion-icon>
              )}
            </span>
            <span className="text "> Tin nhắn </span>
          </Link>
        </li>
        <li className={`${status ? "active list" : "list"} `}>
          <div className="fakeA" style={{ cursor: "pointer" }}>
            <span className="icon">
              {status ? (
                <ion-icon name="add-circle"></ion-icon>
              ) : (
                <ion-icon
                  name="add-circle-outline"
                  onClick={handleAdd}
                ></ion-icon>
              )}
            </span>
            <span className="text "> Tạo mới </span>
          </div>
        </li>
        <li className={`${isActive("/discover")} list`}>
          <Link to="/discover">
            <span className="icon">
              {isActive("/discover") ? (
                <ion-icon name="compass"></ion-icon>
              ) : (
                <ion-icon name="compass-outline"></ion-icon>
              )}
            </span>
            <span className="text "> Khám phá </span>
          </Link>
        </li>

        <li className={`${open ? "active list" : "list"} `}>
          <div className="fakeA" style={{ cursor: "pointer" }}>
            <span className="icon">
              {open ? (
                <ion-icon name="heart" onClick={handleClose}></ion-icon>
              ) : (
                <ion-icon name="heart-outline" onClick={handleOpen}></ion-icon>
              )}
              {notify.data.filter((msg) => msg.isRead !== true).length > 0 ? (
                <span className="notify_new"></span>
              ) : (
                ""
              )}
            </span>
            <span className="text "> Thông báo </span>
          </div>
        </li>

        <li className={`${isIncludes(`profile`) ? " active list" : "list"}`}>
          <Link to={`/profile/${auth.user._id}`}>
            <span className="icon">
              <img
                style={{
                  width: "24px",
                  borderRadius: "50px",
                  verticalAlign: "baseline"
                }}
                src={auth.user.avatar}
                alt="avatar"
              />
            </span>
            <span className="text "> Trang cá nhân </span>
          </Link>
        </li>
        <div className="indicator"></div>
      </ul>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <NotifyModal
              handleCloseNotify={handleClose}
              handleOpenNotify={handleOpen}
            />
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};

export default MenuMobile;
