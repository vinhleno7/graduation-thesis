import React, { useState, useEffect, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getDataAPI } from "../../utils/fetchData";
import { GLOBALTYPES } from "../../redux/actions/globalTypes";
import UserCard from "../UserCard";
import LoadIcon from "../../images/loading.gif";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

const Search = () => {
  const inputRef = useRef();
  const [search, setSearch] = useState("");
  const [users, setUsers] = useState([]);

  const { auth, theme } = useSelector((state) => state);
  const dispatch = useDispatch();
  const [load, setLoad] = useState(false);

  // useEffect(() => {
  //   if (search) {
  //     getDataAPI(`search?username=${search}`, auth.token).then((res) =>
  //       setUsers(res.data.users).catch((err) =>
  //         dispatch({
  //           type: GLOBALTYPES.ALERT,
  //           payload: { error: err.response.data.msg }
  //         })
  //       )
  //     );
  //   } else {
  //     setUsers([]);
  //   }
  // }, [search, auth.token, dispatch]);

  const handleSearch = async (e) => {
    e.preventDefault();
    if (!search) return;
    try {
      setLoad(true);
      const res = await getDataAPI(`search?username=${search}`, auth.token);
      setUsers(res.data.users);
      setLoad(false);
    } catch (err) {
      toast.error(err.response.data.msg);
      // dispatch({
      //   type: GLOBALTYPES.ALERT,
      //   payload: { error: err.response.data.msg }
      // });
    }
  };

  useEffect(() => {
    setUsers([]);
  }, [search]);

  const handleClose = () => {
    setSearch("");
    setUsers([]);
  };

  return (
    <form className="search_form" onSubmit={handleSearch}>
      <input
        type="text"
        name="search"
        ref={inputRef}
        value={search}
        id="search"
        title="Tìm kiếm..."
        onChange={(e) => setSearch(e.target.value)}
      />
      <div className="search_icon" style={{ opacity: search ? 0 : 0.3 }}>
        <span className="material-icons ">search</span>
        <span className="ml-2" style={{ fontSize: "16px" }}>
          Tìm kiếm...
        </span>
      </div>

      {!load && (
        <span
          className="close_search"
          onClick={handleClose}
          style={{ opacity: search.length === 0 ? 0 : 1 }}
        >
          &times;
        </span>
      )}

      <button type="submit" style={{ display: "none" }}>
        Tìm
      </button>

      {load && (
        <img
          src={LoadIcon}
          style={{ width: "10px", height: "10px" }}
          alt="Loading..."
          className="loading"
        />
      )}

      <div className="users">
        {search &&
          users.map((user) => (
            <UserCard
              size="big-avatar"
              key={user._id}
              user={user}
              border="border"
              handleClose={handleClose}
            />
          ))}
      </div>
    </form>
  );
};

export default Search;
