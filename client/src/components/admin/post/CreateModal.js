import React, { useState, useRef, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { GLOBALTYPES } from "../../../redux/actions/globalTypes";
import { adminCreatePost, updatePost } from "../../../redux/actions/postAction";
import Camera from "../../../images/camera.svg";
import Image from "../../../images/Image.svg";
import Icons from "../../Icons";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import Close from "../../../images/icons8-close (1).svg";
import { imageShow, videoShow } from "../../../utils/mediaShow";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

const CreateModal = ({ handleClose }) => {
  const { auth, theme, status, socket, aUser } = useSelector((state) => state);
  const dispatch = useDispatch();

  const [content, setContent] = useState("");
  const [images, setImages] = useState([]);
  const [myUser, setMyUser] = useState({});
  const [stream, setStream] = useState(false);
  const videoRef = useRef();
  const refCanvas = useRef();
  const [tracks, setTracks] = useState("");

  const handleChangeImages = (e) => {
    const files = [...e.target.files];
    let err = "";
    let newImages = [];
    files.forEach((file) => {
      if (!file) return (err = "File does not exist.");

      if (file.size > 1024 * 1024 * 5) {
        return (err = "The image/video largest is 5mb.");
      }

      return newImages.push(file);
    });

    if (err) toast.error(err);

    setImages([...images, ...newImages]);
  };

  const deleteImages = (index) => {
    const newArr = [...images];

    newArr.splice(index, 1);
    setImages(newArr);
  };

  const handleStream = () => {
    setStream(true);
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices
        .getUserMedia({ video: true })
        .then((mediaStream) => {
          videoRef.current.srcObject = mediaStream;
          videoRef.current.play();

          const track = mediaStream.getTracks();
          setTracks(track[0]);
        })
        .catch((err) => console.log(err));
    }
  };

  const handleCapture = () => {
    const width = videoRef.current.clientWidth;
    const height = videoRef.current.clientHeight;

    refCanvas.current.setAttribute("width", width);
    refCanvas.current.setAttribute("height", height);
    const ctx = refCanvas.current.getContext("2d");
    ctx.drawImage(videoRef.current, 0, 0, width, height);
    let URL = refCanvas.current.toDataURL();
    setImages([...images, { camera: URL }]);
  };

  const handleStopStream = () => {
    tracks.stop();
    setStream(false);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (images.length === 0) return toast.error("Please add your media.");
    if (status.onEdit) {
      dispatch(updatePost({ content, images, auth, status }));
    } else {
      dispatch(adminCreatePost({ content, myUser, images, auth, socket }));
    }

    setContent("");
    setImages([]);
    if (tracks) tracks.stop();
    dispatch({ type: GLOBALTYPES.STATUS, payload: false });
  };

  useEffect(() => {
    if (status.onEdit) {
      setContent(status.content);
      setImages(status.images);
    }
  }, [status]);

  // const handleCreateStory = (e) => {
  //   e.preventDefault();
  //   if (images.length === 0) return toast.error("Please add your media.");
  //   else {
  //     dispatch(createStory({ content, images, auth, socket }));
  //   }
  //   setContent("");
  //   setImages([]);
  //   if (tracks) tracks.stop();
  //   dispatch({ type: GLOBALTYPES.STATUS, payload: false });
  // };

  return (
    <div className="status_modal">
      <img
        style={{
          position: "absolute",
          top: "0",
          right: "10px",
          cursor: "pointer",
          fontSize: "42px",
          color: "red"
        }}
        onClick={handleClose}
        src={Close}
        alt="Close"
      />
      <form onSubmit={handleSubmit}>
        <div className="status_header ">
          {/* <Avatar
            src={auth.user.avatar}
            size="medium-avatar"
            style={{ marginBottom: "4px" }}
          />
          <p style={{ margin: "0px 0 0 8px", fontWeight: 500 }}>
            {auth.user.username}
          </p> */}
          <Autocomplete
            id="country-select-demo"
            sx={{ width: 300 }}
            options={aUser.users}
            onChange={(event, value) => setMyUser(value)}
            autoHighlight
            getOptionLabel={(user) => user.username}
            renderOption={(props, user) => (
              <Box
                component="li"
                sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                {...props}
              >
                <img
                  loading="lazy"
                  width="20"
                  src={user.avatar}
                  srcSet={user.avatar}
                  alt="avatar user"
                />
                {user.username}
              </Box>
            )}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Choose a user"
                inputProps={{
                  ...params.inputProps,
                  autoComplete: "new-password" // disable autocomplete and autofill
                }}
              />
            )}
          />
          {/* <h6
            onClick={handleCreateStory}
            className="imgHover"
            style={{
              position: "absolute",
              right: 0,
              cursor: "pointer",
              color: "#38b6ff"
            }}
          >
            Add to my story
          </h6> */}
        </div>

        <div className="status_body">
          <textarea
            name="content"
            value={content}
            placeholder={` ${
              myUser && myUser.username
                ? myUser.username + ", write your caption..."
                : "Please choose your user FIRST"
            } `}
            onChange={(e) => setContent(e.target.value)}
          />

          <div className="d-flex">
            <div className="flex-fill"></div>
            <Icons setContent={setContent} content={content} theme={theme} />
          </div>

          <div className="show_images">
            {images.map((img, index) => (
              <div key={index} id="file_img">
                {img.camera ? (
                  imageShow(img.camera, theme)
                ) : img.url ? (
                  <>
                    {img.url.match(/video/i)
                      ? videoShow(img.url, theme)
                      : imageShow(img.url, theme)}
                  </>
                ) : (
                  <>
                    {img.type.match(/video/i)
                      ? videoShow(URL.createObjectURL(img), theme)
                      : imageShow(URL.createObjectURL(img), theme)}
                  </>
                )}
                <span onClick={() => deleteImages(index)}>
                  <img src={Close} alt="Close" />
                </span>
              </div>
            ))}
          </div>

          {stream && (
            <div className="stream position-relative">
              <video
                autoPlay
                muted
                ref={videoRef}
                width="100%"
                height="100%"
                style={{ filter: theme ? "invert(1)" : "invert(0)" }}
              />
              <span onClick={handleStopStream}>
                <img src={Close} alt="Close" />
              </span>
              <canvas ref={refCanvas} style={{ display: "none" }} />
            </div>
          )}

          <div className="input_images">
            {stream ? (
              <img
                className="button camera"
                style={{ height: "35px", width: "35px", cursor: "pointer" }}
                src={Camera}
                alt="camera"
                onClick={handleCapture}
              />
            ) : (
              <>
                <img
                  className="button camera"
                  style={{ height: "35px", width: "35px", cursor: "pointer" }}
                  src={Camera}
                  alt="camera"
                  onClick={handleStream}
                />
                <div className="file_upload">
                  <img
                    className="button"
                    style={{ height: "35px", width: "50px", cursor: "pointer" }}
                    src={Image}
                    alt="this is my upload pic"
                  />
                  <input
                    className="buttonFile"
                    type="file"
                    name="file"
                    id="file"
                    multiple
                    accept="image/*,video/*"
                    onChange={handleChangeImages}
                  />
                </div>
              </>
            )}
          </div>
        </div>

        <div className="status_footer ">
          {/* <button className="btn btn-secondary w-100" type="submit">
            Share
          </button> */}
          <button
            type="submit"
            className="myCustomButton"
            style={{ "--clr": "#1e9bff", width: "100%" }}
          >
            {" "}
            <span>Share</span>
            <i></i>
          </button>
        </div>
      </form>
    </div>
  );
};

export default CreateModal;
