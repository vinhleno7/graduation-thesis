import * as React from "react";
import { useTheme } from "@mui/material/styles";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";
import TableHead from "@mui/material/TableHead";
import { useSelector, useDispatch } from "react-redux";
import Avatar from "../../Avatar";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import Backdrop from "@mui/material/Backdrop";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import MaleIcon from "@mui/icons-material/Male";
import FemaleIcon from "@mui/icons-material/Female";
import TransgenderIcon from "@mui/icons-material/Transgender";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import PersonIcon from "@mui/icons-material/Person";
import {
  deletePost,
  responseReport
} from "../../../redux/actions/a-postAction";
import EditModal from "./EditModal";
import LoadIcon from "../../../images/loading.gif";
import moment from "moment";
import Carousel from "../../Carousel";
import { GLOBALTYPES } from "../../../redux/actions/globalTypes";
import { onConfirm } from "react-confirm-pro";

const style = {
  position: "absolute",
  borderRadius: "8px",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4
};

interface TablePaginationActionsProps {
  count: number;
  page: number;
  rowsPerPage: number;
  onPageChange: (
    event: React.MouseEvent<HTMLButtonElement>,
    newPage: number
  ) => void;
}

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14
  }
}));

function TablePaginationActions(props: TablePaginationActionsProps) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

function createData(name: string, calories: number, fat: number) {
  return { name, calories, fat };
}

export default function CustomPaginationActionsTable({ reports }) {
  const { aPost, auth, theme, socket } = useSelector((state) => state);
  const dispatch = useDispatch();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - aPost.reports.length) : 0;

  const handleChangePage = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const [images, setImages] = React.useState([]);

  const [open, setOpen] = React.useState(false);
  const [reportAd, setReportAd] = React.useState({});
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const onClickItem = (report) => {
    if (report.status === undefined || !report.status) handleOpen();
    setReportAd(report);
  };
  const handleDeletePost = (post) => {
    dispatch(deletePost({ post, auth }));
    handleClose();
  };

  const [openModalEdit, setOpenModalEdit] = React.useState(false);

  const turnOffModalEdit = () => {
    setOpenModalEdit(false);
  };

  const handleOpenModalEdit = (post) => {
    handleClose();
    dispatch({ type: GLOBALTYPES.STATUS, payload: { ...post, onEdit: true } });
  };

  const deleteImages = (index) => {
    const newArr = [...images];

    newArr.splice(index, 1);
    setImages(newArr);
  };

  const CustomUIDelete = ({ onSubmit, onCancel }) => (
    <div className="confirm">
      <h1>
        <strong>Xóa bài viết ?</strong>
      </h1>
      <p>
        Bạn có chắc chắn việc <strong>đồng ý</strong> bài báo cáo của
        <strong> {reportAd.author.username}</strong> và <strong>xóa</strong> bài
        viết này không ?
      </p>
      <button onClick={onCancel}>Hủy bỏ</button>
      <button onClick={onSubmit}>Đồng ý xóa</button>
    </div>
  );

  const onClickDenyReport = () => {
    const response = false;
    dispatch(
      responseReport({ reportId: reportAd._id, response, auth, socket })
    );
  };

  const onClickAcceptReport = () => {
    handleClose();
    onConfirm({
      onSubmit: () => {
        const response = true;
        dispatch(responseReport({ reportId: reportAd._id, response, auth }));
      },
      onCancel: () => {
        handleOpen();
        // alert("Cancel");
      },
      customUI: CustomUIDelete,
      className: "my-custom-ui-container"
    });
  };

  return (
    <Paper sx={{ width: "100%" }}>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={open}>
          {reportAd?.post?.images ? (
            <Box Box sx={style}>
              <div className="post__admin__container">
                <div>
                  <span style={{ color: "#38b6ff" }}>Người báo cáo: </span>{" "}
                  <Avatar src={reportAd?.author?.avatar} size="medium-avatar" />
                  <span className="ml-1 "> {reportAd?.author?.username}</span>
                </div>
                <div>
                  <span style={{ color: "#38b6ff" }}>Lý do: </span>{" "}
                  <span className="ml-1 "> {reportAd?.reason}</span>
                </div>
                <div className="mb-3">
                  <span style={{ color: "#38b6ff" }}>Tình trạng: </span>{" "}
                  <span className="ml-1 ">
                    {" "}
                    {reportAd?.status === undefined
                      ? "Chưa duyệt"
                      : reportAd.status
                      ? "Đã duyệt và đồng ý xóa"
                      : "Đã hủy bỏ"}
                  </span>
                </div>
                <Carousel
                  images={reportAd?.post?.images}
                  id={reportAd?.post?._id}
                />
              </div>

              <div className="d-flex justify-content-center align-items-center">
                <Typography
                  sx={{ textAlign: "center" }}
                  id="transition-modal-title"
                  variant="h6"
                  component="h2"
                >
                  <span style={{ color: "#38b6ff" }}>Nội dung: </span>{" "}
                  {reportAd.post.content}
                </Typography>
              </div>

              <Avatar src={reportAd?.post?.user?.avatar} size="medium-avatar" />

              <span className="ml-1 "> {reportAd?.post?.user?.username}</span>

              <div className="d-flex justify-content-center">
                <a
                  href={`/post/${reportAd?.post._id}`}
                  target="_blank"
                  rel="noreferrer"
                >
                  <Typography
                    sx={{ padding: "4px 16px", borderRight: "1px solid #ddd" }}
                    id="transition-modal-description"
                  >
                    <span style={{ color: "#38b6ff" }}>Lượt thích: </span>{" "}
                    {!reportAd.post.likes ? "0" : reportAd?.post.likes.length}
                  </Typography>
                </a>
                <a
                  href={`/post/${reportAd?.post._id}`}
                  target="_blank"
                  rel="noreferrer"
                >
                  <Typography
                    sx={{ padding: "4px 16px" }}
                    id="transition-modal-description"
                  >
                    <span style={{ color: "#38b6ff" }}>Lời nhận xét: </span>{" "}
                    {!reportAd?.post.comments
                      ? "0"
                      : reportAd?.post.comments.length}
                  </Typography>
                </a>
              </div>

              <div>
                <p
                  onClick={() => onClickAcceptReport()}
                  className="imgHover"
                  style={{
                    cursor: "pointer",
                    padding: "8px 0 0 0",
                    textAlign: "center",
                    margin: 0,
                    color: "red",
                    fontWeight: "bold"
                  }}
                >
                  Đồng ý báo cáo và xóa
                </p>
                <p
                  onClick={() => onClickDenyReport()}
                  className="imgHover"
                  style={{
                    cursor: "pointer",
                    padding: "8px 0 0 0",
                    textAlign: "center",
                    margin: 0,
                    color: "#bdbdbd",
                    fontWeight: "bold"
                  }}
                >
                  Từ chối báo cáo và hủy bỏ
                </p>
              </div>
            </Box>
          ) : (
            <div className="d-flex justify-content-center">
              <img
                src={LoadIcon}
                style={{ width: "50px", height: "50px" }}
                alt="Loading..."
                className="loading"
              />
            </div>
          )}
        </Fade>
      </Modal>

      {aPost.loading ? (
        <div className="d-flex justify-content-center">
          <img
            src={LoadIcon}
            style={{ width: "50px", height: "50px" }}
            alt="Loading..."
            className="loading"
          />
        </div>
      ) : (
        <>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 500 }} aria-label="custom pagination table">
              <TableHead>
                <TableRow>
                  <StyledTableCell align="center">Bài viết</StyledTableCell>

                  <StyledTableCell align="center">Lý do</StyledTableCell>
                  <StyledTableCell align="center">Tình trạng</StyledTableCell>

                  <StyledTableCell align="center">
                    Người báo cáo
                  </StyledTableCell>
                  <StyledTableCell align="center">Ngày tạo</StyledTableCell>
                </TableRow>
              </TableHead>
              {aPost.countRP >= 0 && (
                <TableBody>
                  {(rowsPerPage > 0
                    ? aPost.reports.slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                    : aPost.reports
                  ).map((report) => (
                    <TableRow
                      key={report?._id}
                      onClick={() => onClickItem(report)}
                    >
                      <TableCell
                        className="d-flex  align-items-center"
                        component="th"
                        scope="row"
                      >
                        {report?.post?.images?.map((image) =>
                          image.url.match(/video/i) ? (
                            <video
                              key={image.url}
                              src={image.url}
                              width="30px"
                              height="30px"
                            />
                          ) : (
                            <Avatar
                              key={image.url}
                              src={image.url}
                              size="medium-avatar"
                            />
                          )
                        )}
                      </TableCell>

                      <TableCell style={{ whiteSpace: "nowrap" }}>
                        {report?.reason}
                      </TableCell>
                      <TableCell style={{ whiteSpace: "nowrap" }}>
                        {report?.status === undefined
                          ? "Chưa duyệt"
                          : report.status
                          ? "Đã duyệt và đồng ý xóa"
                          : "Đã hủy bỏ"}
                      </TableCell>
                      <TableCell style={{ whiteSpace: "nowrap" }}>
                        <Avatar
                          src={report?.author?.avatar}
                          size="medium-avatar"
                        />
                        <span className="ml-1">
                          {" "}
                          {report?.author?.username}
                        </span>
                      </TableCell>

                      <TableCell
                        style={{ whiteSpace: "nowrap" }}
                        align="center"
                      >
                        {moment(report.createdAt).format("llll")}
                      </TableCell>
                    </TableRow>
                  ))}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
              )}
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25, { label: "All", value: -1 }]}
            component="div"
            colSpan={5}
            count={aPost.reports.length}
            rowsPerPage={rowsPerPage}
            page={page}
            SelectProps={{
              inputProps: {
                "aria-label": "rows per page"
              },
              native: true
            }}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
            ActionsComponent={TablePaginationActions}
          />
        </>
      )}
    </Paper>
  );
}
