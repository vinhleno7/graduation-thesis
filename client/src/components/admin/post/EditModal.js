import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { checkImage } from "../../../utils/imageUpload";
import { GLOBALTYPES } from "../../../redux/actions/globalTypes";
import { updateUser } from "../../../redux/actions/profileAction";

const EditModal = ({ turnOffModalEdit, user }) => {
  const initialState = {
    role: "",
    mobile: "",
    story: "",
    address: "",
    website: "",
    fullname: "",
    username: "",
    email: "",
    password: "",
    gender: "",
    cf_password: ""
  };

  const [userData, setUserData] = useState(user);
  const {
    avatar,
    role,
    mobile,
    story,
    address,
    website,
    fullname,
    username,
    email,
    password,
    gender,
    cf_password
  } = userData;

  const [picture, setPicture] = useState("");
  const [typePass, setTypePass] = useState(false);
  const [typeCfPass, setTypeCfPass] = useState(false);

  const { auth, theme } = useSelector((state) => state);
  const dispatch = useDispatch();

  // useEffect(() => {
  //   setUserData(auth.user);
  // }, [auth.user]);

  const changeAvatar = (e) => {
    const file = e.target.files[0];

    const err = checkImage(file);
    if (err)
      return dispatch({ type: GLOBALTYPES.ALERT, payload: { error: err } });
    setPicture(file);
  };
  const handleInput = (e) => {
    const { name, value } = e.target;
    setUserData({ ...userData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(updateUser({ userData, picture, auth }));
    // turnOffModalEdit();
  };

  return (
    <div className="edit_profile">
      <button
        className="btn btn-danger btn_close"
        onClick={() => turnOffModalEdit()}
      >
        Close
      </button>

      <form onSubmit={handleSubmit}>
        <div className="info_avatar">
          <img
            src={picture ? URL.createObjectURL(picture) : avatar}
            alt="avatar"
            style={{ filter: theme ? "invert(1)" : "invert(0)" }}
          />
          <span>
            <i className="fas fa-camera" />
            <p>Thay đổi</p>
            <input
              type="file"
              name="file"
              id="file_up"
              accept="image/*"
              onChange={changeAvatar}
            />
          </span>
        </div>

        <label htmlFor="role">Quyền</label>
        <div className="input-group-prepend px-0 mb-4">
          <select
            name="role"
            id="role"
            value={role}
            className="custom-select text-capitalize"
            onChange={handleInput}
          >
            <option value="user">Người dùng</option>
            <option value="admin">Admin</option>
          </select>
        </div>

        <div className="form-group">
          <label htmlFor="username">Tên tài khoản</label>
          <div className="position-relative">
            <input
              type="text"
              className="form-control"
              id="username"
              name="username"
              value={username}
              onChange={handleInput}
            />
            <small
              className="text-danger position-absolute"
              style={{
                top: "50%",
                right: "5px",
                transform: "translateY(-50%)"
              }}
            >
              {username.length}/25
            </small>
          </div>
        </div>
        <div className="form-group">
          <label htmlFor="fullname">Họ và tên</label>
          <div className="position-relative">
            <input
              type="text"
              className="form-control"
              id="fullname"
              name="fullname"
              value={fullname}
              onChange={handleInput}
            />
            <small
              className="text-danger position-absolute"
              style={{
                top: "50%",
                right: "5px",
                transform: "translateY(-50%)"
              }}
            >
              {fullname.length}/25
            </small>
          </div>
        </div>

        <div className="form-group">
          <label htmlFor="mobile">Số điện thoại</label>
          <input
            type="text"
            name="mobile"
            value={mobile}
            className="form-control"
            onChange={handleInput}
          />
        </div>

        <div className="form-group">
          <label htmlFor="address">Địa chỉ</label>
          <input
            type="text"
            name="address"
            value={address}
            className="form-control"
            onChange={handleInput}
          />
        </div>
        <div className="form-group">
          <label htmlFor="website">Trang web</label>
          <input
            type="text"
            name="website"
            value={website}
            className="form-control"
            onChange={handleInput}
          />
        </div>
        <div className="form-group">
          <label htmlFor="story">Câu chuyện</label>
          <textarea
            name="story"
            value={story}
            className="form-control"
            onChange={handleInput}
            cols="30"
            rows="4"
          />
          <small className="text-danger">{story.length}/200</small>
        </div>

        <label htmlFor="gender">Gender</label>
        <div className="input-group-prepend px-0 mb-4">
          <select
            name="gender"
            id="gender"
            value={gender}
            className="custom-select text-capitalize"
            onChange={handleInput}
          >
            <option value="male">Male</option>
            <option value="female">Female</option>
            <option value="other">Other</option>
          </select>
        </div>

        <div className="form-group">
          <label htmlFor="exampleInputPassword1">Password</label>

          <div className="pass">
            <input
              type={typePass ? "text" : "password"}
              className="form-control"
              id="exampleInputPassword1"
              onChange={handleInput}
              value={password}
              name="password"
              style={{ background: `${alert.password ? "#fd2d6a14" : ""}` }}
            />

            <small onClick={() => setTypePass(!typePass)}>
              {typePass ? "Ẩn" : "Hiện"}
            </small>
          </div>

          <small className="form-text text-danger">
            {alert.password ? alert.password : ""}
          </small>
        </div>

        <div className="form-group">
          <label htmlFor="cf_password">Confirm Password</label>

          <div className="pass">
            <input
              type={typeCfPass ? "text" : "password"}
              className="form-control"
              id="cf_password"
              onChange={handleInput}
              value={cf_password}
              name="cf_password"
              style={{ background: `${alert.cf_password ? "#fd2d6a14" : ""}` }}
            />

            <small onClick={() => setTypeCfPass(!typeCfPass)}>
              {typeCfPass ? "Ẩn" : "Hiện"}
            </small>
          </div>

          <small className="form-text text-danger">
            {alert.cf_password ? alert.cf_password : ""}
          </small>
        </div>

        <button
          className="myCustomButton  w-100"
          type="submit"
          style={{ "--clr": "#1e9bff", width: "100% " }}
        >
          {" "}
          <span>Save </span>
          <i></i>
        </button>
      </form>
    </div>
  );
};

export default EditModal;
