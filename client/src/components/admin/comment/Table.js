import * as React from "react";
import { useTheme } from "@mui/material/styles";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";
import TableHead from "@mui/material/TableHead";
import { useSelector, useDispatch } from "react-redux";
import Avatar from "../../Avatar";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import Backdrop from "@mui/material/Backdrop";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import MaleIcon from "@mui/icons-material/Male";
import FemaleIcon from "@mui/icons-material/Female";
import TransgenderIcon from "@mui/icons-material/Transgender";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import PersonIcon from "@mui/icons-material/Person";
import { deleteComment } from "../../../redux/actions/a-commentAction";
import EditModal from "./EditModal";
import LoadIcon from "../../../images/loading.gif";
import moment from "moment";
import Carousel from "../../Carousel";
import { GLOBALTYPES } from "../../../redux/actions/globalTypes";
import { updateComment } from "../../../redux/actions/a-commentAction";
import Comments from "../../home/Comments";
import InputComment from "./InputComment";

const style = {
  position: "absolute",
  borderRadius: "8px",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4
};

interface TablePaginationActionsProps {
  count: number;
  page: number;
  rowsPerPage: number;
  onPageChange: (
    event: React.MouseEvent<HTMLButtonElement>,
    newPage: number
  ) => void;
}

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14
  }
}));

function TablePaginationActions(props: TablePaginationActionsProps) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

function createData(name: string, calories: number, fat: number) {
  return { name, calories, fat };
}

export default function CustomPaginationActionsTable({ search }) {
  const { aComment, auth, theme } = useSelector((state) => state);
  const dispatch = useDispatch();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0
      ? Math.max(0, (1 + page) * rowsPerPage - aComment.comments.length)
      : 0;

  const handleChangePage = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const [images, setImages] = React.useState([]);

  const [onEdit, setOnEdit] = React.useState(false);
  const [content, setContent] = React.useState();
  const [open, setOpen] = React.useState(false);
  const [comment, setComment] = React.useState({});
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const onClickItem = (comment) => {
    handleOpen();
    setComment(comment);
  };
  const handleDeleteComment = (comment) => {
    dispatch(deleteComment({ comment, auth }));
    handleClose();
  };

  const [openModalEdit, setOpenModalEdit] = React.useState(false);

  const turnOffModalEdit = () => {
    setOpenModalEdit(false);
  };

  const handleOpenModalEdit = (comment) => {
    setOnEdit(!onEdit);
    setContent(comment.content);
  };

  const handleUpdate = (comment) => {
    if (comment.content !== content) {
      dispatch(updateComment({ comment, content, auth }));
      setOnEdit(false);
    } else {
      setOnEdit(false);
    }
  };
  return (
    <Paper sx={{ width: "100%" }}>
      {openModalEdit && (
        <EditModal turnOffModalEdit={turnOffModalEdit} comment={comment} />
      )}

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={open}>
          {comment ? (
            <Box sx={style}>
              <div className="comment__admin__container">
                <div className="left__container">
                  <span
                    style={{
                      display: "block",
                      fontSize: "0.7rem",
                      color: "#38b6ff",
                      fontStyle: "italic",
                      margin: "0 0 8px 0"
                    }}
                  >
                    Bài viết:
                  </span>

                  {comment.user && (
                    <Avatar src={comment.user.avatar} size="medium-avatar" />
                  )}
                  <span className="ml-1">
                    {" "}
                    {!comment.postId
                      ? "Không có"
                      : comment.postId.user.username}
                  </span>
                  <div
                    style={{
                      margin: "10px",
                      wordWrap: "break-word",
                      borderBottom: "1px solid black"
                    }}
                  >
                    <p>
                      {!comment.postId ? "Không có" : comment.postId.content}
                    </p>

                    {comment.postId && (
                      <Carousel
                        images={comment.postId.images}
                        id={comment.postId._id}
                      />
                    )}
                  </div>
                </div>

                <div className="right__container">
                  <span
                    style={{
                      margin: "0 0 8px 0",
                      fontSize: "0.7rem",
                      color: "red",
                      fontStyle: "italic"
                    }}
                  >
                    Lời nhận xét:
                  </span>
                  {comment.postId && (
                    <InputComment
                      handleCloseAdminCmt={handleClose}
                      post={comment.postId}
                    />
                  )}
                  <div style={{ height: "400px", overflowY: "scroll" }}>
                    {comment.postId && (
                      <Comments
                        post={comment.postId}
                        handleCloseAdminCmt={handleClose}
                      />
                    )}
                  </div>
                </div>
              </div>
              <div className="middle__container">
                {comment.postId && (
                  <a
                    href={`post/${comment.postId._id}`}
                    target="_blank"
                    rel="noreferrer"
                  >
                    <p
                      className="imgHover"
                      style={{
                        cursor: "pointer",
                        padding: "8px",
                        textAlign: "center",
                        borderBottom: "1px solid #ddd",
                        margin: 0
                      }}
                    >
                      Xem chi tiết
                    </p>
                  </a>
                )}
              </div>
            </Box>
          ) : (
            <div className="d-flex justify-content-center">
              <img
                src={LoadIcon}
                style={{ width: "50px", height: "50px" }}
                alt="Loading..."
                className="loading"
              />
            </div>
          )}
        </Fade>
      </Modal>

      {search &&
        aComment.comments.filter(
          (comment) =>
            comment.content.toLowerCase().includes(search.toLowerCase()) ||
            (comment.postId &&
              comment.postId.content
                .toLowerCase()
                .includes(search.toLowerCase())) ||
            (comment.user &&
              comment.user.username
                .toLowerCase()
                .includes(search.toLowerCase())) ||
            moment(comment.createdAt)
              .format("llll")
              .toLowerCase()
              .includes(search.toLowerCase())
        ).length === 0 && (
          <p style={{ whiteSpace: "nowrap", color: "rgb(217 5 5)" }}>
            Không có kết quả cho lời nhận xét này.{" "}
          </p>
        )}
      {aComment.loading ? (
        <div className="d-flex justify-content-center">
          <img
            src={LoadIcon}
            style={{ width: "50px", height: "50px" }}
            alt="Loading..."
            className="loading"
          />
        </div>
      ) : (
        <>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 500 }} aria-label="custom pagination table">
              <TableHead>
                <TableRow>
                  <StyledTableCell align="center">Tác giả</StyledTableCell>

                  <StyledTableCell align="center">Nội dung</StyledTableCell>

                  <StyledTableCell align="center">Lượt thích</StyledTableCell>

                  <StyledTableCell align="center"> Bài viết</StyledTableCell>
                  <StyledTableCell align="center">Phản hồi cho</StyledTableCell>
                  <StyledTableCell align="center">Ngày tạo</StyledTableCell>
                </TableRow>
              </TableHead>
              {aComment.comments.length >= 0 && (
                <TableBody>
                  {(search
                    ? aComment.comments.filter(
                        (comment) =>
                          comment.content
                            .toLowerCase()
                            .includes(search.toLowerCase()) ||
                          (comment.postId &&
                            comment.postId.content
                              .toLowerCase()
                              .includes(search.toLowerCase())) ||
                          (comment.user &&
                            comment.user.username
                              .toLowerCase()
                              .includes(search.toLowerCase())) ||
                          moment(comment.createdAt)
                            .format("llll")
                            .toLowerCase()
                            .includes(search.toLowerCase())
                      )
                    : rowsPerPage > 0
                    ? aComment.comments.slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                    : aComment.comments
                  ).map((comment) => (
                    <TableRow
                      key={comment._id}
                      onClick={() => onClickItem(comment)}
                    >
                      <TableCell style={{ whiteSpace: "nowrap" }}>
                        {comment.user && (
                          <Avatar
                            src={comment.user.avatar}
                            size="medium-avatar"
                          />
                        )}
                        <span className="ml-1">
                          {" "}
                          {!comment.user
                            ? "User have been deleted"
                            : comment.user.username}
                        </span>
                      </TableCell>

                      <TableCell style={{ whiteSpace: "nowrap" }}>
                        {comment.content.length > 50
                          ? comment.content.slice(0, 20) + "..."
                          : comment.content}
                      </TableCell>

                      <TableCell
                        style={{ whiteSpace: "nowrap" }}
                        align="center"
                      >
                        {comment.likes.length}
                      </TableCell>

                      <TableCell
                        style={{ whiteSpace: "nowrap" }}
                        component="th"
                        scope="row"
                      >
                        {comment.postId ? (
                          <div className="d-flex  align-items-center">
                            {comment.postId.images.map((image) =>
                              image.url.match(/video/i) ? (
                                <video
                                  key={image.url}
                                  src={image.url}
                                  width="30px"
                                  height="30px"
                                />
                              ) : (
                                <Avatar
                                  key={image.url}
                                  src={image.url}
                                  size="medium-avatar"
                                />
                              )
                            )}
                            <p style={{ margin: "0px 2px" }}>
                              {comment.postId.content}
                            </p>
                          </div>
                        ) : (
                          <span style={{ color: "red" }}>
                            Bài viết này đã bị xóa
                          </span>
                        )}
                      </TableCell>

                      <TableCell style={{ whiteSpace: "nowrap" }}>
                        {comment.reply && (
                          <Avatar
                            src={comment.reply.user.avatar}
                            size="medium-avatar"
                          />
                        )}

                        <span className="ml-1">
                          {!comment.reply
                            ? "Không có lời nhận xét gốc."
                            : comment.reply.content.length > 50
                            ? comment.reply.content.slice(0, 20) + "..."
                            : comment.reply.content}
                        </span>
                      </TableCell>

                      <TableCell
                        style={{ whiteSpace: "nowrap" }}
                        align="center"
                      >
                        {moment(comment.createdAt).format("llll")}
                      </TableCell>
                    </TableRow>
                  ))}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
              )}
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25, { label: "All", value: -1 }]}
            component="div"
            colSpan={5}
            count={aComment.comments.length}
            rowsPerPage={rowsPerPage}
            page={page}
            SelectProps={{
              inputProps: {
                "aria-label": "rows per page"
              },
              native: true
            }}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
            ActionsComponent={TablePaginationActions}
          />
        </>
      )}
    </Paper>
  );
}
