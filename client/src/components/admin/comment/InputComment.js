import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { createComment } from "../../../redux/actions/a-commentAction";
import Autocomplete from "@mui/material/Autocomplete";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";

const InputComment = ({
  handleCloseAdminCmt,
  children,
  post,
  onReply,
  setOnReply
}) => {
  const [content, setContent] = useState("");
  const [myUser, setMyUser] = useState({});

  const { auth, socket, aUser } = useSelector((state) => state);

  // ! THEM SOCKET
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!content.trim()) {
      if (setOnReply) return setOnReply(false);
      return;
    }

    setContent("");

    const newComment = {
      content,
      likes: [],
      user: myUser,
      createdAt: new Date().toISOString(),
      reply: onReply && onReply.commentId,
      tag: onReply && onReply.user
    };
    dispatch(createComment(post, newComment, auth, socket));

    // ! THEM SOCKET

    if (setOnReply) return setOnReply(false);
    handleCloseAdminCmt();
  };
  return (
    <>
      <Autocomplete
        id="country-select-demo"
        sx={{ margin: "4px 0" }}
        options={aUser.users}
        onChange={(event, value) => setMyUser(value)}
        autoHighlight
        getOptionLabel={(user) => user.username}
        renderOption={(props, user) => (
          <Box
            component="li"
            sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
            {...props}
          >
            <img
              loading="lazy"
              width="20"
              src={user.avatar}
              srcSet={user.avatar}
              alt="avatar user"
            />
            {user.username}
          </Box>
        )}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Choose a user"
            inputProps={{
              ...params.inputProps,
              autoComplete: "new-password" // disable autocomplete and autofill
            }}
          />
        )}
      />
      <form className="card-footer comment_input" onSubmit={handleSubmit}>
        {children}
        <input
          type="text"
          placeholder={`${
            myUser && myUser.username
              ? `${myUser.username}, add your comment`
              : "Please choose a user"
          }`}
          value={content}
          onChange={(e) => setContent(e.target.value)}
        />

        <button
          type="submit"
          className="postBtn"
          style={
            content ? { opacity: 1 } : { opacity: 0.25, cursor: "pointer" }
          }
        >
          Đăng
        </button>
      </form>
    </>
  );
};

export default InputComment;
