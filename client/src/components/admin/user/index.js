import React, { useEffect, useRef, useState } from "react";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Table from "./Table";
import { useSelector, useDispatch } from "react-redux";
import { getAllUsers } from "../../../redux/actions/a-userAction";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import Typography from "@mui/material/Typography";
import CreateModal from "./CreateModal";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4
};

const User = () => {
  const ref = useRef();
  const { auth, aUser } = useSelector((state) => state);
  const dispatch = useDispatch();
  const [search, setSearch] = useState("");
  useEffect(() => {
    dispatch(getAllUsers(auth.token));
  }, [dispatch, auth.token]);

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <h1>
        Người dùng <span>{aUser.count}</span>
      </h1>
      <div className="d-flex flex-row align-items-center justify-content-between mb-2">
        <TextField
          id="outlined-basic"
          label="Tìm người dùng"
          variant="outlined"
          defaultValue=""
          inputRef={ref}
          onChange={() => setSearch(ref.current.value)}
        />

        {/* <Button variant="contained" onClick={handleOpen}>
          Create
        </Button> */}
        <button
          onClick={handleOpen}
          className="myCustomButton"
          style={{ "--clr": "#1e9bff" }}
        >
          {" "}
          <span>Thêm</span>
          <i></i>
        </button>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          open={open}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500
          }}
        >
          <Fade in={open}>
            <Box sx={style}>
              <p
                style={{ margin: "0px" }}
                className="text-uppercase text-center "
              >
                Thêm 1 người dùng
              </p>

              <CreateModal handleClose={handleClose} />
            </Box>
          </Fade>
        </Modal>
      </div>

      {aUser.users.length >= 0 && <Table search={search} />}
    </div>
  );
};

export default User;
