import * as React from "react";
import { useTheme } from "@mui/material/styles";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";
import TableHead from "@mui/material/TableHead";
import { useSelector, useDispatch } from "react-redux";
import Avatar from "../../Avatar";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import Backdrop from "@mui/material/Backdrop";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import MaleIcon from "@mui/icons-material/Male";
import FemaleIcon from "@mui/icons-material/Female";
import TransgenderIcon from "@mui/icons-material/Transgender";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import PersonIcon from "@mui/icons-material/Person";
import { deleteUser } from "../../../redux/actions/a-userAction";
import EditModal from "./EditModal";
import LoadIcon from "../../../images/loading.gif";
import { onConfirm } from "react-confirm-pro";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

const style = {
  position: "absolute",
  borderRadius: "8px",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4
};

interface TablePaginationActionsProps {
  count: number;
  page: number;
  rowsPerPage: number;
  onPageChange: (
    event: React.MouseEvent<HTMLButtonElement>,
    newPage: number
  ) => void;
}

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14
  }
}));

function TablePaginationActions(props: TablePaginationActionsProps) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

function createData(name: string, calories: number, fat: number) {
  return { name, calories, fat };
}

export default function CustomPaginationActionsTable({ search }) {
  const { aUser, auth } = useSelector((state) => state);
  const dispatch = useDispatch();

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - aUser.users.length) : 0;

  const handleChangePage = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const [open, setOpen] = React.useState(false);
  const [user, setUser] = React.useState({});
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const onClickItem = (user) => {
    handleOpen();
    setUser(user);
  };
  const handleDeleteUser = (user) => {
    if (user.role === "admin") return toast.error("You can't delete ADMIN.");
    else {
      dispatch(deleteUser({ user, auth }));
      handleClose();
    }
  };

  const [openModalEdit, setOpenModalEdit] = React.useState(false);

  const turnOffModalEdit = () => {
    setOpenModalEdit(false);
  };

  const handleOpenModalEdit = (user) => {
    handleClose();
    setOpenModalEdit(true);
  };

  const CustomUIDelete = ({ onSubmit, onCancel }) => (
    <div className="confirm">
      <h1>
        <strong>Xóa người dùng ?</strong>
      </h1>
      <p>
        Bạn có chắc chắn việc <strong>xóa {user.username}</strong> này không ?
      </p>
      <button onClick={onCancel}>Hủy bỏ</button>
      <button onClick={onSubmit}>Đồng ý xóa</button>
    </div>
  );

  const onClickAcceptReport = (user) => {
    handleClose();
    onConfirm({
      onSubmit: () => {
        if (user.role === "admin")
          return toast.error("Bạn không được xóa người quản lý.");
        else {
          dispatch(deleteUser({ user, auth }));
          handleClose();
        }
      },
      onCancel: () => {
        handleOpen();
        // alert("Cancel");
      },
      customUI: CustomUIDelete,
      className: "my-custom-ui-container"
    });
  };

  return (
    <Paper sx={{ width: "100%" }}>
      {openModalEdit && (
        <EditModal turnOffModalEdit={turnOffModalEdit} user={user} />
      )}

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <div className="d-flex justify-content-center">
              <Avatar src={user.avatar} size="half-avatar" />
            </div>

            <div className="d-flex justify-content-center align-items-center">
              {user.role === "admin" ? (
                <AdminPanelSettingsIcon color="error" />
              ) : (
                <PersonIcon color="primary" />
              )}
              <Typography
                sx={{ textAlign: "center" }}
                id="transition-modal-title"
                variant="h6"
                component="h2"
              >
                {user.username}
              </Typography>
              {user.gender === "other" ? (
                <TransgenderIcon />
              ) : user.gender === "male" ? (
                <MaleIcon color="info" />
              ) : (
                <FemaleIcon color="error" />
              )}
            </div>

            <div className="d-flex justify-content-center">
              <Typography
                sx={{ padding: "4px 16px", borderRight: "1px solid #ddd" }}
                id="transition-modal-description"
              >
                <span style={{ color: "#38b6ff" }}>Người theo dõi: </span>{" "}
                {!user.followers ? "0" : user.followers.length}
              </Typography>

              <Typography
                sx={{ padding: "4px 16px" }}
                id="transition-modal-description"
              >
                <span style={{ color: "#38b6ff" }}>Đang theo dõi: </span>{" "}
                {!user.followers ? "0" : user.following.length}
              </Typography>
            </div>

            <Typography id="transition-modal-description">
              <span style={{ color: "#38b6ff" }}>Họ và tên: </span>{" "}
              {!user.fullname ? (
                <span style={{ color: "#ddd" }}>Đang cập nhật </span>
              ) : (
                user.fullname
              )}
            </Typography>
            <Typography id="transition-modal-description">
              <span style={{ color: "#38b6ff" }}>Số điện thoại: </span>{" "}
              {!user.mobile ? (
                <span style={{ color: "#ddd" }}>Đang cập nhật </span>
              ) : (
                user.mobile
              )}
            </Typography>
            <Typography id="transition-modal-description">
              <span style={{ color: "#38b6ff" }}>Địa chỉ: </span>{" "}
              {!user.address ? (
                <span style={{ color: "#ddd" }}>Đang cập nhật </span>
              ) : (
                user.address
              )}
            </Typography>
            <Typography id="transition-modal-description">
              <span style={{ color: "#38b6ff" }}>Câu chuyện: </span>{" "}
              {!user.story ? (
                <span style={{ color: "#ddd" }}>Đang cập nhật </span>
              ) : (
                user.story
              )}
            </Typography>

            <Typography id="transition-modal-description">
              {!user.website ? (
                <>
                  <span style={{ color: "#38b6ff" }}>Trang web: </span>
                  <span style={{ color: "#ddd" }}> Đang cập nhật</span>
                </>
              ) : (
                <a
                  style={{ color: "#38b6ff" }}
                  href={user.website}
                  target="_blank"
                  rel="noreferrer"
                >
                  {user.website}
                </a>
              )}
            </Typography>
            <Typography id="transition-modal-description">
              <span style={{ color: "#38b6ff" }}>Địa chỉ email: </span>{" "}
              {user.email}
            </Typography>

            <div>
              <p
                onClick={() => handleOpenModalEdit(user)}
                className="imgHover"
                style={{
                  cursor: "pointer",
                  padding: "8px",
                  textAlign: "center",
                  borderBottom: "1px solid #ddd",
                  margin: 0
                }}
              >
                Chỉnh sửa
              </p>
              <p
                onClick={() => onClickAcceptReport(user)}
                className="imgHover"
                style={{
                  cursor: "pointer",
                  padding: "8px 0 0 0",
                  textAlign: "center",
                  margin: 0,
                  color: "red",
                  fontWeight: "bold"
                }}
              >
                Xóa
              </p>
            </div>
          </Box>
        </Fade>
      </Modal>
      {search &&
        aUser.users.filter(
          (user) =>
            user.username.toLowerCase().includes(search.toLowerCase()) ||
            user.fullname.toLowerCase().includes(search.toLowerCase()) ||
            user.email.toLowerCase().includes(search.toLowerCase()) ||
            user.role.toLowerCase().includes(search.toLowerCase())
        ).length === 0 && (
          <p style={{ whiteSpace: "nowrap", color: "rgb(217 5 5)" }}>
            Không có thông tin người dùng này.{" "}
          </p>
        )}
      {aUser.loading ? (
        <div className="d-flex justify-content-center">
          <img
            src={LoadIcon}
            style={{ width: "50px", height: "50px" }}
            alt="Loading..."
            className="loading"
          />
        </div>
      ) : (
        <>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 500 }} aria-label="custom pagination table">
              <TableHead>
                <TableRow>
                  <StyledTableCell align="center">Hình nền</StyledTableCell>

                  <StyledTableCell align="center">
                    Tên tài khoản
                  </StyledTableCell>

                  <StyledTableCell align="center">Họ và tên</StyledTableCell>
                  <StyledTableCell align="center">Quyền</StyledTableCell>
                  <StyledTableCell align="center">Giới tính</StyledTableCell>
                  <StyledTableCell align="center">
                    Người theo dõi
                  </StyledTableCell>
                  <StyledTableCell align="center">
                    Đang theo dõi
                  </StyledTableCell>
                  <StyledTableCell align="center">
                    Số điện thoại
                  </StyledTableCell>
                  <StyledTableCell align="center">Địa chỉ</StyledTableCell>
                  <StyledTableCell align="center">Câu chuyện</StyledTableCell>
                  <StyledTableCell align="center">Trang web</StyledTableCell>

                  <StyledTableCell align="center">
                    Địa chỉ email
                  </StyledTableCell>
                </TableRow>
              </TableHead>
              {aUser.users.length >= 0 && (
                <TableBody>
                  {(search
                    ? aUser.users.filter(
                        (user) =>
                          user.username
                            .toLowerCase()
                            .includes(search.toLowerCase()) ||
                          user.fullname
                            .toLowerCase()
                            .includes(search.toLowerCase()) ||
                          user.email
                            .toLowerCase()
                            .includes(search.toLowerCase()) ||
                          user.role.toLowerCase().includes(search.toLowerCase())
                      )
                    : rowsPerPage > 0
                    ? aUser.users.slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                    : aUser.users
                  ).map((user) => (
                    <TableRow key={user._id} onClick={() => onClickItem(user)}>
                      <TableCell align="center" component="th" scope="row">
                        <Avatar src={user.avatar} size="medium-avatar" />
                      </TableCell>

                      <TableCell
                        style={{ whiteSpace: "nowrap" }}
                        align="center"
                      >
                        {user.username}
                      </TableCell>

                      <TableCell
                        style={{ whiteSpace: "nowrap" }}
                        align="center"
                      >
                        {user.fullname}
                      </TableCell>
                      <TableCell
                        style={{ whiteSpace: "nowrap" }}
                        align="center"
                      >
                        {!user.role ? (
                          <p style={{ color: "#ddd" }}> Updating</p>
                        ) : user.role === "admin" ? (
                          <AdminPanelSettingsIcon color="error" />
                        ) : (
                          <PersonIcon color="primary" />
                        )}
                      </TableCell>
                      <TableCell
                        style={{ whiteSpace: "nowrap" }}
                        align="center"
                      >
                        {!user.gender ? (
                          <p style={{ color: "#ddd" }}> Updating</p>
                        ) : user.gender === "other" ? (
                          <TransgenderIcon />
                        ) : user.gender === "male" ? (
                          <MaleIcon color="info" />
                        ) : (
                          <FemaleIcon color="error" />
                        )}
                      </TableCell>

                      <TableCell
                        style={{ whiteSpace: "nowrap" }}
                        align="center"
                      >
                        {!user.followers ? (
                          <p style={{ color: "#ddd" }}> Updating</p>
                        ) : (
                          user.followers.length
                        )}
                      </TableCell>
                      <TableCell
                        style={{ whiteSpace: "nowrap" }}
                        align="center"
                      >
                        {!user.following ? (
                          <p style={{ color: "#ddd" }}> Updating</p>
                        ) : (
                          user.following.length
                        )}
                      </TableCell>

                      <TableCell
                        style={{ whiteSpace: "nowrap" }}
                        align="center"
                      >
                        {!user.mobile ? (
                          <p style={{ color: "#ddd" }}> Updating</p>
                        ) : (
                          user.mobile
                        )}
                      </TableCell>
                      <TableCell
                        style={{
                          width: "150px !important",
                          whiteSpace: "nowrap"
                        }}
                        align="center"
                      >
                        {!user.address ? (
                          <p style={{ color: "#ddd" }}> Updating</p>
                        ) : (
                          user.address.slice(0, 30) + "..."
                        )}
                      </TableCell>
                      <TableCell
                        style={{ whiteSpace: "nowrap" }}
                        align="center"
                      >
                        {!user.story ? (
                          <p style={{ color: "#ddd" }}> Updating</p>
                        ) : (
                          user.story.slice(0, 30) + "..."
                        )}
                      </TableCell>

                      <TableCell
                        style={{ whiteSpace: "nowrap" }}
                        align="center"
                      >
                        {!user.website ? (
                          <p style={{ color: "#ddd" }}> Updating</p>
                        ) : (
                          <a
                            style={{ color: "#38b6ff" }}
                            href={user.website}
                            target="_blank"
                            rel="noreferrer"
                          >
                            {user.website.slice(0, 30) + "..."}
                          </a>
                        )}
                      </TableCell>
                      <TableCell
                        style={{ whiteSpace: "nowrap" }}
                        align="center"
                      >
                        {user.email}
                      </TableCell>
                    </TableRow>
                  ))}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
              )}
              {/* <TableFooter>
            <TableRow>
            </TableRow>
          </TableFooter> */}
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25, { label: "All", value: -1 }]}
            component="div"
            colSpan={5}
            count={aUser.users.length}
            rowsPerPage={rowsPerPage}
            page={page}
            SelectProps={{
              inputProps: {
                "aria-label": "rows per page"
              },
              native: true
            }}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
            ActionsComponent={TablePaginationActions}
          />
        </>
      )}
    </Paper>
  );
}
