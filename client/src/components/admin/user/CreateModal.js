import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory, Link } from "react-router-dom";
import { register, createUser } from "../../../redux/actions/authAction";
import { checkImage } from "../../../utils/imageUpload";
import { GLOBALTYPES } from "../../../redux/actions/globalTypes";

const CreateModal = ({ handleClose }) => {
  const { auth, alert, theme } = useSelector((state) => state);

  const dispatch = useDispatch();
  const history = useHistory();

  const initialState = {
    role: "user",
    mobile: "",
    story: "",
    address: "",
    website: "",
    fullname: "",
    username: "",
    email: "",
    password: "",
    cf_password: "",
    gender: "male"
  };
  const [userData, setUserData] = useState(initialState);
  const {
    role,
    mobile,
    story,
    address,
    website,
    fullname,
    username,
    email,
    password,
    cf_password,
    gender
  } = userData;

  const [typePass, setTypePass] = useState(false);
  const [typeCfPass, setTypeCfPass] = useState(false);

  // useEffect(() => {
  //   if (auth.token) history.push("/");
  // }, [auth.token, history]);

  const handleChangeInput = (e) => {
    const { name, value } = e.target;
    setUserData({ ...userData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    dispatch(createUser({ userData, picture, auth }));
  };

  const [picture, setPicture] = useState("");
  const changeAvatar = (e) => {
    const file = e.target.files[0];

    const err = checkImage(file);
    if (err)
      return dispatch({ type: GLOBALTYPES.ALERT, payload: { error: err } });
    setPicture(file);
  };
  return (
    <div className="admin_page">
      <form onSubmit={handleSubmit}>
        <div className="info_avatar">
          <img
            src={
              picture
                ? URL.createObjectURL(picture)
                : "https://res.cloudinary.com/devatchannel/image/upload/v1602752402/avatar/avatar_cugq40.png"
            }
            alt="avatar"
            style={{ filter: theme ? "invert(1)" : "invert(0)" }}
          />
          <span>
            <i className="fas fa-camera" />
            <p>Change</p>
            <input
              type="file"
              name="file"
              id="file_up"
              accept="image/*"
              onChange={changeAvatar}
            />
          </span>
        </div>
        <div className="row justify-content-between mx-0 mb-1">
          <label htmlFor="user">
            User:{" "}
            <input
              type="radio"
              id="user"
              name="role"
              value="user"
              defaultChecked
              onChange={handleChangeInput}
            />
          </label>

          <label htmlFor="admin">
            Admin:{" "}
            <input
              type="radio"
              id="admin"
              name="role"
              value="admin"
              onChange={handleChangeInput}
            />
          </label>
        </div>

        <div className="form-group">
          <label htmlFor="fullname">Full Name</label>
          <input
            type="text"
            className="form-control"
            id="fullname"
            name="fullname"
            onChange={handleChangeInput}
            value={fullname}
            style={{ background: `${alert.fullname ? "#fd2d6a14" : ""}` }}
          />

          <small className="form-text text-danger">
            {alert.fullname ? alert.fullname : ""}
          </small>
        </div>

        <div className="form-group">
          <label htmlFor="username">User Name</label>
          <input
            type="text"
            className="form-control"
            id="username"
            name="username"
            onChange={handleChangeInput}
            value={username.toLowerCase().replace(/ /g, "")}
            style={{ background: `${alert.username ? "#fd2d6a14" : ""}` }}
          />

          <small className="form-text text-danger">
            {alert.username ? alert.username : ""}
          </small>
        </div>

        <div className="form-group">
          <label htmlFor="exampleInputEmail1">Email address</label>
          <input
            type="email"
            className="form-control"
            id="exampleInputEmail1"
            name="email"
            onChange={handleChangeInput}
            value={email}
            style={{ background: `${alert.email ? "#fd2d6a14" : ""}` }}
          />

          <small className="form-text text-danger">
            {alert.email ? alert.email : ""}
          </small>
        </div>

        <div className="form-group">
          <label htmlFor="fullname">story</label>
          <textarea
            name="story"
            value={story}
            className="form-control"
            onChange={handleChangeInput}
            cols="30"
            rows="4"
          />
          <small className="text-danger">{story.length}/200</small>

          <small className="form-text text-danger">
            {alert.story ? alert.story : ""}
          </small>
        </div>

        <div className="form-group">
          <label htmlFor="fullname">website</label>
          <input
            type="text"
            className="form-control"
            id="website"
            name="website"
            onChange={handleChangeInput}
            value={website}
            style={{ background: `${alert.website ? "#fd2d6a14" : ""}` }}
          />

          <small className="form-text text-danger">
            {alert.website ? alert.website : ""}
          </small>
        </div>
        <div className="form-group">
          <label htmlFor="fullname">mobile</label>
          <input
            type="text"
            className="form-control"
            id="mobile"
            name="mobile"
            onChange={handleChangeInput}
            value={mobile}
            style={{ background: `${alert.mobile ? "#fd2d6a14" : ""}` }}
          />

          <small className="form-text text-danger">
            {alert.mobile ? alert.mobile : ""}
          </small>
        </div>
        <div className="form-group">
          <label htmlFor="fullname">address</label>
          <input
            type="text"
            className="form-control"
            id="address"
            name="address"
            onChange={handleChangeInput}
            value={address}
            style={{ background: `${alert.address ? "#fd2d6a14" : ""}` }}
          />

          <small className="form-text text-danger">
            {alert.address ? alert.address : ""}
          </small>
        </div>

        <div className="form-group">
          <label htmlFor="exampleInputPassword1">Password</label>

          <div className="pass">
            <input
              type={typePass ? "text" : "password"}
              className="form-control"
              id="exampleInputPassword1"
              onChange={handleChangeInput}
              value={password}
              name="password"
              style={{ background: `${alert.password ? "#fd2d6a14" : ""}` }}
            />

            <small onClick={() => setTypePass(!typePass)}>
              {typePass ? "Hide" : "Show"}
            </small>
          </div>

          <small className="form-text text-danger">
            {alert.password ? alert.password : ""}
          </small>
        </div>

        <div className="form-group">
          <label htmlFor="cf_password">Confirm Password</label>

          <div className="pass">
            <input
              type={typeCfPass ? "text" : "password"}
              className="form-control"
              id="cf_password"
              onChange={handleChangeInput}
              value={cf_password}
              name="cf_password"
              style={{ background: `${alert.cf_password ? "#fd2d6a14" : ""}` }}
            />

            <small onClick={() => setTypeCfPass(!typeCfPass)}>
              {typeCfPass ? "Hide" : "Show"}
            </small>
          </div>

          <small className="form-text text-danger">
            {alert.cf_password ? alert.cf_password : ""}
          </small>
        </div>

        <div className="row justify-content-between mx-0 mb-1">
          <label htmlFor="male">
            Male:{" "}
            <input
              type="radio"
              id="male"
              name="gender"
              value="male"
              defaultChecked
              onChange={handleChangeInput}
            />
          </label>

          <label htmlFor="female">
            Female:{" "}
            <input
              type="radio"
              id="female"
              name="gender"
              value="female"
              onChange={handleChangeInput}
            />
          </label>

          <label htmlFor="other">
            Other:{" "}
            <input
              type="radio"
              id="other"
              name="gender"
              value="other"
              onChange={handleChangeInput}
            />
          </label>
        </div>

        {/* <button type="submit" className="btn btn-dark w-100">
          Create
        </button> */}
        <button
          type="submit"
          className="myCustomButton"
          style={{ "--clr": "#1e9bff", width: "100%" }}
        >
          {" "}
          <span>Thêm</span>
          <i></i>
        </button>
      </form>
    </div>
  );
};

export default CreateModal;
