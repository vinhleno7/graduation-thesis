import * as React from "react";
import { useTheme } from "@mui/material/styles";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";
import TableHead from "@mui/material/TableHead";
import { useSelector, useDispatch } from "react-redux";
import Avatar from "../../Avatar";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import Backdrop from "@mui/material/Backdrop";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import MaleIcon from "@mui/icons-material/Male";
import FemaleIcon from "@mui/icons-material/Female";
import TransgenderIcon from "@mui/icons-material/Transgender";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import PersonIcon from "@mui/icons-material/Person";
import { deleteStory } from "../../../redux/actions/a-storyAction";
import EditModal from "./EditModal";
import LoadIcon from "../../../images/loading.gif";
import { onConfirm } from "react-confirm-pro";

import moment from "moment";
import Carousel from "../../Carousel";
import { GLOBALTYPES } from "../../../redux/actions/globalTypes";

const style = {
  position: "absolute",
  borderRadius: "8px",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4
};

interface TablePaginationActionsProps {
  count: number;
  page: number;
  rowsPerPage: number;
  onPageChange: (
    event: React.MouseEvent<HTMLButtonElement>,
    newPage: number
  ) => void;
}

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14
  }
}));

function TablePaginationActions(props: TablePaginationActionsProps) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

function createData(name: string, calories: number, fat: number) {
  return { name, calories, fat };
}

export default function CustomPaginationActionsTable({ search }) {
  const { aStory, auth, theme } = useSelector((state) => state);
  const dispatch = useDispatch();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0
      ? Math.max(0, (1 + page) * rowsPerPage - aStory.stories.length)
      : 0;

  const handleChangePage = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const [images, setImages] = React.useState([]);

  const [open, setOpen] = React.useState(false);
  const [story, setStory] = React.useState({});
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const onClickItem = (story) => {
    handleOpen();
    setStory(story);
  };
  const handleDeleteStory = (story) => {
    dispatch(deleteStory({ story, auth }));
    handleClose();
  };

  const [openModalEdit, setOpenModalEdit] = React.useState(false);

  const turnOffModalEdit = () => {
    setOpenModalEdit(false);
  };

  const handleOpenModalEdit = (story) => {
    handleClose();
    dispatch({ type: GLOBALTYPES.STATUS, payload: { ...story, onEdit: true } });
  };

  const deleteImages = (index) => {
    const newArr = [...images];

    newArr.splice(index, 1);
    setImages(newArr);
  };
  const CustomUIDelete = ({ onSubmit, onCancel }) => (
    <div className="confirm">
      <h1>
        <strong>Xóa câu chuyện ?</strong>
      </h1>
      <p>
        Bạn có chắc chắn việc <strong>xóa câu chuyện</strong> này không ?
      </p>
      <button onClick={onCancel}>Hủy bỏ</button>
      <button onClick={onSubmit}>Đồng ý xóa</button>
    </div>
  );

  const onClickAcceptReport = (story) => {
    handleClose();
    onConfirm({
      onSubmit: () => {
        dispatch(deleteStory({ story, auth }));
      },
      onCancel: () => {
        handleOpen();
        // alert("Cancel");
      },
      customUI: CustomUIDelete,
      className: "my-custom-ui-container"
    });
  };
  return (
    <Paper sx={{ width: "100%" }}>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={open}>
          {story.images ? (
            <Box sx={style}>
              <div className="story__admin__container">
                <Carousel images={story.images} id={story._id} />
              </div>

              <div className="d-flex justify-content-center align-items-center">
                <Typography
                  sx={{ textAlign: "center" }}
                  id="transition-modal-title"
                  variant="h6"
                  component="h2"
                >
                  <span style={{ color: "#38b6ff" }}>Nội dung: </span>{" "}
                  {story.content}
                </Typography>
              </div>

              <Avatar src={story.user.avatar} size="medium-avatar" />

              <span className="ml-1 "> {story.user.username}</span>

              <div className="d-flex justify-content-center">
                <a
                  href={`/story/${story._id}`}
                  target="_blank"
                  rel="noreferrer"
                >
                  <Typography
                    sx={{ padding: "4px 16px", borderRight: "1px solid #ddd" }}
                    id="transition-modal-description"
                  >
                    <span style={{ color: "#38b6ff" }}>Lượt thích: </span>{" "}
                    {!story.likes ? "0" : story.likes.length}
                  </Typography>
                </a>
              </div>

              <div>
                <p
                  onClick={() => onClickAcceptReport(story)}
                  className="imgHover"
                  style={{
                    cursor: "pointer",
                    padding: "8px 0 0 0",
                    textAlign: "center",
                    margin: 0,
                    color: "red",
                    fontWeight: "bold"
                  }}
                >
                  Xóa
                </p>
              </div>
            </Box>
          ) : (
            <div className="d-flex justify-content-center">
              <img
                src={LoadIcon}
                style={{ width: "50px", height: "50px" }}
                alt="Loading..."
                className="loading"
              />
            </div>
          )}
        </Fade>
      </Modal>
      {search &&
        aStory.stories.filter(
          (story) =>
            story.content.toLowerCase().includes(search.toLowerCase()) ||
            story.user.username.toLowerCase().includes(search.toLowerCase()) ||
            moment(story.createdAt)
              .format("llll")
              .toLowerCase()
              .includes(search.toLowerCase())
        ).length === 0 && (
          <p style={{ whiteSpace: "nowrap", color: "rgb(217 5 5)" }}>
            Không có kết quả cho câu chuyện này.{" "}
          </p>
        )}
      {aStory.loading ? (
        <div className="d-flex justify-content-center">
          <img
            src={LoadIcon}
            style={{ width: "50px", height: "50px" }}
            alt="Loading..."
            className="loading"
          />
        </div>
      ) : (
        <>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 500 }} aria-label="custom pagination table">
              <TableHead>
                <TableRow>
                  <StyledTableCell align="center">Tác giả</StyledTableCell>
                  <StyledTableCell align="center">Hình ảnh</StyledTableCell>

                  <StyledTableCell align="center">Nội dung</StyledTableCell>

                  <StyledTableCell align="center">Lượt thích</StyledTableCell>
                  <StyledTableCell align="center">Ngày tạo</StyledTableCell>
                </TableRow>
              </TableHead>
              {aStory.count >= 0 && (
                <TableBody>
                  {(search
                    ? aStory.stories.filter(
                        (story) =>
                          story.content
                            .toLowerCase()
                            .includes(search.toLowerCase()) ||
                          story.user.username
                            .toLowerCase()
                            .includes(search.toLowerCase()) ||
                          moment(story.createdAt)
                            .format("llll")
                            .toLowerCase()
                            .includes(search.toLowerCase())
                      )
                    : rowsPerPage > 0
                    ? aStory.stories.slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                    : aStory.stories
                  ).map((story) => (
                    <TableRow
                      key={story._id}
                      onClick={() => onClickItem(story)}
                    >
                      <TableCell style={{ whiteSpace: "nowrap" }}>
                        <Avatar src={story.user.avatar} size="medium-avatar" />
                        <span className="ml-1"> {story.user.username}</span>
                      </TableCell>

                      <TableCell
                        className="d-flex  align-items-center"
                        component="th"
                        scope="row"
                      >
                        {story.images.map((image) =>
                          image.url.match(/video/i) ? (
                            <video
                              key={image.url}
                              src={image.url}
                              width="30px"
                              height="30px"
                            />
                          ) : (
                            <Avatar
                              key={image.url}
                              src={image.url}
                              size="medium-avatar"
                            />
                          )
                        )}
                      </TableCell>

                      <TableCell style={{ whiteSpace: "nowrap" }}>
                        {story.content}
                      </TableCell>

                      <TableCell
                        style={{ whiteSpace: "nowrap" }}
                        align="center"
                      >
                        {story.likes.length}
                      </TableCell>

                      <TableCell
                        style={{ whiteSpace: "nowrap" }}
                        align="center"
                      >
                        {moment(story.createdAt).format("llll")}
                      </TableCell>
                    </TableRow>
                  ))}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
              )}
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25, { label: "All", value: -1 }]}
            component="div"
            colSpan={5}
            count={aStory.stories.length}
            rowsPerPage={rowsPerPage}
            page={page}
            SelectProps={{
              inputProps: {
                "aria-label": "rows per page"
              },
              native: true
            }}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
            ActionsComponent={TablePaginationActions}
          />
        </>
      )}
    </Paper>
  );
}
