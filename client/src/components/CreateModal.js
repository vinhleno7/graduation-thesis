import React, { useState, useRef, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { GLOBALTYPES } from "../redux/actions/globalTypes";
import { createAlbum, updateAlbum } from "../redux/actions/albumAction";
import Camera from "../images/camera.svg";
import Image from "../images/Image.svg";
import Icons from "./Icons";
import Autocomplete from "@mui/material/Autocomplete";

import Avatar from "../components/Avatar";
import Close from "../images/icons8-close (1).svg";
import { imageShow, videoShow } from "../utils/mediaShow";

import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

const CreateModal = ({ openFun, closeFun }) => {
  const { auth, theme, status } = useSelector((state) => state);
  const dispatch = useDispatch();
  const [content, setContent] = useState("");
  const [images, setImages] = useState([]);

  const [stream, setStream] = useState(false);
  const videoRef = useRef();
  const refCanvas = useRef();
  const [tracks, setTracks] = useState("");
  const [friend, setFriend] = useState([]);

  const myIIDE = () => {
    let result = [];

    // for (const userFollower of auth.user.followers) {
    //   tempt = auth.user.following.filter((us) => us._id !== userFollower._id);
    // }
    // console.log(tempt)
    const myFinallyResult = auth.user.followers.filter(
      (a) => !auth.user.following.map((b) => b._id).includes(a._id)
    );
    result = myFinallyResult.concat(auth.user.following);
    return result;
  };

  const myListFriend = auth.user ? myIIDE() : [];
  const [myFriend, setMyFriend] = useState([]);

  useEffect(() => {
    if (status.onEditAlbum) {
      setContent(status.name);
      setImages(status.images);
      setMode(status.visibility);
      if (status.friend.length > 0) {
        setFriend(status.friend);
        status.friend.map((us) => setMyFriend(...myFriend, us._id));
      }
    }
  }, [status]);

  const handleChangeImages = (e) => {
    const files = [...e.target.files];
    let err = "";
    let newImages = [];
    files.forEach((file) => {
      if (!file) return (err = "Tập tin không tồn tại.");

      if (file.size > 1024 * 1024 * 5) {
        return (err = "Tập tin này đã quá 5mb.");
      }

      return newImages.push(file);
    });

    if (err) toast.error(err);

    setImages([...images, ...newImages]);
  };

  const deleteImages = (index) => {
    const newArr = [...images];

    newArr.splice(index, 1);
    setImages(newArr);
  };

  const handleStream = () => {
    setStream(true);
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices
        .getUserMedia({ video: true })
        .then((mediaStream) => {
          videoRef.current.srcObject = mediaStream; // Lấy đường dẫn của đối tượng đó
          videoRef.current.play(); // Bật <video/> lên

          const track = mediaStream.getTracks(); // Âm thanh
          setTracks(track[0]);
        })
        .catch((err) => console.log(err));
    }
  };

  const handleCapture = () => {
    const width = videoRef.current.clientWidth;
    const height = videoRef.current.clientHeight;

    refCanvas.current.setAttribute("width", width);
    refCanvas.current.setAttribute("height", height);
    const ctx = refCanvas.current.getContext("2d");
    ctx.drawImage(videoRef.current, 0, 0, width, height);
    let URL = refCanvas.current.toDataURL();
    setImages([...images, { camera: URL }]);
  };

  const handleStopStream = () => {
    tracks.stop();
    setStream(false);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (images.length === 0)
      return dispatch({
        type: GLOBALTYPES.ALERT,
        payload: { error: "Làm ơn chọn tập tin. " }
      });

    closeFun();
    if (status.onEditAlbum) {
      const id = status._id;
      dispatch(
        updateAlbum({ id, status, content, images, mode, myFriend, auth })
      );
    } else {
      dispatch(
        createAlbum({
          content,
          images,
          mode,
          myFriend,
          auth
        })
      );
    }

    setContent("");
    setImages([]);
    if (tracks) tracks.stop();
    dispatch({ type: GLOBALTYPES.STATUS, payload: false });
  };

  const [mode, setMode] = useState("public");
  const [open, setOpen] = useState(false);
  const Mode1 = "public";
  const Mode2 = "private";
  const Mode3 = "friend";

  const handleChange = (event: SelectChangeEvent<typeof mode>) => {
    setMode(event.target.value);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };
  return (
    <div className="status_modal" style={{ background: "transparent" }}>
      <form onSubmit={handleSubmit}>
        <div className="status_header ">
          <Avatar
            src={auth.user.avatar}
            size="medium-avatar"
            style={{ marginBottom: "4px" }}
          />
          <p style={{ margin: "0px 0 0 8px", fontWeight: 500 }}>
            {auth.user.username}
          </p>
        </div>

        <div className="status_body">
          <FormControl sx={{ m: 1, minWidth: 120 }}>
            <InputLabel id="demo-controlled-open-select-label">
              Chế độ
            </InputLabel>
            <Select
              labelId="demo-controlled-open-select-label"
              id="demo-controlled-open-select"
              open={open}
              onClose={handleClose}
              onOpen={handleOpen}
              value={mode}
              label="Mode"
              onChange={handleChange}
            >
              <MenuItem value={Mode1}>Công khai</MenuItem>
              <MenuItem value={Mode2}>Riêng tư</MenuItem>
              <MenuItem value={Mode3}>Bạn bè</MenuItem>
            </Select>
          </FormControl>
          {mode === "friend" && (
            <Autocomplete
              className="animate delay-1k"
              multiple
              limitTags={2}
              id="country-select-demo"
              sx={{ margin: "4px 0" }}
              options={myListFriend}
              defaultValue={myListFriend.filter((a) =>
                friend.map((b) => b._id).includes(a._id)
              )}
              onChange={(event, value) => setMyFriend(value)}
              autoHighlight
              getOptionLabel={(user) => user.username}
              renderOption={(props, user) => (
                <Box
                  component="li"
                  sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                  {...props}
                >
                  <img
                    loading="lazy"
                    width="20"
                    src={user.avatar}
                    srcSet={user.avatar}
                    alt="avatar user"
                  />
                  {user.username}
                </Box>
              )}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Choose your friends"
                  inputProps={{
                    ...params.inputProps,
                    autoComplete: "new-password" // disable autocomplete and autofill
                  }}
                />
              )}
            />
          )}

          <textarea
            name="content"
            value={content}
            placeholder={` ${auth.user.username}, Điền tên album của bạn... `}
            onChange={(e) => setContent(e.target.value)}
          />

          <div className="d-flex">
            <div className="flex-fill"></div>
            <Icons setContent={setContent} content={content} theme={theme} />
          </div>

          <div className="show_images">
            {images.map((img, index) => (
              <div key={index} id="file_img">
                {img.camera ? (
                  imageShow(img.camera, theme)
                ) : img.url ? (
                  <>
                    {img.url.match(/video/i)
                      ? videoShow(img.url, theme)
                      : imageShow(img.url, theme)}
                  </>
                ) : (
                  <>
                    {img.type.match(/video/i)
                      ? videoShow(URL.createObjectURL(img), theme)
                      : imageShow(URL.createObjectURL(img), theme)}
                  </>
                )}
                <span onClick={() => deleteImages(index)}>
                  <img src={Close} alt="Close" />
                </span>
              </div>
            ))}
          </div>

          {stream && (
            <div className="stream position-relative">
              <video
                autoPlay
                muted
                ref={videoRef}
                width="100%"
                height="100%"
                style={{ filter: theme ? "invert(1)" : "invert(0)" }}
              />
              <span onClick={handleStopStream}>
                <img src={Close} alt="Close" />
              </span>
              <canvas ref={refCanvas} style={{ display: "none" }} />
            </div>
          )}

          <div className="input_images">
            {stream ? (
              <img
                className="button camera"
                style={{ height: "35px", width: "35px", cursor: "pointer" }}
                src={Camera}
                alt="camera"
                onClick={handleCapture}
              />
            ) : (
              <>
                <img
                  className="button camera"
                  style={{ height: "35px", width: "35px", cursor: "pointer" }}
                  src={Camera}
                  alt="camera"
                  onClick={handleStream}
                />
                <div className="file_upload">
                  <img
                    className="button"
                    style={{ height: "35px", width: "35px", cursor: "pointer" }}
                    src={Image}
                    alt="this is my upload pic"
                  />
                  <input
                    className="buttonFile"
                    type="file"
                    name="file"
                    id="file"
                    multiple
                    accept="image/*,video/*"
                    onChange={handleChangeImages}
                  />
                </div>
              </>
            )}
          </div>
        </div>

        <div className="status_footer ">
          {/* <button className="btn btn-secondary w-100" type="submit">
            Share
          </button> */}
          {status.onEditAlbum ? (
            <button
              type="submit"
              className="myCustomButton"
              style={{ "--clr": "#6eff3e", width: "100%" }}
            >
              {" "}
              <span>Sửa</span>
              <i></i>
            </button>
          ) : (
            <button
              type="submit"
              className="myCustomButton"
              style={{ "--clr": "#1e9bff", width: "100%" }}
            >
              {" "}
              <span>Thêm</span>
              <i></i>
            </button>
          )}
        </div>
      </form>
    </div>
  );
};

export default CreateModal;
