// import story from "../../utils/Story.json";
import Story from "./Story";
import { BiChevronRight, BiChevronLeft } from "react-icons/bi";
import { useRef, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { GLOBALTYPES } from "../../redux/actions/globalTypes";

function Stories() {
  const { story } = useSelector((state) => state);

  const dispatch = useDispatch();

  // useEffect(() => {
  //   dispatch(getStories(auth.token));
  // }, [dispatch, auth.token]);

  const storiesRef = useRef(null);
  const [showLeft, setShowLeft] = useState(false);
  const [showRight, setShowRight] = useState(true);

  const onScroll = () => {
    if (
      Math.round(storiesRef.current.scrollLeft + 0.3) ===
      Math.round(
        storiesRef.current.scrollWidth - storiesRef.current.clientWidth
      )
    ) {
      setShowRight(false);
    } else {
      setShowRight(true);
    }
    if (storiesRef.current.scrollLeft > 0) {
      setShowLeft(true);
    } else {
      setShowLeft(false);
    }
  };
  return (
    <div
      id="list_user_story"
      style={{
        boxShadow: " 0 0 5px #ddd",
        padding: "20px",
        borderRadius: "5px",
        border: " 1px solid rgb(0, 0, 0, 0.125)"
      }}
      className="position-relative"
    >
      {story.stories.length > 0 ? (
        <div
          style={{
            background: "transparent",
            overflow: "hidden",
            scrollBehavior: "smooth"
            // overflowX: "scroll"
          }}
          className="d-flex
        "
          onScroll={onScroll}
          ref={storiesRef}
        >
          {story.stories.map((item, index) => (
            <Story
              key={index}
              myStoryId={item.stories}
              id={item.username}
              isReadModal={item.stories[0]}
              name={
                item.username.length > 11
                  ? `${item.username.slice(0, 9)}...`
                  : item.username
              }
              image={item.avatar}
            />
          ))}
        </div>
      ) : (
        <p
          onClick={() => dispatch({ type: GLOBALTYPES.STATUS, payload: true })}
          className="imgHover"
          style={{ color: "black", cursor: "pointer" }}
        >
          Không có câu chuyện nào cả. Hãy tạo 1 câu chuyện riêng của bạn.
        </p>
      )}
      {story.stories.length > 0 && (
        <div
          style={{
            top: "13px",
            left: 0,
            position: " absolute",
            outline: "none"
          }}
          className=" w-100 p-4
        d-flex align-items-center justify-content-between"
        >
          <button
            style={{
              zIndex: "1",
              background: "transparent",
              outline: "none",
              border: "none"
            }}
            onClick={() => {
              storiesRef.current.scrollLeft =
                storiesRef.current.scrollLeft - 600;
            }}
          >
            <BiChevronLeft
              style={{
                cursor: "pointer",
                background: "white",
                color: "gray",
                fontWeight: 400,
                width: "1.75rem",
                height: "1.75rem",
                borderRadius: "9999px",
                boxShadow:
                  " 0 10px 15px -3px rgb(0 0 0 / 0.1), 0 4px 6px -4px rgb(0 0 0 / 0.1)"
              }}
              className={`
          ${showLeft ? "visible" : "invisible"}`}
            />
          </button>
          <button
            style={{
              background: "transparent",
              outline: "none",
              border: "none",
              zIndex: "1"
            }}
            onClick={() => {
              storiesRef.current.scrollLeft =
                storiesRef.current.scrollLeft + 600;
            }}
          >
            <BiChevronRight
              style={{
                cursor: "pointer",
                background: "white",
                color: "gray",
                fontWeight: 400,
                width: "1.75rem",
                height: "1.75rem",
                borderRadius: "9999px",
                boxShadow:
                  " 0 10px 15px -3px rgb(0 0 0 / 0.1), 0 4px 6px -4px rgb(0 0 0 / 0.1)"
              }}
              className={`${showRight ? "visible" : "invisible"}`}
            />
          </button>
        </div>
      )}
    </div>
  );
}

export default Stories;
