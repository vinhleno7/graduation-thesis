import React from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { isReadStory } from "../../redux/actions/storyAction";

function Story({ myStoryId, id, image, name, isReadModal }) {
  const { auth } = useSelector((state) => state);
  const dispatch = useDispatch();

  const handleIsRead = (id) => {
    dispatch(isReadStory({ id, auth }));
  };
  const checkIsRead = () => {
    return isReadModal.isRead.find((item) => item === auth.user._id)
      ? true
      : false;
  };
  return (
    <div
      className="d-flex justify-content-center flex-column align-items-center"
      style={{ zIndex: 1, margin: "0 6px 0 0" }}
    >
      <Link
        to={`/stories/${id}`}
        className="d-flex justify-content-center"
        onClick={() => handleIsRead(myStoryId[0]._id)}
      >
        <img
          style={{
            width: "3.5rem",
            height: "3.5rem",
            objectFit: "cover",
            border: `${
              checkIsRead() ? "2px solid #e0e0e0 " : "2px solid #e56898"
            }`,
            cursor: "pointer",
            padding: "1.5px",
            borderRadius: "999px"
          }}
          src={image}
          alt=""
        />
      </Link>
      <h1
        style={{
          fontSize: "0.75rem",
          lineHeight: "1rem",
          width: "4rem",
          overflow: "hidden",
          textOverflow: "ellipsis",
          whiteSpace: "nowrap",
          textAlign: "center"
        }}
        className=" d-flex"
      >
        {name}
      </h1>
    </div>
  );
}

export default Story;
