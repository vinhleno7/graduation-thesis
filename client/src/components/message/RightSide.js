import React, { useState, useEffect, useRef } from "react";
import UserCard from "../UserCard";
import { useSelector, useDispatch } from "react-redux";
import { useParams, useHistory } from "react-router-dom";
import MsgDisplay from "./MsgDisplay";
import Icons from "../Icons";
import { GLOBALTYPES } from "../../redux/actions/globalTypes";
import { imageShow, videoShow } from "../../utils/mediaShow";
import { imageUpload } from "../../utils/imageUpload";
import Camera from "../../images/camera.svg";
import {
  addMessage,
  getMessages,
  loadMoreMessages,
  deleteConversation
} from "../../redux/actions/messageAction";
import LoadIcon from "../../images/loading.gif";
import iconPhone from "../../images/icons8-call-30.png";
import iconVideoCall from "../../images/icons8-video-call-30.png";
import iconSend from "../../images/icons8-sent-24.png";
import iconTrash from "../../images/icons8-trash.svg";
import Image from "../../images/Image.svg";
import { onConfirm } from "react-confirm-pro";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

const RightSide = () => {
  const { auth, message, theme, socket, peer } = useSelector((state) => state);
  const dispatch = useDispatch();

  const { id } = useParams();
  const [user, setUser] = useState([]);
  const [text, setText] = useState("");
  const [media, setMedia] = useState([]);
  const [loadMedia, setLoadMedia] = useState(false);

  const refDisplay = useRef();
  const pageEnd = useRef();

  const [data, setData] = useState([]);
  const [result, setResult] = useState(9);
  const [page, setPage] = useState(0);
  const [isLoadMore, setIsLoadMore] = useState(0);
  const [stream, setStream] = useState(false);

  const history = useHistory();
  const videoRef = useRef();
  const refCanvas = useRef();
  const [tracks, setTracks] = useState("");

  // const handleChangeImages = (e) => {
  //   const files = [...e.target.files];
  //   let err = "";
  //   let newImages = [];
  //   files.forEach((file) => {
  //     if (!file) return (err = "File does not exist.");

  //     if (file.size > 1024 * 1024 * 5) {
  //       return (err = "The image/video largest is 5mb.");
  //     }

  //     return newImages.push(file);
  //   });

  //   if (err) toast.error(err);

  //   setImages([...images, ...newImages]);
  // };

  // const deleteImages = (index) => {
  //   const newArr = [...images];

  //   newArr.splice(index, 1);
  //   setImages(newArr);
  // };

  const handleStream = () => {
    setStream(true);
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices
        .getUserMedia({ video: true })
        .then((mediaStream) => {
          videoRef.current.srcObject = mediaStream;
          videoRef.current.play();

          const track = mediaStream.getTracks();
          setTracks(track[0]);
        })
        .catch((err) => console.log(err));
    }
  };

  const handleCapture = () => {
    const width = videoRef.current.clientWidth;
    const height = videoRef.current.clientHeight;

    refCanvas.current.setAttribute("width", width);

    refCanvas.current.setAttribute("height", height);

    const ctx = refCanvas.current.getContext("2d");

    ctx.drawImage(videoRef.current, 0, 0, width, height);
    let URL = refCanvas.current.toDataURL();

    setMedia([...media, { camera: URL }]);
  };

  const handleStopStream = () => {
    tracks.stop();
    setStream(false);
  };
  useEffect(() => {
    const newData = message.data.find((item) => item._id === id);
    if (newData) {
      setData(newData.messages);
      setResult(newData.result);
      setPage(newData.page);
    }
  }, [message.data, id]);

  useEffect(() => {
    if (id && message.users.length > 0) {
      setTimeout(() => {
        refDisplay.current.scrollIntoView({ behavior: "smooth", block: "end" });
      }, 50);

      const newUser = message.users.find((user) => user._id === id);
      if (newUser) setUser(newUser);
    }
  }, [message.users, id]);

  const handleChangeMedia = (e) => {
    const files = [...e.target.files];
    let err = "";
    let newMedia = [];

    files.forEach((file) => {
      if (!file) return (err = "Tập tin không tồn tại.");

      if (file.size > 1024 * 1024 * 5) {
        return (err = "Tập tin của bạn quá 5MB.");
      }

      return newMedia.push(file);
    });

    if (err) toast.error(err);
    setMedia([...media, ...newMedia]);
  };

  const handleDeleteMedia = (index) => {
    const newArr = [...media];
    newArr.splice(index, 1);
    setMedia(newArr);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!text.trim() && media.length === 0) return;
    setText("");
    setMedia([]);
    setLoadMedia(true);

    let newArr = [];
    if (media.length > 0) newArr = await imageUpload(media);

    const msg = {
      sender: auth.user._id,
      recipient: id,
      text,
      media: newArr,
      createdAt: new Date().toISOString()
    };

    setLoadMedia(false);
    await dispatch(addMessage({ msg, auth, socket }));
    if (refDisplay.current) {
      refDisplay.current.scrollIntoView({ behavior: "smooth", block: "end" });
    }
  };

  useEffect(() => {
    const getMessagesData = async () => {
      if (message.data.every((item) => item._id !== id)) {
        await dispatch(getMessages({ auth, id }));
        setTimeout(() => {
          refDisplay.current.scrollIntoView({
            behavior: "smooth",
            block: "end"
          });
        }, 50);
      }
    };
    getMessagesData();
  }, [id, dispatch, auth, message.data]);

  // Load More
  useEffect(() => {
    const observer = new IntersectionObserver(
      (entries) => {
        if (entries[0].isIntersecting) {
          setIsLoadMore((p) => p + 1);
        }
      },
      {
        threshold: 0.1
      }
    );

    observer.observe(pageEnd.current);
  }, [setIsLoadMore]);

  useEffect(() => {
    if (isLoadMore > 1) {
      if (result >= page * 9) {
        dispatch(loadMoreMessages({ auth, id, page: page + 1 }));
        setIsLoadMore(1);
      }
    }
    // eslint-disable-next-line
  }, [isLoadMore]);

  const CustomUIDelete = ({ onSubmit, onCancel }) => (
    <div className="confirm">
      <h1>
        <strong>Xóa đoạn nói chuyện?</strong>
      </h1>
      <p>
        Bạn có muốn <strong>xóa</strong>đoạn nói chuyện này?
      </p>
      <button onClick={onCancel}>Hủy bỏ</button>
      <button onClick={onSubmit}>Xóa</button>
    </div>
  );

  const handleDeleteConversation = () => {
    onConfirm({
      onSubmit: () => {
        dispatch(deleteConversation({ auth, id }));
        return history.push("/message");
      },
      onCancel: () => {
        // alert("Cancel");
      },
      customUI: CustomUIDelete,
      className: "my-custom-ui-container"
    });
  };

  // Call
  const caller = ({ video }) => {
    const { _id, avatar, username, fullname } = user;

    const msg = {
      sender: auth.user._id,
      recipient: _id,
      avatar,
      username,
      fullname,
      video
    };
    dispatch({ type: GLOBALTYPES.CALL, payload: msg });
  };

  const callUser = ({ video }) => {
    const { _id, avatar, username, fullname } = auth.user;

    const msg = {
      sender: _id,
      recipient: user._id,
      avatar,
      username,
      fullname,
      video
    };

    if (peer.open) msg.peerId = peer._id;
    // Gui loi yeu cau cuoc goi cho doi phuong
    socket.emit("callUser", msg);
  };

  const handleAudioCall = () => {
    caller({ video: false });
    callUser({ video: false });
  };

  const handleVideoCall = () => {
    caller({ video: true });
    callUser({ video: true });
  };

  const handleLove = async (e) => {
    e.preventDefault();
    const icon = "❤️";

    const msg = {
      sender: auth.user._id,
      recipient: id,
      text: icon,

      createdAt: new Date().toISOString()
    };

    await dispatch(addMessage({ msg, auth, socket }));
    if (refDisplay.current) {
      refDisplay.current.scrollIntoView({ behavior: "smooth", block: "end" });
    }
  };

  return (
    <>
      <div className="message_header" style={{ cursor: "pointer" }}>
        {user.length !== 0 && (
          <UserCard user={user} size="big-avatar">
            <span
              style={{
                position: "absolute",
                fontSize: " 0.6rem",
                top: " 2.3rem",
                color: "green",
                left: "4rem"
              }}
            >
              {auth.user.following.find((item) => item._id === user._id)
                ? user.online
                  ? "Active now"
                  : "Offline"
                : `You have not follow this one.`}
            </span>
            <div>
              {/* <i className="fas fa-phone-alt" onClick={handleAudioCall} /> */}
              <img
                className="imgHover"
                src={iconPhone}
                style={{ margin: "0 8px" }}
                alt="iconPhone"
                onClick={handleAudioCall}
              />

              <img
                className="imgHover"
                src={iconVideoCall}
                alt="iconVideoCall"
                onClick={handleVideoCall}
                style={{ margin: "0 8px" }}
              />
              {/* <i className="fas fa-video mx-3" onClick={handleVideoCall} /> */}

              <img
                className="imgHover"
                src={iconTrash}
                alt="iconVideoCall"
                // style={{ margin: "0 8px" }}
                onClick={handleDeleteConversation}
              />
              {/* <i
                className="fas fa-trash text-danger"
                onClick={handleDeleteConversation}
              /> */}
            </div>
          </UserCard>
        )}
      </div>

      <div
        className="chat_container"
        style={{
          height: media.length > 0 || stream ? "calc(100% - 360px)" : ""
        }}
      >
        <div className="chat_display" ref={refDisplay}>
          <button style={{ marginTop: "-25px", opacity: 0 }} ref={pageEnd}>
            Tải thêm
          </button>

          {data.map((msg, index) => (
            <div key={index}>
              {msg.sender !== auth.user._id && (
                <div className="chat_row other_message">
                  <MsgDisplay user={user} msg={msg} theme={theme} />
                </div>
              )}

              {msg.sender === auth.user._id && (
                <div className="chat_row you_message">
                  <MsgDisplay
                    user={auth.user}
                    msg={msg}
                    theme={theme}
                    data={data}
                  />
                </div>
              )}
            </div>
          ))}

          {loadMedia && (
            <div className="chat_row you_message d-flex justify-content-center">
              <img src={LoadIcon} alt="loading" style={{ width: "50px" }} />
            </div>
          )}
        </div>
      </div>

      <div className="d-flex position-relative">
        {stream && (
          <div className="stream position-relative">
            <video
              autoPlay
              muted
              ref={videoRef}
              width="100%"
              height="100%"
              style={{ filter: theme ? "invert(1)" : "invert(0)" }}
            />
            <span className="span__close" onClick={handleStopStream}>
              &times;
            </span>
            <canvas ref={refCanvas} style={{ display: "none" }} />
          </div>
        )}
        <div
          className="show_media"
          style={{ display: media.length > 0 || stream ? "grid" : "none" }}
        >
          {media.map((item, index) => (
            <div key={index} id="file_media">
              {item.camera ? (
                imageShow(item.camera, theme)
              ) : item.url ? (
                <>
                  {item.url.match(/video/i)
                    ? videoShow(item.url, theme)
                    : imageShow(item.url, theme)}
                </>
              ) : (
                <>
                  {item.type.match(/video/i)
                    ? videoShow(URL.createObjectURL(item), theme)
                    : imageShow(URL.createObjectURL(item), theme)}
                </>
              )}
              <span onClick={() => handleDeleteMedia(index)}>&times;</span>
            </div>
          ))}
        </div>
      </div>

      <form className="chat_input" onSubmit={handleSubmit}>
        <Icons setContent={setText} content={text} theme={theme} />

        <input
          type="text"
          placeholder="Aa"
          value={text}
          onChange={(e) => setText(e.target.value)}
          style={{
            // filter: theme ? "invert(1)" : "invert(0)",
            background: theme ? "#040404" : "",
            color: theme ? "white" : ""
          }}
        />

        <div className="file_upload">
          {/* <img
            className="button"
            style={{ height: "27px", width: "50px", cursor: "pointer" }}
            src={Image}
            alt="this is my upload pic"
          />
          <input
            type="file"
            name="file"
            id="file"
            multiple
            accept="image/*,video/*"
            onChange={handleChangeMedia}
          /> */}
          {stream ? (
            <img
              className="button camera"
              style={{ height: "35px", width: "35px", cursor: "pointer" }}
              src={Camera}
              alt="camera"
              onClick={handleCapture}
            />
          ) : (
            <>
              <img
                className="button camera"
                style={{
                  height: "35px",
                  width: "35px",
                  cursor: "pointer",
                  marginRight: "8px"
                }}
                src={Camera}
                alt="camera"
                onClick={handleStream}
              />
              <div className="file_upload">
                <img
                  className="button"
                  style={{
                    height: "35px",
                    width: "35px",
                    cursor: "pointer",
                    marginRight: "25px"
                  }}
                  src={Image}
                  alt="this is my upload pic"
                />
                <input
                  className="buttonFile"
                  type="file"
                  name="file"
                  id="file"
                  multiple
                  accept="image/*,video/*"
                  onChange={handleChangeMedia}
                />
              </div>
            </>
          )}
        </div>
        <span
          className="imgHover"
          onClick={handleLove}
          style={{
            cursor: "pointer",
            fontSize: "1.5rem",
            paddingTop: "7px",
            margin: "0 8px 0 8px"
          }}
        >
          <ion-icon name="heart-outline"></ion-icon>
        </span>
        {(text || media.length > 0) && (
          <button
            type="submit"
            // disabled={text || media.length > 0 ? false : true}
          >
            <img src={iconSend} alt="icon" />
          </button>
        )}
      </form>
    </>
  );
};

export default RightSide;
