import React from "react";
import Avatar from "../Avatar";
import { imageShow, videoShow } from "../../utils/mediaShow";
import { useSelector, useDispatch } from "react-redux";
import { deleteMessages } from "../../redux/actions/messageAction";
import Times from "./Times";
import { onConfirm } from "react-confirm-pro";

const MsgDisplay = ({ user, msg, theme, data }) => {
  const { auth } = useSelector((state) => state);
  const dispatch = useDispatch();

  const CustomUIDelete = ({ onSubmit, onCancel }) => (
    <div className="confirm">
      <h1>
        <strong>Xóa tin nhắn ?</strong>
      </h1>
      <p>
        Bạn có chắc muốn <strong>xóa</strong>tin nhắn này không?
      </p>
      <button onClick={onCancel}>Hủy bỏ</button>
      <button onClick={onSubmit}>Xóa</button>
    </div>
  );

  const handleDeleteMessages = () => {
    onConfirm({
      onSubmit: () => {
        if (!data) return;

        dispatch(deleteMessages({ msg, data, auth }));
      },
      onCancel: () => {
        // alert("Cancel");
      },
      customUI: CustomUIDelete,
      className: "my-custom-ui-container"
    });
  };

  const handleTimeChat = (time) => {
    let result = "";

    if (
      new Date(msg.createdAt).toLocaleDateString() ===
      new Date().toLocaleDateString()
    ) {
      result = `At ${new Date(msg.createdAt)
        .toLocaleString()
        .substring(
          new Date(msg.createdAt).toLocaleString().indexOf(",") + 1,
          new Date(msg.createdAt).toLocaleString().length
        )
        .trim()}`;
    } else {
      result = `In ${new Date(msg.createdAt)
        .toLocaleString()
        .substring(0, new Date(msg.createdAt).toLocaleString().indexOf(","))
        .trim()}, At ${new Date(msg.createdAt)
        .toLocaleString()
        .substring(
          new Date(msg.createdAt).toLocaleString().indexOf(",") + 1,
          new Date(msg.createdAt).toLocaleString().length
        )
        .trim()}`;
    }
    return result;
  };

  return (
    <>
      <div className="d-flex align-items-center">
        {user._id !== auth.user._id && (
          <div className="chat_title">
            <Avatar src={user.avatar} size="small-avatar" />
            {/* <span>{user.username}</span> */}
          </div>
        )}

        <div className="you_content ml-2">
          {user._id === auth.user._id && (
            <i
              className="fas fa-trash text-danger"
              onClick={handleDeleteMessages}
            />
          )}
        </div>

        <div>
          {msg.text && (
            <div
              className="chat_text"
              style={{ filter: theme ? "invert(1)" : "invert(0)" }}
            >
              {msg.text}
            </div>
          )}
          {msg.media.map((item, index) => (
            <div key={index} className="chat_text">
              {item.url.match(/video/i)
                ? videoShow(item.url, theme)
                : imageShow(item.url, theme)}
            </div>
          ))}
        </div>

        {msg.call && (
          <button
            className="btn d-flex align-items-center py-3"
            style={{ background: "#eee", borderRadius: "10px" }}
          >
            <span
              className="material-icons font-weight-bold mr-1"
              style={{
                fontSize: "2.5rem",
                color: msg.call.times === 0 ? "crimson" : "green",
                filter: theme ? "invert(1)" : "invert(0)"
              }}
            >
              {msg.call.times === 0
                ? msg.call.video
                  ? "videocam_off"
                  : "phone_disabled"
                : msg.call.video
                ? "video_camera_front"
                : "call"}
            </span>

            <div className="text-left">
              <h6>{msg.call.video ? "Video Call" : "Audio Call"}</h6>
              <small>
                {msg.call.times > 0 ? (
                  <Times total={msg.call.times} />
                ) : (
                  new Date(msg.createdAt).toLocaleTimeString()
                )}
              </small>
            </div>
          </button>
        )}
      </div>

      <div className="chat_time">
        {handleTimeChat(msg.createdAt)}
        {/* {new Date(msg.createdAt).toLocaleString()} */}
      </div>
    </>
  );
};

export default MsgDisplay;
