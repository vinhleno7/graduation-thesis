import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import UserCard from "../UserCard";
import FollowBtn from "../FollowBtn";

import { logout } from "../../redux/actions/authAction";

import { getSuggestions } from "../../redux/actions/suggestionsAction";
import { getUserHobby } from "../../redux/actions/hobbyAction";

import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import Hobby from "./hobby/Hobby";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "58%",
  bgcolor: "background.paper",
  boxShadow: 24,
  borderRadius: 5,
  outline: "none"
};

const RightSideBar = () => {
  const { auth, suggestions, hobby } = useSelector((state) => state);
  const dispatch = useDispatch();

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  useEffect(() => {
    dispatch(getUserHobby(auth.token));
  }, [auth.token]);

  const handleSuggest = (e) => {
    e.preventDefault();
    dispatch(getSuggestions(auth.token));
  };

  return (
    <div className="my-4">
      <div
        className="d-flex align-items-center "
        style={{ fontSize: "0.75rem" }}
      >
        <UserCard user={auth.user} size="big-avatar" />
        <span
          onClick={dispatch(logout)}
          className="imgHover"
          style={{
            fontWeight: 600,
            color: "#0095F6",
            cursor: "pointer",
            marginRight: "2.5rem",
            whiteSpace: "nowrap"
          }}
        >
          Đăng xuất
        </span>
      </div>

      {hobby.myHobby.length === 0 ? (
        <>
          <button
            onClick={handleOpen}
            className="myCustomButton"
            style={{ "--clr": "#1e9bff", width: "100%" }}
          >
            {" "}
            <span>Chọn sở thích của bạn</span>
            <i></i>
          </button>
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500
            }}
          >
            <Fade in={open}>
              <Box sx={style}>
                <Hobby handleCloseHobby={handleClose} />
              </Box>
            </Fade>
          </Modal>
        </>
      ) : (
        <>
          <p style={{ fontWeight: 600, margin: 0, color: "#8E8E8E" }}>
            Sở thích
          </p>
          <div
            className="d-flex  flex-wrap"
            style={{ paddingLeft: "8px", width: "20rem" }}
          >
            {hobby.myHobby.map((item) => (
              <div
                key={item._id}
                style={{
                  fontSize: "0.75rem",
                  marginRight: "6px",
                  marginBottom: "2px",
                  backgroundColor: "black",
                  color: "white",
                  padding: "2px",
                  whiteSpace: "nowrap",
                  borderRadius: "5px"
                }}
              >
                #{item.name}
              </div>
            ))}
          </div>
          <div className="d-flex justify-content-between align-items-center my-2">
            <p style={{ fontWeight: 600, margin: 0, color: "#8E8E8E" }}>
              Đề xuất kết bạn
            </p>
            <span style={{ marginRight: "3rem" }}>
              {!suggestions.loading ? (
                <i
                  className="fas fa-redo"
                  style={{ cursor: "pointer" }}
                  onClick={(e) => handleSuggest(e)}
                />
              ) : (
                <i className="fas fa-circle-notch fa-spin"></i>
              )}
            </span>
          </div>
          <div className="suggestions" style={{ fontSize: "0.75rem" }}>
            {suggestions.users.slice(0, 5).map((user) => (
              <UserCard user={user} key={user._id} size="avatar-32px">
                <FollowBtn user={user} />
              </UserCard>
            ))}
          </div>
        </>
      )}

      <div style={{ opacity: 0.5 }} className="my-2">
        <a
          href="https://www.facebook.com/caubedammebongda/"
          target="_blank"
          rel="noreferrer"
        >
          {" "}
          https://www.facebook.com/caubedammebongda/
        </a>
        <small className="d-block mt-2" style={{ fontSize: "0.75rem" }}>
          {" "}
          Chào mừng đến với "LV Group | VietNam"
        </small>
        <small style={{ textTransform: "uppercase" }}>
          &copy; 2022 LV Social từ LV Dev
        </small>
      </div>
    </div>
  );
};

export default RightSideBar;
