import React, { useState } from "react";
import Avatar from "../../Avatar";
import { Link, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import moment from "moment";
import "moment/locale/vi";
import InfoModal from "../../InfoModal";
import { GLOBALTYPES } from "../../../redux/actions/globalTypes";
import { deletePost } from "../../../redux/actions/postAction";
import { BASE_URL } from "../../../utils/config";
import { onConfirm } from "react-confirm-pro";

moment.locale("vi");

const CardHeader = ({ post }) => {
  const [infoModal, setInfoModal] = useState(false);
  const { auth, socket } = useSelector((state) => state);
  const dispatch = useDispatch();

  const history = useHistory();

  const handleHover = () => {
    setTimeout(() => {
      setInfoModal(true);
    }, 400);
  };
  const handleUnHover = () => {
    setTimeout(() => {
      setInfoModal(false);
    }, 400);
  };

  const handleEditPost = () => {
    dispatch({ type: GLOBALTYPES.STATUS, payload: { ...post, onEdit: true } });
  };

  const CustomUIDelete = ({ onSubmit, onCancel }) => (
    <div className="confirm">
      <h1>
        <strong>Xóa bài viết ?</strong>
      </h1>
      <p>
        Bạn có chắc muốn <strong>xóa</strong> bài viết này ?
      </p>
      <button onClick={onCancel}>Hủy bỏ</button>
      <button onClick={onSubmit}>Xóa</button>
    </div>
  );

  const onClickDelete = () => {
    onConfirm({
      onSubmit: () => {
        dispatch(deletePost({ post, auth, socket }));
        // return history.goBack();
      },
      onCancel: () => {
        // alert("Cancel");
      },
      customUI: CustomUIDelete,
      className: "my-custom-ui-container"
    });
  };

  const handleCopyLink = () => {
    navigator.clipboard.writeText(`${BASE_URL}/post/${post._id}`);
  };

  return (
    <div className="card_header">
      <div
        className="d-flex "
        onMouseEnter={handleHover}
        onMouseLeave={handleUnHover}
      >
        {infoModal && <InfoModal user={post.user} />}
        <Link to={`/profile/${post.user._id}`}>
          <Avatar src={post.user.avatar} size="big-avatar" />
        </Link>
        <div className="card_name">
          <h6 className="m-0">
            <Link to={`/profile/${post.user._id}`} className="text-dark">
              {post.user.username}
            </Link>
          </h6>
          <small className="text-muted">
            {moment(post.createdAt).fromNow()}
          </small>
        </div>
      </div>
      <div className="nav-item dropdown">
        <span
          className="material-icons btn-toggle p-2"
          id="moreLink"
          data-toggle="dropdown"
        >
          more_horiz
        </span>

        <div
          className="dropdown-menu"
          style={{ background: "black", padding: " 12px 12px 10px 12px" }}
        >
          {auth.user._id === post.user._id && (
            <>
              <div
                className="dropdown-item "
                style={{ padding: "0px 0px 4px 0" }}
                onClick={handleEditPost}
              >
                <button
                  className="myCustomButton"
                  style={{ "--clr": "#1e9bff", width: "100% " }}
                >
                  {" "}
                  <span>Chỉnh sửa </span>
                  <i></i>
                </button>
              </div>{" "}
              <div
                className="dropdown-item"
                style={{ padding: "0px 0px 4px 0" }}
                onClick={onClickDelete}
              >
                <button
                  className="myCustomButton"
                  style={{ "--clr": "#ff1867", width: "100% " }}
                >
                  {" "}
                  <span>Xóa </span>
                  <i></i>
                </button>
              </div>{" "}
            </>
          )}
          <div
            className="dropdown-item"
            style={{ padding: "0px 0px 4px 0" }}
            onClick={handleCopyLink}
          >
            <button
              className="myCustomButton"
              style={{ "--clr": "#6eff3e", width: "100% " }}
            >
              {" "}
              <span>Copy Link </span>
              <i></i>
            </button>
          </div>{" "}
        </div>
      </div>
    </div>
  );
};

export default CardHeader;
