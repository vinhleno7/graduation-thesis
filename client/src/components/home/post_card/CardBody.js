import React, { useState } from "react";
import Carousel from "../../Carousel";
import InfoModal from "../../../components/InfoModal";
const CardBody = ({ post }) => {
  const [infoModal, setInfoModal] = useState(false);

  const [readMore, setReadMore] = useState(false);
  const handleHover = () => {
    setTimeout(() => {
      setInfoModal(true);
    }, 400);
  };
  const handleUnHover = () => {
    setTimeout(() => {
      setInfoModal(false);
    }, 400);
  };
  return (
    <div className="card_body">
      {post.images.length > 0 && (
        <Carousel images={post.images} id={post._id} />
      )}
      <div className="card_body-content mt-1 position-relative">
        <span onMouseLeave={handleUnHover}>
          <span
            style={{ fontWeight: 600, cursor: "pointer" }}
            className="mr-1 "
            onMouseEnter={handleHover}
          >
            {post.user.username}
          </span>
          {infoModal && <InfoModal user={post.user} top="top" />}

          {post.content.length < 60
            ? post.content
            : readMore
            ? post.content + " "
            : post.content.slice(0, 60) + "....."}
        </span>
        {post.content.length > 60 && (
          <span className="readMore" onClick={() => setReadMore(!readMore)}>
            {readMore ? "Rút ngắn" : "Xem thêm"}
          </span>
        )}
      </div>
    </div>
  );
};

export default CardBody;
