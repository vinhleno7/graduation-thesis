import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { HOBBY_TYPES } from "../../../redux/actions/hobbyAction";

const HobbyItem = ({ itemHobby }) => {
  const dispatch = useDispatch();
  const { hobby } = useSelector((state) => state);

  const handleCheck = () => {
    if (hobby.check.includes(itemHobby._id))
      dispatch({ type: HOBBY_TYPES.UN_CHECK_HOBBY, payload: itemHobby._id });
    else dispatch({ type: HOBBY_TYPES.CHECK_HOBBY, payload: itemHobby._id });
  };
  return (
    <div
      onClick={handleCheck}
      className={
        hobby.check.includes(itemHobby._id)
          ? "hobbyActive hobby-item d-flex align-items-end position-relative"
          : " hobby-item d-flex align-items-end position-relative"
      }
      style={{
        padding: "0 8px",
        color: "white",
        fontWeight: 600,
        width: "144px",
        height: "144px",
        margin: "6px",
        borderRadius: "15px",
        background: `url(${itemHobby.avatar})`,
        backgroundSize: "cover",
        backgroundPosition: "50%",
        cursor: "pointer"
      }}
    >
      {hobby.check.includes(itemHobby._id) && (
        <div
          className="d-flex justify-content-center align-content-center position-absolute"
          style={{
            width: "20px",
            right: 5,
            top: 5,
            height: "20px",
            background: "#111",
            borderRadius: "4px"
          }}
        >
          &#10004;
        </div>
      )}
      <p>{itemHobby.name}</p>
    </div>
  );
};

export default HobbyItem;
