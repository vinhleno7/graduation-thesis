import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import HobbyItem from "./HobbyItem";
import {
  getAllHobbies,
  setHobbyUser
} from "../../../redux/actions/hobbyAction";

const Hobby = ({ handleCloseHobby }) => {
  const dispatch = useDispatch();
  const { auth, hobby } = useSelector((state) => state);

  useEffect(() => {
    dispatch(getAllHobbies(auth.token));
  }, []);

  const handleSetHobbyUser = () => {
    const myHobby = hobby.check;
    const token = auth.token;

    dispatch(setHobbyUser({ myHobby, token }));

    handleCloseHobby();
  };

  return (
    <div style={{ padding: "8px 0" }}>
      <p
        style={{
          textAlign: "center",
          fontSize: "28px",
          padding: "8px",
          fontWeight: 600,
          color: "black"
        }}
      >
        Cho chúng tôi biết bạn quan tâm đến những gì
      </p>
      <div
        className="d-flex flex-wrap justify-content-center"
        style={{
          width: "100%",
          height: "400px",
          overflowY: "scroll",
          overflowX: "hidden"
        }}
      >
        {hobby.count > 0 &&
          hobby.hobbies.map((item) => (
            <HobbyItem itemHobby={item} key={item._id} />
          ))}
      </div>
      <div className="d-flex justify-content-center p-3">
        <button
          onClick={handleSetHobbyUser}
          className="myCustomButton"
          disabled={hobby.check.length < 5 ? true : false}
          style={{ "--clr": "#ff1867", width: "80%" }}
        >
          {" "}
          <span>
            {hobby.check.length < 5
              ? `Chọn thêm ${5 - hobby.check.length}`
              : "Thiết lập"}
          </span>
          <i></i>
        </button>
      </div>
    </div>
  );
};

export default Hobby;
