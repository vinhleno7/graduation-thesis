import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { deleteComment } from "../../../redux/actions/commentAction";
import { onConfirm } from "react-confirm-pro";

const CommentMenu = ({ handleCloseAdminCmt, post, comment, setOnEdit }) => {
  const { auth, socket } = useSelector((state) => state);
  // ! SOCKET HERE
  const dispatch = useDispatch();

  const CustomUIDelete = ({ onSubmit, onCancel }) => (
    <div className="confirm">
      <h1>
        <strong>Xóa lời nhận xét ?</strong>
      </h1>
      <p>
        Bạn có chắc muốn<strong> xóa</strong> lời nhận xét này?
      </p>
      <button onClick={onCancel}>Hủy bỏ</button>
      <button onClick={onSubmit}>Xóa</button>
    </div>
  );

  const onClickDelete = () => {
    if (handleCloseAdminCmt) handleCloseAdminCmt();
    // THIEU GOI VAO~ PROPS CAC COMMENT
    onConfirm({
      onSubmit: () => {
        if (
          auth.user.role === "admin" ||
          post.user._id === auth.user._id ||
          comment.user._id === auth.user._id
        ) {
          dispatch(deleteComment({ post, auth, comment, socket }));
        }
      },
      onCancel: () => {
        // alert("Cancel");
      },
      customUI: CustomUIDelete,
      className: "my-custom-ui-container"
    });
  };

  const MenuItem = () => {
    return (
      <>
        <div className="dropdown-item" onClick={() => setOnEdit(true)}>
          <button
            className="myCustomButton"
            style={{ "--clr": "#1e9bff", width: "100% " }}
          >
            {" "}
            <span> Sửa </span>
            <i></i>
          </button>
        </div>

        <div className="dropdown-item" onClick={onClickDelete}>
          <button
            className="myCustomButton"
            style={{ "--clr": "#ff1867", width: "100% " }}
          >
            {" "}
            <span>Xóa </span>
            <i></i>
          </button>
        </div>
      </>
    );
  };
  return (
    <div className="menu">
      {auth.user.role === "admin" ? (
        <div className="nav-item dropdown mr-1">
          <span
            className="material-icons btn-toggle p-1"
            id="moreLink"
            data-toggle="dropdown"
          >
            more_vert
          </span>

          <div
            className="dropdown-menu"
            style={{ background: "black" }}
            aria-labelledby="moreLink"
          >
            {MenuItem()}
          </div>
        </div>
      ) : (
        (post.user._id === auth.user._id ||
          comment.user._id === auth.user._id) && (
          <div className="nav-item dropdown mr-1">
            <span
              className="material-icons btn-toggle p-1"
              id="moreLink"
              data-toggle="dropdown"
            >
              more_vert
            </span>

            <div
              className="dropdown-menu"
              style={{ background: "black" }}
              aria-labelledby="moreLink"
            >
              {post.user._id === auth.user._id ? (
                comment.user._id === auth.user._id ? (
                  MenuItem()
                ) : (
                  <div className="dropdown-item" onClick={onClickDelete}>
                    <button
                      className="myCustomButton"
                      style={{ "--clr": "#ff1867", width: "100% " }}
                    >
                      {" "}
                      <span>Xóa </span>
                      <i></i>
                    </button>
                  </div>
                )
              ) : (
                comment.user._id === auth.user._id && MenuItem()
              )}
            </div>
          </div>
        )
      )}
    </div>
  );
};

export default CommentMenu;
