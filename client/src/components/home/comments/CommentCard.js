import React, { useState, useEffect } from "react";
import Avatar from "../../Avatar";
import { Link } from "react-router-dom";
import moment from "moment";
import "moment/locale/vi";

import { useParams } from "react-router-dom";

import LikeButton from "../../LikeButton";
import { useSelector, useDispatch } from "react-redux";
import CommentMenu from "./CommentMenu";
import {
  updateComment,
  likeComment,
  unLikeComment
} from "../../../redux/actions/commentAction";
import InputComment from "../InputComment";
import AdminInput from "../../admin/comment/InputComment";
import InfoModal from "../../InfoModal";

moment.locale("vi");

const CommentCard = ({
  handleCloseAdminCmt,
  children,
  comment,
  post,
  commentId
}) => {
  const { auth } = useSelector((state) => state);
  const dispatch = useDispatch();

  const [content, setContent] = useState("");
  const [readMore, setReadMore] = useState(false);

  const [onEdit, setOnEdit] = useState(false);
  const [isLike, setIsLike] = useState(false);

  const [loadLike, setLoadLike] = useState(false);

  const [onReply, setOnReply] = useState(false);

  useEffect(() => {
    setContent(comment.content);
    setIsLike(false);
    setOnReply(false);
    if (comment.likes.find((like) => like._id === auth.user._id)) {
      setIsLike(true);
    }
  }, [comment, auth.user._id]);

  const handleUpdate = () => {
    if (comment.content !== content) {
      dispatch(updateComment({ comment, post, content, auth }));
      setOnEdit(false);
    } else {
      setOnEdit(false);
    }
  };

  const handleLike = async () => {
    if (loadLike) return;
    setIsLike(true);

    setLoadLike(true);
    await dispatch(likeComment({ comment, post, auth }));
    setLoadLike(false);
  };

  const handleUnLike = async () => {
    if (loadLike) return;
    setIsLike(false);

    setLoadLike(true);
    await dispatch(unLikeComment({ comment, post, auth }));
    setLoadLike(false);
  };

  const handleReply = () => {
    if (onReply) return setOnReply(false);
    setOnReply({ ...comment, commentId });
  };

  const styleCard = {
    opacity: comment._id ? 1 : 0.5,
    pointerEvents: comment._id ? "inherit" : "none"
  };

  const [infoModal, setInfoModal] = useState(false);

  const handleHover = () => {
    setTimeout(() => {
      setInfoModal(true);
    }, 400);
  };

  const handleUnHover = () => {
    setTimeout(() => {
      setInfoModal(false);
    }, 400);
  };
  const { page } = useParams();
  return (
    <div
      className="comment_card mt-2 position-relative"
      style={styleCard}
      onMouseLeave={handleUnHover}
    >
      {infoModal && <InfoModal user={comment.user} />}

      {comment.user && (
        <Link
          to={`/profile/${comment.user._id}`}
          className="d-flex text-dark position-relative"
        >
          <Avatar src={comment.user.avatar} size="small-avatar" />
          <h6 className="mx-1 " onMouseEnter={handleHover}>
            {comment.user.username}
          </h6>
        </Link>
      )}

      {comment.user && (
        <div
          className={
            comment.user._id === auth.user._id
              ? "comment_content myCmt"
              : "comment_content yourCmt"
          }
        >
          <div className="flex-fill" style={{ width: "80%" }}>
            {onEdit ? (
              <textarea
                rows="5"
                value={content}
                onChange={(e) => setContent(e.target.value)}
              />
            ) : (
              <div>
                {comment.tag && comment.tag._id !== comment.user._id && (
                  <Link to={`/profile/${comment.tag._id}`} className="mr-1">
                    @{comment.tag.username}
                  </Link>
                )}
                <span>
                  {content.length < 100
                    ? content
                    : readMore
                    ? content + " "
                    : content.slice(0, 100) + "..."}
                </span>
                {content.length > 100 && (
                  <span
                    className="readMore"
                    onClick={() => setReadMore(!readMore)}
                  >
                    {readMore ? "Hide content" : "Read more"}
                  </span>
                )}
              </div>
            )}
            <div style={{ cursor: "pointer" }}>
              <small className="text-muted mr-3">
                {moment(comment.createdAt).fromNow()}
              </small>

              <small className="font-weight-bold mr-3">
                {comment.likes.length} lượt thích
              </small>

              {onEdit ? (
                <>
                  <small
                    className="font-weight-bold mr-3 text-primary"
                    onClick={handleUpdate}
                  >
                    Cập nhật
                  </small>
                  <small
                    className="font-weight-bold mr-3 text-danger"
                    onClick={() => setOnEdit(false)}
                  >
                    Hủy bỏ
                  </small>
                </>
              ) : (
                <small className="font-weight-bold mr-3" onClick={handleReply}>
                  {onReply ? "Hủy bỏ" : "Phản hồi"}
                </small>
              )}
            </div>
          </div>

          <div
            className="d-flex align-items-center mx-2"
            style={{ cursor: "pointer" }}
          >
            <CommentMenu
              post={post}
              comment={comment}
              auth={auth}
              handleCloseAdminCmt={handleCloseAdminCmt}
              setOnEdit={setOnEdit}
            />
            <LikeButton
              isLike={isLike}
              handleLike={handleLike}
              handleUnLike={handleUnLike}
            />
          </div>
        </div>
      )}

      {onReply ? (
        page === "admin" ? (
          <AdminInput post={post} onReply={onReply} setOnReply={setOnReply}>
            <Link to={`/profile/${onReply.user._id}`} className="mr-1">
              @{onReply.user.username}:
            </Link>
          </AdminInput>
        ) : (
          <InputComment post={post} onReply={onReply} setOnReply={setOnReply}>
            <Link to={`/profile/${onReply.user._id}`} className="mr-1">
              @{onReply.user.username}:
            </Link>
          </InputComment>
        )
      ) : (
        ""
      )}
      {children}
    </div>
  );
};

export default CommentCard;
