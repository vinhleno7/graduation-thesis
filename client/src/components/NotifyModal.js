import React from "react";
import { useSelector, useDispatch } from "react-redux";
import NoNotice from "../images/notice.png";
import { Link } from "react-router-dom";
import Avatar from "./Avatar";
import moment from "moment";
import "moment/locale/vi";
import {
  isReadNotify,
  NOTIFY_TYPES,
  deleteAllNotifies
} from "../redux/actions/notifyAction";
import { responseRequestFl } from "../redux/actions/profileAction";
import { onConfirm } from "react-confirm-pro";

moment.locale("vi");

const NotifyModal = ({ handleCloseNotify, handleOpenNotify }) => {
  const { auth, notify, socket } = useSelector((state) => state);
  const dispatch = useDispatch();

  const handleSeenAll = () => {
    notify.data.forEach((msg) => {
      dispatch(isReadNotify({ msg, auth }));
    });
  };

  const handleIsRead = (msg) => {
    dispatch(isReadNotify({ msg, auth }));
  };

  const handleSound = () => {
    dispatch({ type: NOTIFY_TYPES.UPDATE_SOUND, payload: true });
  };

  const handleUnSound = () => {
    dispatch({ type: NOTIFY_TYPES.UPDATE_SOUND, payload: false });
  };

  const CustomUIDelete = ({ onSubmit, onCancel }) => (
    <div className="confirm">
      <h1>
        <strong>Xóa tất cả thông báo ?</strong>
      </h1>
      <p>
        <span style={{ color: "red", marginRight: "4px" }}>
          {notify.data.filter((item) => item.isRead === false).length} thông báo
          chưa đọc.
        </span>
        Bạn có chắc muốn <strong>xóa</strong> bài viết này?
      </p>
      <button onClick={onCancel}>Hủy bỏ</button>
      <button onClick={onSubmit}>Xóa</button>
    </div>
  );

  const onClickDelete = () => {
    handleCloseNotify();
    onConfirm({
      onSubmit: () => {
        dispatch(deleteAllNotifies(auth.token));
        handleOpenNotify();
      },
      onCancel: () => {
        handleOpenNotify();
        // alert("Cancel");
      },
      customUI: CustomUIDelete,
      className: "my-custom-ui-container"
    });
  };

  const handleAccept = ({ e, msg }) => {
    e.preventDefault();
    const response = true;
    dispatch(responseRequestFl({ msg, response, auth, socket }));
  };
  const handleDeny = ({ e, msg }) => {
    e.preventDefault();
    const response = false;
    dispatch(responseRequestFl({ msg, response, auth, socket }));
  };

  return (
    <div className="myModalNotify">
      <div className="d-flex justify-content-between align-items-center px-3">
        <h3 style={{ margin: "4px" }}>Thông báo</h3>
        {notify.sound ? (
          <div>
            <i
              className="fas fa-bell text-danger"
              style={{ fontSize: "1.2rem", cursor: "pointer" }}
              onClick={handleUnSound}
            />
            <span
              style={{
                fontSize: "9px",
                margin: "0 2px 1px 2px",
                color: "gray"
              }}
            >
              Âm báo: Bật
            </span>
          </div>
        ) : (
          <div>
            <i
              className="fas fa-bell-slash text-danger"
              style={{ fontSize: "1.2rem", cursor: "pointer" }}
              onClick={handleSound}
            />
            <span
              style={{
                fontSize: "9px",
                margin: "0 2px 1px 2px",
                color: "gray"
              }}
            >
              Âm báo: Tắt
            </span>
          </div>
        )}
      </div>
      <hr className="mt-0" />

      {notify.data.length === 0 ? (
        <>
          <p style={{ color: "#8e8e8e", fontSize: "10px", marginLeft: "4px" }}>
            Bạn không có thông báo nào cả.
          </p>
          <div className="d-flex justify-content-center">
            <img
              src={NoNotice}
              alt="NoNotice"
              style={{ width: "100px", height: "100px" }}
            />
          </div>
        </>
      ) : (
        <div style={{ overflowY: "scroll", height: "400px" }}>
          {notify.data.map((msg, index) => (
            <div
              key={index}
              className="px-2 mb-1"
              style={{
                background: msg.isRead ? "" : "#ddd",
                borderRadius: "8px",
                padding: "10px 0 0 0"
              }}
            >
              <Link
                to={`${msg.url}`}
                className="d-flex text-dark align-items-center"
                onClick={() => handleIsRead(msg)}
              >
                {!msg ? (
                  "no"
                ) : (
                  <Avatar src={msg?.user?.avatar} size="big-avatar" />
                )}

                <div className="mx-2 flex-fill">
                  <div>
                    <strong className="mr-1">{msg?.user?.username}</strong>
                    <span>{msg?.text}</span>
                  </div>
                  {msg.content && <small>{msg.content.slice(0, 30)}...</small>}
                  {msg?.text === "Đã gửi yêu cầu theo dõi" && (
                    <div
                      className="d-flex justify-content-center"
                      style={{ zIndex: 2 }}
                    >
                      <div
                        style={{
                          padding: "4px 12px",
                          background: "#0095f6",
                          borderRadius: "6px",
                          color: "white"
                        }}
                        onClick={(e) => handleAccept({ e, msg })}
                      >
                        Đồng ý
                      </div>
                      <div
                        style={{
                          padding: "4px 12px",
                          color: "red"
                        }}
                        onClick={(e) => handleDeny({ e, msg })}
                      >
                        Từ chối
                      </div>
                    </div>
                  )}
                </div>

                {msg.image && (
                  <div style={{ width: "30px", margin: "0 8px 0 0 " }}>
                    {msg.image.match(/video/i) ? (
                      <video src={msg.image} width="100%" />
                    ) : (
                      <Avatar src={msg.image} size="medium-avatar" />
                    )}
                  </div>
                )}
                {!msg.isRead && (
                  <i
                    className="fas fa-circle text-primary"
                    style={{ fontSize: "12px" }}
                  />
                )}
              </Link>
              <small className="text-muted d-flex justify-content-between px-1">
                {moment(msg.createdAt).fromNow()}
                {/* {!msg.isRead && <i className="fas fa-circle text-primary" />} */}
              </small>
            </div>
          ))}
        </div>
      )}

      <hr className="my-1" />
      <div className="d-flex justify-content-between">
        <div
          onClick={handleSeenAll}
          className="text-left text-primary ml-2"
          style={{ cursor: "pointer" }}
        >
          <span style={{ fontWeight: 600 }}> Đánh dấu xem hết</span>
        </div>

        <div
          className="text-right text-danger mr-2"
          style={{ cursor: "pointer" }}
          onClick={onClickDelete}
        >
          <span style={{ fontWeight: 600 }}> Xóa tất cả</span>
        </div>
      </div>
    </div>
  );
};

export default NotifyModal;
