import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { checkImage } from "../../utils/imageUpload";
import { GLOBALTYPES } from "../../redux/actions/globalTypes";
import {
  updateProfileUser,
  changePassword
} from "../../redux/actions/profileAction";
const EditProfile = ({ setOnEdit }) => {
  const initialState = {
    username: "",
    fullname: "",
    mobile: "",
    address: "",
    website: "",
    story: "",
    gender: ""
  };

  const [userData, setUserData] = useState(initialState);
  const { username, fullname, mobile, address, website, story, gender } =
    userData;

  const [avatar, setAvatar] = useState("");
  const [openModelPassword, setOpenModelPassword] = useState(false);
  const { auth, theme } = useSelector((state) => state);
  const dispatch = useDispatch();

  useEffect(() => {
    setUserData(auth.user);
  }, [auth.user]);

  const changeAvatar = (e) => {
    const file = e.target.files[0];

    const err = checkImage(file);
    if (err)
      return dispatch({ type: GLOBALTYPES.ALERT, payload: { error: err } });
    setAvatar(file);
  };
  const handleInput = (e) => {
    const { name, value } = e.target;
    setUserData({ ...userData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(updateProfileUser({ userData, avatar, auth }));
    setOnEdit(false);
  };

  const [oldPass, setOldPass] = useState("");
  const [newPass, setNewPass] = useState("");
  const [newConformPass, setNewConformPass] = useState("");

  const handleChangePassword = (e) => {
    e.preventDefault();

    dispatch(changePassword({ oldPass, newPass, newConformPass, auth }));

    setOldPass("");
    setNewPass("");
    setNewConformPass("");
  };

  return (
    <div className="edit_profile">
      <button
        className="btn btn-danger btn_close"
        onClick={() => setOnEdit(false)}
      >
        Đóng
      </button>

      <form onSubmit={handleSubmit}>
        <div className="info_avatar">
          <img
            src={avatar ? URL.createObjectURL(avatar) : auth.user.avatar}
            alt="avatar"
            style={{ filter: theme ? "invert(1)" : "invert(0)" }}
          />
          <span>
            <i className="fas fa-camera" />
            <p>Đổi</p>
            <input
              type="file"
              name="file"
              id="file_up"
              accept="image/*"
              onChange={changeAvatar}
            />
          </span>
        </div>

        <div className="form-group">
          <label htmlFor="username">Tên tài khoản</label>
          <div className="position-relative">
            <input
              type="text"
              className="form-control"
              id="username"
              name="username"
              value={username}
              onChange={handleInput}
            />
            <small
              className="text-danger position-absolute"
              style={{
                top: "50%",
                right: "5px",
                transform: "translateY(-50%)"
              }}
            >
              {username.length}/25
            </small>
          </div>
        </div>
        <div className="form-group">
          <label htmlFor="fullname">Họ và tên</label>
          <div className="position-relative">
            <input
              type="text"
              className="form-control"
              id="fullname"
              name="fullname"
              value={fullname}
              onChange={handleInput}
            />
            <small
              className="text-danger position-absolute"
              style={{
                top: "50%",
                right: "5px",
                transform: "translateY(-50%)"
              }}
            >
              {fullname.length}/25
            </small>
          </div>
        </div>

        <div className="form-group">
          <label htmlFor="mobile">Số điện thoại</label>
          <input
            type="text"
            name="mobile"
            value={mobile}
            className="form-control"
            onChange={handleInput}
          />
        </div>

        <div className="form-group">
          <label htmlFor="address">Địa chỉ</label>
          <input
            type="text"
            name="address"
            value={address}
            className="form-control"
            onChange={handleInput}
          />
        </div>
        <div className="form-group">
          <label htmlFor="website">Trang web</label>
          <input
            type="text"
            name="website"
            value={website}
            className="form-control"
            onChange={handleInput}
          />
        </div>
        <div className="form-group">
          <label htmlFor="story">Câu chuyện</label>
          <textarea
            name="story"
            value={story}
            className="form-control"
            onChange={handleInput}
            cols="30"
            rows="4"
          />
          <small className="text-danger">{story.length}/200</small>
        </div>

        <label htmlFor="gender">Giới tính</label>
        <div className="input-group-prepend px-0 mb-4">
          <select
            name="gender"
            id="gender"
            value={gender}
            className="custom-select text-capitalize"
            onChange={handleInput}
          >
            <option value="male">Nam</option>
            <option value="female">Nữ</option>
            <option value="other">Giới tính thứ 3</option>
          </select>
        </div>
        <p
          style={{ cursor: "pointer" }}
          onClick={() => setOpenModelPassword(!openModelPassword)}
        >
          Đổi mật khẩu
        </p>

        <div
          style={{ border: "1px solid #ddd" }}
          className={
            openModelPassword
              ? "activeOpenPassword openModelPassword p-3 mb-2"
              : "openModelPassword p-3 mb-2"
          }
        >
          <div className="form-group">
            <label htmlFor="old_password">Mật khẩu cũ</label>
            <input
              type="password"
              name="old_password"
              value={oldPass}
              className="form-control"
              onChange={(e) => setOldPass(e.target.value)}
            />
          </div>

          <div className="form-group">
            <label htmlFor="new_password">Mật khẩu mới</label>
            <input
              type="password"
              name="new_password"
              value={newPass}
              className="form-control"
              onChange={(e) => setNewPass(e.target.value)}
            />
          </div>

          <div className="form-group">
            <label htmlFor="newConformPass">Xác nhận mật khẩu</label>
            <input
              type="password"
              name="newConformPass"
              value={newConformPass}
              className="form-control"
              onChange={(e) => setNewConformPass(e.target.value)}
            />
          </div>
          <button
            onClick={(e) => handleChangePassword(e)}
            className="myCustomButton"
            style={{ "--clr": "orange", width: "50% " }}
          >
            {" "}
            <span>Đổi mật khẩu </span>
            <i></i>
          </button>
        </div>
        <button
          className="myCustomButton"
          style={{ "--clr": "#1e9bff", width: "100% " }}
        >
          {" "}
          <span>Chỉnh sửa </span>
          <i></i>
        </button>
      </form>
    </div>
  );
};

export default EditProfile;
