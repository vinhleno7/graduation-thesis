import React from "react";
import UserCard from "../UserCard";
import FollowBtn from "../FollowBtn";
import { useSelector } from "react-redux";

const Following = ({ users, setShowFollowing }) => {
  const { auth } = useSelector((state) => state);
  return (
    <div className="follow">
      <div className="follow_box">
        <h5>Đang theo dõi</h5>
        <hr />

        {users.length > 0 ? (
          <div
            style={{
              height: "30rem",
              overflowY: "scroll",
              overflowX: "hidden"
            }}
          >
            {users.map((user) => (
              <UserCard
                size="big-avatar"
                key={user._id}
                user={user}
                setShowFollowing={setShowFollowing}
              >
                {auth.user._id !== user._id && <FollowBtn user={user} />}
              </UserCard>
            ))}
          </div>
        ) : (
          <p>Bạn chưa theo dõi ai cả! Bạn nên tìm và theo dõi 1 vài bạn bè.</p>
        )}
        <div className="close" onClick={() => setShowFollowing(false)}>
          &times;
        </div>
      </div>
    </div>
  );
};

export default Following;
// ! 14:36 #12
