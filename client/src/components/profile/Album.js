import React, { useState, useEffect } from "react";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import AlbumThumb from "../AlbumThumb";
import LoadIcon from "../../images/loading.gif";
import LoadMoreBtn from "../LoadMoreBtn";
import { getDataAPI } from "../../utils/fetchData";
import { PROFILE_TYPES } from "../../redux/actions/profileAction";
import CreateAlbum from "../CreateModal";
import { useSelector } from "react-redux";
import { GLOBALTYPES } from "../../redux/actions/globalTypes";
const Album = ({ auth, id, dispatch, profile }) => {
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4
  };

  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => {
    if (status.onEditAlbum) {
      dispatch({ type: GLOBALTYPES.STATUS, payload: false });
    }
    setOpen(false);
  };

  const [albums, setAlbums] = useState([]);
  const [result, setResult] = useState(9);
  const [page, setPage] = useState(0);
  const [load, setLoad] = useState(false);
  const { status } = useSelector((state) => state);

  useEffect(() => {
    profile.albums.forEach((data) => {
      if (data._id === id) {
        setAlbums(data.albums);
        setResult(data.result);
        setPage(data.page);
      }
    });
  }, [profile.albums, id]);

  useEffect(() => {
    if (status.onEditAlbum) handleOpen();
  }, [status]);

  const handleLoadMore = async () => {
    setLoad(true);
    const res = await getDataAPI(
      `user_albums/${id}?limit=${page * 9}`,
      auth.token
    );
    const newData = { ...res.data, page: page + 1, _id: id };
    dispatch({ type: PROFILE_TYPES.UPDATE_ALBUM, payload: newData });
    setLoad(false);
  };
  const iconCreateAlbum = true;
  return (
    <div className="albums mt-2">
      {auth.user._id === id && (
        <>
          <p style={{ color: "#8e8e8e", fontSize: "10px", marginLeft: "4px" }}>
            Bạn có thể chọn nhiều chế độ, đó là công khai, riêng tư và đăng với
            ai đó.
          </p>
          <div className="d-flex justify-content-center">
            <button
              onClick={handleOpen}
              className="myCustomButton width__100__mobile"
              style={{
                "--clr": "#d35eb0",
                width: "40%",
                margin: "0 4px 0 0"
              }}
            >
              {" "}
              <span>Tạo album</span>
              <i></i>
            </button>
          </div>
        </>
      )}

      {load || albums.length < 0 ? (
        <img
          src={LoadIcon}
          style={{ width: "50px", height: "50px" }}
          alt="Loading"
          className="d-block mx-auto "
        />
      ) : (
        <AlbumThumb
          albums={albums}
          result={result}
          iconCreateAlbum={iconCreateAlbum}
        />
      )}

      <LoadMoreBtn
        result={result}
        page={page}
        load={load}
        handleLoadMore={handleLoadMore}
      />

      <Modal
        sx={{ height: "800px", overflow: "hidden" }}
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <CreateAlbum OpenFun={handleOpen} closeFun={handleClose} />
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};

export default Album;
