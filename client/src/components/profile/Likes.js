import React from "react";
import UserCard from "../UserCard";
import FollowBtn from "../FollowBtn";
import { useSelector } from "react-redux";

const Likes = ({ users, setShowLikes }) => {
  const { auth } = useSelector((state) => state);
  return (
    <div className="follow">
      <div className="follow_box">
        <h5>Lượt thích</h5>
        <hr />
        {users.length > 0 ? (
          users.map((user) => (
            <UserCard
              size="big-avatar"
              key={user._id}
              user={user}
              setShowLikes={setShowLikes}
            >
              {auth.user._id !== user._id && <FollowBtn user={user} />}
            </UserCard>
          ))
        ) : (
          <p>Không có lượt thích nào!</p>
        )}
        <div className="close" onClick={() => setShowLikes(false)}>
          &times;
        </div>
      </div>
    </div>
  );
};

export default Likes;
