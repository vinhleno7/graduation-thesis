import React, { useState, useEffect } from "react";
import Avatar from "../Avatar";
import { logout } from "../../redux/actions/authAction";

import EditProfile from "./EditProfile";
import FollowBtn from "../FollowBtn";
import Followers from "./Followers";
import Following from "./Following";
import { GLOBALTYPES } from "../../redux/actions/globalTypes";
import Verified from "../../images/Verified.png";

const Info = ({ id, auth, profile, dispatch }) => {
  const [userData, setUserData] = useState([]);
  const [onEdit, setOnEdit] = useState(false);

  const [showFollowers, setShowFollowers] = useState(false);
  const [showFollowing, setShowFollowing] = useState(false);

  useEffect(() => {
    if (id === auth.user._id) {
      setUserData([auth.user]);
    } else {
      const newData = profile.users.filter((user) => user._id === id);
      setUserData(newData);
    }
  }, [id, auth, dispatch, profile.users]);

  useEffect(() => {
    // function làm cho các modal dưới ở cố định vị trí ban đầu.
    if (showFollowers || showFollowing || onEdit) {
      dispatch({ type: GLOBALTYPES.MODAL, payload: true });
    } else {
      dispatch({ type: GLOBALTYPES.MODAL, payload: false });
    }
  }, [showFollowers, showFollowing, onEdit, dispatch]);

  return (
    <div className="info">
      {userData.map((user) => (
        <div key={user._id} className="info_container">
          <Avatar src={user.avatar} size="supper-avatar" />

          <div className="info_content">
            <div className="info_content_title">
              <h2 className="center_mobile" style={{ width: "fit-content" }}>
                {user.username}
                {user.role === "admin" && (
                  <span>
                    <img
                      style={{ width: "32px" }}
                      src={Verified}
                      alt="Verified"
                    />
                  </span>
                )}
              </h2>
              {user._id === auth.user._id ? (
                <>
                  <button
                    onClick={() => setOnEdit(true)}
                    className="myCustomButton hidden"
                    style={{
                      "--clr": "#6eff3e",
                      width: "100%",
                      margin: "0 4px 0 0"
                    }}
                  >
                    {" "}
                    <span>Chỉnh sửa</span>
                    <i></i>
                  </button>
                  <button
                    onClick={dispatch(logout)}
                    className="myCustomButton hidden"
                    style={{ "--clr": "#ff1867", width: "100%" }}
                  >
                    {" "}
                    <span>Đăng xuất</span>
                    <i></i>
                  </button>
                  <div className="group__btn__control">
                    <button
                      onClick={() => setOnEdit(true)}
                      className="myCustomButton"
                      style={{
                        "--clr": "#6eff3e",
                        width: "100%",
                        margin: "0 4px 0 0"
                      }}
                    >
                      {" "}
                      <span>Chỉnh sửa</span>
                      <i></i>
                    </button>
                    <button
                      onClick={dispatch(logout)}
                      className="myCustomButton"
                      style={{ "--clr": "#ff1867", width: "100%" }}
                    >
                      {" "}
                      <span>Đăng xuất</span>
                      <i></i>
                    </button>
                  </div>
                </>
              ) : (
                <FollowBtn user={user} />
              )}
            </div>

            <div className="follow_btn" style={{ width: "fit-content" }}>
              <span className="mr-4" onClick={() => setShowFollowers(true)}>
                <span style={{ fontWeight: "bolder" }}>
                  {user.followers.length}
                </span>{" "}
                Người theo dõi
              </span>
              <span className="ml-4" onClick={() => setShowFollowing(true)}>
                <span style={{ fontWeight: "bolder" }}>
                  {user.following.length}
                </span>{" "}
                Đang theo dõi
              </span>
            </div>

            <h6 style={{ width: "fit-content" }}>{user.fullname}</h6>
            <p className="m-0">{user.address}</p>
            <p className="text-danger m-0">{user.mobile}</p>
            <h6 className="m-0">{user.email}</h6>
            <a href={user.website} target="_blank" rel="noreferrer">
              {user.website}
            </a>
            <p>{user.story}</p>
          </div>

          {onEdit && <EditProfile setOnEdit={setOnEdit} />}

          {showFollowers && (
            <Followers
              users={user.followers}
              setShowFollowers={setShowFollowers}
            />
          )}
          {showFollowing && (
            <Following
              users={user.following}
              setShowFollowing={setShowFollowing}
            />
          )}
        </div>
      ))}
    </div>
  );
};

export default Info;
