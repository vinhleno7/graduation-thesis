import { GLOBALTYPES } from "./globalTypes";
import { imageUpload } from "../../utils/imageUpload";

import valid from "../../utils/valid";
import {
  getDataAPI,
  postDataAPI,
  patchDataAPI,
  deleteDataAPI
} from "../../utils/fetchData";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

export const A_STORY_TYPES = {
  A_CREATE_STORY: "A_CREATE_STORY",
  A_LOADING_STORY: "A_LOADING_STORY",
  A_GET_STORIES: "A_GET_STORIES",
  A_UPDATE_STORY: "A_UPDATE_STORY",
  A_GET_STORY: "A_GET_STORY",
  A_DELETE_STORY: "A_DELETE_STORY"
};

export const getAllStories = (token) => async (dispatch) => {
  try {
    dispatch({ type: A_STORY_TYPES.A_LOADING_STORY, payload: true });

    const res = await getDataAPI(`stories`, token);

    dispatch({ type: A_STORY_TYPES.A_GET_STORIES, payload: res.data });

    dispatch({ type: A_STORY_TYPES.A_LOADING_STORY, payload: false });
  } catch (err) {
    toast.error(err.response.data.msg);
  }
};
export const deleteStory =
  ({ story, auth }) =>
  async (dispatch) => {
    try {
      const res = await deleteDataAPI(`story/${story._id}`, auth.token);

      dispatch(getAllStories(auth.token));

      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const adminCreateStory =
  ({ content, myUser, images, auth, socket }) =>
  async (dispatch) => {
    let media = [];
    try {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });
      if (images.length > 0) media = await imageUpload(images);

      const res = await postDataAPI(
        "story",
        { content, images: media, myUser },
        auth.token
      );

      dispatch(getAllStories(auth.token));
      toast.success(`Tạo câu chuyện của ${myUser.username} thành công.`);

      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
      // ! NOTIFY
      // const msg = {
      //   id: res.data.newPost._id,
      //   text: "has just post a new status.",
      //   recipients: res.data.newPost.user.followers,
      //   url: `/post/${res.data.newPost._id}`,
      //   content,
      //   image: media[0].url
      // };

      // dispatch(createNotify({ msg, auth, socket }));
    } catch (err) {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
      toast.error(err.response.data.msg);
    }
  };
