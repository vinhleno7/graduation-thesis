import { GLOBALTYPES } from "./globalTypes";
// import { getProfileUsers } from "../../redux/actions/profileAction";
import { imageUpload } from "../../utils/imageUpload";
import { createNotify } from "./notifyAction";

import {
  postDataAPI,
  getDataAPI,
  patchDataAPI,
  deleteDataAPI
} from "../../utils/fetchData";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

export const STORY_TYPES = {
  CREATE_STORY: "CREATE_STORY",
  LOADING_STORY: "LOADING_STORY",
  GET_STORIES: "GET_STORIES",
  UPDATE_STORY: "UPDATE_STORY",
  GET_STORY: "GET_STORY",
  DELETE_STORY: "DELETE_STORY"
};

export const createStory =
  ({ content, images, auth, socket }) =>
  async (dispatch) => {
    let media = [];
    try {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });
      if (images.length > 0) media = await imageUpload(images);

      const res = await postDataAPI(
        "story",
        { content, images: media },
        auth.token
      );

      dispatch(getStories(auth.token));

      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
    } catch (err) {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
      toast.error(err.response.data.msg);
    }
  };
export const getStories = (token) => async (dispatch) => {
  try {
    dispatch({ type: STORY_TYPES.LOADING_STORY, payload: true });
    const res = await getDataAPI("story", token);

    dispatch({
      type: STORY_TYPES.GET_STORIES,
      payload: { ...res.data, page: 2 }
    });

    dispatch({ type: STORY_TYPES.LOADING_STORY, payload: false });
  } catch (err) {
    dispatch({ type: STORY_TYPES.LOADING_STORY, payload: false });
    toast.error(err.response.data.msg);
  }
};
export const deleteStory =
  ({ id, auth }) =>
  async (dispatch) => {
    try {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });

      const res = await deleteDataAPI(`story/${id}`, auth.token);

      dispatch(getStories(auth.token));

      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
    } catch (err) {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
      toast.error(err.response.data.msg);
    }
  };
export const isReadStory =
  ({ id, auth }) =>
  async (dispatch) => {
    try {
      const res = await patchDataAPI(`/isReadStory/${id}`, null, auth.token);
      dispatch(getStories(auth.token));
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const likeStory =
  ({ id, auth, socket }) =>
  async (dispatch) => {
    try {
      const res = await patchDataAPI(`story/${id}/like`, null, auth.token);

      const msg = {
        id: auth.user._id,
        text: "loved your story.",
        recipients: [res.data.newStory.user._id],
        url: `/stories/${res.data.newStory.user.username}`,
        content: res.data.newStory.content,
        image: res.data.newStory.images[0].url
      };

      dispatch(createNotify({ msg, auth, socket }));
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const unLikeStory =
  ({ id, auth }) =>
  async (dispatch) => {
    try {
      await patchDataAPI(`story/${id}/unlike`, null, auth.token);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };
