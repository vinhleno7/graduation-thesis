import { getDataAPI } from "../../utils/fetchData";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

export const SUGGEST_TYPES = {
  LOADING: "LOADING_SUGGEST",
  GET_USERS: "GET_USERS_SUGGEST"
};

export const getSuggestions = (token) => async (dispatch) => {
  try {
    dispatch({ type: SUGGEST_TYPES.LOADING, payload: true });

    const res = await getDataAPI("suggestionsUser", token);

    dispatch({ type: SUGGEST_TYPES.GET_USERS, payload: res.data });

    dispatch({ type: SUGGEST_TYPES.LOADING, payload: false });
  } catch (err) {
    toast.error(err.response.data.msg);
  }
};
