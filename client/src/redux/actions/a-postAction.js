import { GLOBALTYPES } from "./globalTypes";
import valid from "../../utils/valid";
import {
  getDataAPI,
  postDataAPI,
  patchDataAPI,
  deleteDataAPI
} from "../../utils/fetchData";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { createNotify, removeNotify } from "./notifyAction";

toast.configure();

export const A_POST_TYPES = {
  A_CREATE_POST: "A_CREATE_POST",
  A_LOADING_POST: "A_LOADING_POST",
  A_GET_POSTS: "A_GET_POSTS",
  A_UPDATE_POST: "A_UPDATE_POST",
  A_GET_POST: "A_GET_POST",
  A_GET_REPORT: "A_GET_REPORT",
  A_DELETE_POST: "A_DELETE_POST"
};

export const getAllPosts = (token) => async (dispatch) => {
  try {
    dispatch({ type: A_POST_TYPES.A_LOADING_POST, payload: true });

    const res = await getDataAPI(`adminPosts`, token);

    dispatch({ type: A_POST_TYPES.A_GET_POSTS, payload: res.data });

    dispatch({ type: A_POST_TYPES.A_LOADING_POST, payload: false });
  } catch (err) {
    toast.error(err.response.data.msg);
  }
};
export const getAllReports = (token) => async (dispatch) => {
  try {
    dispatch({ type: A_POST_TYPES.A_LOADING_POST, payload: true });

    const res = await getDataAPI(`report/getAll`, token);

    dispatch({ type: A_POST_TYPES.A_GET_REPORT, payload: res.data });

    dispatch({ type: A_POST_TYPES.A_LOADING_POST, payload: false });
  } catch (err) {
    toast.error(err.response.data.msg);
  }
};
export const deletePost =
  ({ post, auth }) =>
  async (dispatch) => {
    try {
      const res = await deleteDataAPI(`adminPost/${post._id}`, auth.token);

      dispatch(getAllPosts(auth.token));

      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };
export const responseReport =
  ({ reportId, response, auth, socket }) =>
  async (dispatch) => {
    console.log({ reportId, response, auth });
    try {
      const res = await postDataAPI(
        `report/response`,
        { reportId, response },
        auth.token
      );

      if (response) {
        // Notify với người báo cáo
        const msg = {
          id: auth.user._id,
          text: "Người quản lý đã đồng ý với bài cáo của bạn và xóa bài viết đó đi.",
          recipients: [res.data.authorReport._id],
          url: `/profile/${res.data.updatePost.user._id}`,
          content: res.data.updatePost.content,
          image: res.data.updatePost.images[0].url
        };

        dispatch(createNotify({ msg, auth, socket }));

        // Notify với người chủ bài viết
        const msg2 = {
          id: auth.user._id,
          text: "Người quản lý đã thấy bài viết của bạn đã vi phạm qui tắc và đã xóa nó.",
          recipients: [res.data.updatePost.user._id],
          url: `/profile/${res.data.updatePost.user._id}`,
          content: res.data.updatePost.content,
          image: res.data.updatePost.images[0].url
        };

        dispatch(createNotify({ msg: msg2, auth, socket }));

        dispatch(getAllReports(auth.token));
        dispatch(getAllPosts(auth.token));
      } else dispatch(getAllReports(auth.token));

      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };
