// import { GLOBALTYPES } from "./globalTypes";
import { getDataAPI, postDataAPI } from "../../utils/fetchData";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

export const HOBBY_TYPES = {
  LOADING_HOBBY: "LOADING_HOBBY",
  GET_ALL_HOBBY: "GET_ALL_HOBBY",
  CHECK_HOBBY: "CHECK_HOBBY",
  UN_CHECK_HOBBY: "UN_CHECK_HOBBY",
  SET_MY_HOBBY: "SET_MY_HOBBY",
  GET_MY_HOBBY: "GET_MY_HOBBY"
};

export const getAllHobbies = (token) => async (dispatch) => {
  try {
    const res = await getDataAPI("hobby/getAll", token);

    dispatch({ type: HOBBY_TYPES.LOADING_HOBBY, payload: true });

    dispatch({ type: HOBBY_TYPES.GET_ALL_HOBBY, payload: res.data });

    dispatch({ type: HOBBY_TYPES.LOADING_HOBBY, payload: false });
  } catch (err) {
    toast.error(err.response.data.msg);
  }
};
export const setHobbyUser =
  ({ myHobby, token }) =>
  async (dispatch) => {
    try {
      const res = await postDataAPI("hobby/setHobbyUser", { myHobby }, token);
      dispatch({ type: HOBBY_TYPES.LOADING_HOBBY, payload: true });

      dispatch(getUserHobby(token));

      toast.success(res.data.msg);

      dispatch({ type: HOBBY_TYPES.LOADING_HOBBY, payload: false });
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };
export const getUserHobby = (token) => async (dispatch) => {
  try {
    const res = await getDataAPI("hobby/getUser", token);
    dispatch({ type: HOBBY_TYPES.LOADING_HOBBY, payload: true });

    dispatch({ type: HOBBY_TYPES.GET_MY_HOBBY, payload: res.data });

    dispatch({ type: HOBBY_TYPES.LOADING_HOBBY, payload: false });
  } catch (err) {
    toast.error(err.response.data.msg);
  }
};
