import { GLOBALTYPES } from "./globalTypes";
import {
  postDataAPI,
  deleteDataAPI,
  getDataAPI,
  patchDataAPI
} from "../../utils/fetchData";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

export const NOTIFY_TYPES = {
  GET_NOTIFIES: "GET_NOTIFIES",
  CREATE_NOTIFY: "CREATE_NOTIFY",
  REMOVE_NOTIFY: "REMOVE_NOTIFY",
  UPDATE_NOTIFY: "UPDATE_NOTIFY",
  UPDATE_SOUND: "UPDATE_SOUND",
  DELETE_ALL_NOTIFIES: "DELETE_ALL_NOTIFIES"
};

export const createNotify =
  ({ msg, userNotify, auth, socket }) =>
  async (dispatch) => {
    try {
      const res = await postDataAPI("notify", msg, auth.token);

      socket.emit("createNotify", {
        ...res.data.notify,
        user: {
          username: !userNotify ? auth.user.username : userNotify.username,
          avatar: !userNotify ? auth.user.avatar : userNotify.avatar
        }
      });
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const removeNotify =
  ({ msg, auth, socket }) =>
  async (dispatch) => {
    try {
      const res = await deleteDataAPI(
        `notify/${msg.id}?url=${msg.url}`,
        auth.token
      );

      dispatch({
        type: NOTIFY_TYPES.REMOVE_NOTIFY,
        payload: res.data.notify._id
      });
      socket.emit("removeNotify", msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const getNotifies = (token) => async (dispatch) => {
  try {
    const res = await getDataAPI("notifies", token);

    dispatch({ type: NOTIFY_TYPES.GET_NOTIFIES, payload: res.data.notifies });
  } catch (err) {
    // dispatch({
    //   type: GLOBALTYPES.ALERT,
    //   payload: { error: err.response.data.msg }
    // });
    toast.error(err.response.data.msg);
  }
};

export const isReadNotify =
  ({ msg, auth }) =>
  async (dispatch) => {
    dispatch({
      type: NOTIFY_TYPES.UPDATE_NOTIFY,
      payload: { ...msg, isRead: true }
    });
    try {
      await patchDataAPI(`/isReadNotify/${msg._id}`, null, auth.token);
    } catch (err) {
      // dispatch({
      //   type: GLOBALTYPES.ALERT,
      //   payload: { error: err.response.data.msg }
      // });
      toast.error(err.response.data.msg);
    }
  };

export const deleteAllNotifies = (token) => async (dispatch) => {
  dispatch({ type: NOTIFY_TYPES.DELETE_ALL_NOTIFIES, payload: [] });
  try {
    await deleteDataAPI("deleteAllNotify", token);
  } catch (err) {
    // dispatch({
    //   type: GLOBALTYPES.ALERT,
    //   payload: { error: err.response.data.msg }
    // });
    toast.error(err.response.data.msg);
  }
};
