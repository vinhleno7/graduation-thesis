import { GLOBALTYPES, DeleteData } from "./globalTypes";
import { getDataAPI, patchDataAPI, postDataAPI } from "../../utils/fetchData";
import { imageUpload } from "../../utils/imageUpload";
import {
  createNotify,
  removeNotify,
  NOTIFY_TYPES
} from "../actions/notifyAction";
import { getAllUsers } from "./a-userAction";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

export const PROFILE_TYPES = {
  LOADING: "LOADING_PROFILE",
  GET_USER: "GET_PROFILE_USER",
  UPDATE_USER_FL: "UPDATE_USER_FL",
  FOLLOW: "FOLLOW",
  UNFOLLOW: "UNFOLLOW",
  GET_ID: "GET_PROFILE_ID",
  GET_POSTS: "GET_PROFILE_POSTS",
  GET_ALBUMS: "GET_PROFILE_ALBUMS",
  UPDATE_POST: "UPDATE_PROFILE_POST",
  UPDATE_ALBUM: "UPDATE_PROFILE_ALBUM",
  CREATE_ALBUM: "CREATE_PROFILE_ALBUM",
  GET_REQUEST_FL: "GET_REQUEST_FL"
};

export const getProfileUsers =
  ({ id, auth }) =>
  async (dispatch) => {
    dispatch({ type: PROFILE_TYPES.GET_ID, payload: id });
    try {
      dispatch({ type: PROFILE_TYPES.LOADING, payload: true });
      const res = getDataAPI(`/user/${id}`, auth.token);

      const res1 = getDataAPI(`/user_posts/${id}`, auth.token);
      const res2 = getDataAPI(`/user_albums/${id}`, auth.token);

      const users = await res;
      const posts = await res1;
      const albums = await res2;

      dispatch({ type: PROFILE_TYPES.GET_USER, payload: users.data });
      dispatch({
        type: PROFILE_TYPES.GET_POSTS,
        payload: { ...posts.data, _id: id, page: 2 }
      });
      dispatch({
        type: PROFILE_TYPES.GET_ALBUMS,
        payload: { ...albums.data, _id: id, page: 2 }
      });
      dispatch({ type: PROFILE_TYPES.LOADING, payload: false });
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const updateProfileUser =
  ({ userData, avatar, auth }) =>
  async (dispatch) => {
    if (!userData.fullname) return toast.error("Vui lòng điền họ và tên");
    if (userData.username.length > 25)
      return toast.error("Tên tài khoản của bạn quá dài.");
    if (userData.fullname.length > 25)
      return toast.error("Họ và tên của bạn quá dài.");
    if (userData.story.length > 200)
      return toast.error("Tiêu sử của bạn quá dài.");

    try {
      let media;
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });

      if (avatar) media = await imageUpload([avatar]);

      const res = await patchDataAPI(
        "user",
        {
          ...userData,
          avatar: avatar ? media[0].url : auth.user.avatar
        },
        auth.token
      );

      dispatch({
        type: GLOBALTYPES.AUTH,
        payload: {
          ...auth,
          user: {
            ...auth.user,
            ...userData,
            avatar: avatar ? media[0].url : auth.user.avatar
          }
        }
      });

      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });

      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
    }
  };

export const updateUser =
  ({ userData, picture, auth }) =>
  async (dispatch) => {
    if (!userData.fullname)
      return toast.error("Vui lòng điền họ và tên của bạn.");
    if (userData.username.length > 25)
      return toast.error("Tên tài khoản của bạn quá dài.");
    if (userData.fullname.length > 25)
      return toast.error("Họ và tên của bạn quá dài.");
    if (userData.story.length > 200)
      return toast.error("Câu chuyện của bạn quá dài.");

    try {
      let media;
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });

      if (picture) media = await imageUpload([picture]);

      const res = await patchDataAPI(
        "user",
        {
          ...userData,
          avatar: picture ? media[0].url : userData.avatar
        },
        auth.token
      );

      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });

      toast.success(res.data.msg);
      dispatch(getAllUsers(auth.token));
    } catch (err) {
      toast.error(err.response.data.msg);
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
    }
  };
export const changePassword =
  ({ oldPass, newPass, newConformPass, auth }) =>
  async (dispatch) => {
    if (!oldPass || !newPass || !newConformPass || !auth)
      return toast.error("Vui lòng điền đủ thông tin.");

    try {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });

      const res = await postDataAPI(
        "change_password",
        {
          oldPass,
          newPass,
          newConformPass
        },
        auth.token
      );

      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });

      toast.success(res.data.msg);
    } catch (err) {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
      toast.error(err.response.data.msg);
    }
  };

export const follow =
  ({ users, user, auth, socket }) =>
  async (dispatch) => {
    let newUser;

    if (users.every((item) => item._id !== user._id)) {
      newUser = { ...user, followers: [...user.followers, auth.user] };
    } else {
      users.forEach((item) => {
        if (item._id === user._id) {
          newUser = { ...item, followers: [...item.followers, auth.user] };
        }
      });
    }

    dispatch({
      type: PROFILE_TYPES.FOLLOW,
      payload: newUser
    });

    dispatch({
      type: GLOBALTYPES.AUTH,
      payload: {
        ...auth,
        user: { ...auth.user, following: [...auth.user.following, newUser] }
      }
    });

    try {
      const res = await patchDataAPI(
        `user/${user._id}/follow`,
        null,
        auth.token
      );
      // ! SOCKET HERE !
      socket.emit("follow", res.data.newUser);

      // Notify
      const msg = {
        id: auth.user._id,
        text: "has started to follow you.",
        recipients: [newUser._id],
        url: `/profile/${auth.user._id}`
      };

      dispatch(createNotify({ msg, auth, socket }));
    } catch (err) {
      // dispatch({
      //   type: GLOBALTYPES.ALERT,
      //   payload: { error: err.response.data.msg }
      // });
      toast.error(err.response.data.msg);
    }
  };

export const unfollow =
  ({ users, user, auth, socket }) =>
  async (dispatch) => {
    let newUser;

    if (users.every((item) => item._id !== user._id)) {
      newUser = {
        ...user,
        followers: DeleteData(user.followers, auth.user._id)
      };
    } else {
      users.forEach((item) => {
        if (item._id === user._id) {
          newUser = {
            ...item,
            followers: DeleteData(item.followers, auth.user._id)
          };
        }
      });
    }

    dispatch({
      type: PROFILE_TYPES.UNFOLLOW,
      payload: newUser
    });

    dispatch({
      type: GLOBALTYPES.AUTH,
      payload: {
        ...auth,
        user: {
          ...auth.user,
          following: DeleteData(auth.user.following, newUser._id)
        }
      }
    });

    try {
      const res = await patchDataAPI(
        `user/${user._id}/unfollow`,
        null,
        auth.token
      );
      const newArr = {
        newUser: res.data.newUser,
        updateUser: res.data.updateUser
      };
      socket.emit("unFollow", newArr);

      const msgNewAuthor = {
        id: res.data.updateUser._id,
        text: `Bạn đã đồng ý lời mời kết bạn của ${res.data.updateUser.username}`,
        recipients: [res.data.newUser._id],
        url: `/profile/${res.data.updateUser._id}`,
        user: res.data.updateUser
      };

      dispatch(removeNotify({ msg: msgNewAuthor, auth, socket }));
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const getRequestFl =
  ({ userId, token }) =>
  async (dispatch) => {
    try {
      const res = await getDataAPI(`requestFl/${userId}`, token);

      dispatch({
        type: PROFILE_TYPES.GET_REQUEST_FL,
        payload: res.data.msg
      });
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };
export const sendRequestFl =
  ({ userId, auth, socket }) =>
  async (dispatch) => {
    const content = "Gửi lời yêu cầu theo dõi";

    try {
      dispatch({ type: PROFILE_TYPES.LOADING, payload: true });
      const res = await postDataAPI(
        `requestFl`,
        { content, userId },
        auth.token
      );

      const msg = {
        id: res.data.getRequestResult.author._id,
        text: "Đã gửi yêu cầu theo dõi",
        recipients: [res.data.getRequestResult.user._id],
        url: `/profile/${res.data.getRequestResult.author._id}`,
        requestId: res.data.getRequestResult._id
      };
      dispatch(createNotify({ msg, auth, socket }));

      dispatch({ type: PROFILE_TYPES.LOADING, payload: false });

      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };
export const responseRequestFl =
  ({ msg, response, auth, socket }) =>
  async (dispatch) => {
    try {
      dispatch({ type: PROFILE_TYPES.LOADING, payload: true });

      const res = await postDataAPI(
        `requestFl/response`,
        { requestId: msg.requestId, response },
        auth.token
      );
      toast.success(res.data.msg);

      dispatch(removeNotify({ msg, auth, socket }));

      if (res?.data?.newUser && response) {
        const newArr = {
          newUser: res.data.newUser,
          updateUser: res.data.updateUser
        };
        socket.emit("follow", newArr);

        const msgNew = {
          id: auth.user._id,
          text: "Đã chấp nhận yêu cầu theo dõi của bạn.",
          recipients: [res.data.newUser._id],
          url: `/profile/${auth.user._id}`
        };
        const getUserFl = res.data.newUser;
        dispatch({
          type: PROFILE_TYPES.FOLLOW,
          payload: getUserFl
        });
        dispatch({
          type: GLOBALTYPES.AUTH,
          payload: { ...auth, user: newArr.updateUser }
        });
        dispatch(createNotify({ msg: msgNew, auth, socket }));

        const msgNewAuthor = {
          id: res.data.newUser._id,
          text: `Cảm ơn bạn đã đồng ý yêu cầu theo dõi.`,
          recipients: [auth.user._id],
          url: `/profile/${res.data.newUser._id}`,
          user: res.data.newUser
        };

        dispatch({
          type: NOTIFY_TYPES.CREATE_NOTIFY,
          payload: msgNewAuthor
        });
        dispatch(
          createNotify({
            msg: msgNewAuthor,
            userNotify: res.data.newUser,
            auth,
            socket
          })
        );

        dispatch({ type: PROFILE_TYPES.ALERT, payload: { loading: false } });
      }

      dispatch({ type: PROFILE_TYPES.LOADING, payload: false });
    } catch (err) {
      toast.error(err.response?.data?.msg);
    }
  };
