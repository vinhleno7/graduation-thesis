import { GLOBALTYPES } from "./globalTypes";
import { getProfileUsers } from "../../redux/actions/profileAction";
import { imageUpload } from "../../utils/imageUpload";
import { createNotify, removeNotify } from "./notifyAction";
import { getAllPosts } from "./a-postAction";
import {
  postDataAPI,
  getDataAPI,
  patchDataAPI,
  deleteDataAPI
} from "../../utils/fetchData";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

export const POST_TYPES = {
  CREATE_POST: "CREATE_POST",
  LOADING_POST: "LOADING_POST",
  GET_POSTS: "GET_POSTS",
  UPDATE_POST: "UPDATE_POST",
  GET_POST: "GET_POST",
  DELETE_POST: "DELETE_POST"
};

export const createPost =
  ({ content, images, auth, socket }) =>
  async (dispatch) => {
    let media = [];
    try {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });
      if (images.length > 0) media = await imageUpload(images);

      const res = await postDataAPI(
        "posts",
        { content, images: media },
        auth.token
      );
      dispatch({
        type: POST_TYPES.CREATE_POST,
        payload: { ...res.data.newPost, user: auth.user }
      });
      toast.success(res.data.msg);

      const id = auth.user._id;
      dispatch(getProfileUsers({ id, auth }));

      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
      // ! NOTIFY
      const msg = {
        id: res.data.newPost._id,
        text: "Đã đăng 1 bài viết",
        recipients: res.data.newPost.user.followers,
        url: `/post/${res.data.newPost._id}`,
        content,
        image: media[0].url
      };

      dispatch(createNotify({ msg, auth, socket }));
    } catch (err) {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });

      toast.error(err.response.data.msg);
    }
  };
export const adminCreatePost =
  ({ content, images, myUser, auth, socket }) =>
  async (dispatch) => {
    let media = [];
    try {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });
      if (images.length > 0) media = await imageUpload(images);

      const res = await postDataAPI(
        "adminPost",
        { content, images: media, myUser },
        auth.token
      );
      dispatch({
        type: POST_TYPES.CREATE_POST,
        payload: { ...res.data.newPost, user: myUser }
      });

      const id = myUser._id;
      dispatch(getProfileUsers({ id, auth }));

      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
      dispatch(getAllPosts(auth.token));
      toast.success(res.data.msg);

      // ! NOTIFY
      const msg = {
        id: res.data.newPost._id,
        text: "Đã đăng 1 bài viết.",
        recipients: res.data.newPost.user.followers,
        url: `/post/${res.data.newPost._id}`,
        content,
        image: media[0].url
      };

      dispatch(createNotify({ msg, auth, socket }));
    } catch (err) {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });

      toast.error(err.response.data.msg);
    }
  };

export const getPosts = (token) => async (dispatch) => {
  try {
    dispatch({ type: POST_TYPES.LOADING_POST, payload: true });
    const res = await getDataAPI("posts", token);
    dispatch({
      type: POST_TYPES.GET_POSTS,
      payload: { ...res.data, page: 2 }
    });

    dispatch({ type: POST_TYPES.LOADING_POST, payload: false });
  } catch (err) {
    dispatch({ type: POST_TYPES.LOADING_POST, payload: false });

    toast.error(err.response.data.msg);
  }
};

export const updatePost =
  ({ content, images, auth, status }) =>
  async (dispatch) => {
    let media = [];
    const imgNewUrl = images.filter((img) => !img.url);
    const imgOldUrl = images.filter((img) => img.url);
    if (
      status.content === content &&
      imgNewUrl.length === 0 &&
      imgOldUrl.length === status.images.length
    )
      return;
    try {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });
      if (imgNewUrl.length > 0) media = await imageUpload(imgNewUrl);
      const res = await patchDataAPI(
        `post/${status._id}`,
        { content, images: [...imgOldUrl, ...media] },
        auth.token
      );

      dispatch({
        type: POST_TYPES.UPDATE_POST,
        payload: { ...res.data.newPost, user: auth.user }
      });
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
      toast.success(res.data.msg);
      dispatch(getAllPosts(auth.token));
    } catch (err) {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });

      toast.error(err.response.data.msg);
    }
  };

export const likePost =
  ({ post, auth, socket }) =>
  async (dispatch) => {
    const newPost = { ...post, likes: [...post.likes, auth.user] };
    dispatch({ type: POST_TYPES.UPDATE_POST, payload: newPost });

    socket.emit("likePost", newPost);
    // !SOCKET HERE
    try {
      await patchDataAPI(`post/${post._id}/like`, null, auth.token);

      // Notify
      const msg = {
        id: auth.user._id,
        text: "Đã thích bài viết của bạn.",
        recipients: [post.user._id],
        url: `/post/${post._id}`,
        content: post.content,
        image: post.images[0].url
      };

      dispatch(createNotify({ msg, auth, socket }));
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const unLikePost =
  ({ post, auth, socket }) =>
  async (dispatch) => {
    const newPost = {
      ...post,
      likes: post.likes.filter((like) => like._id !== auth.user._id)
    };
    dispatch({ type: POST_TYPES.UPDATE_POST, payload: newPost });

    socket.emit("unLikePost", newPost);
    //! SOCKET HERE
    try {
      await patchDataAPI(`post/${post._id}/unlike`, null, auth.token);

      // Notify
      const msg = {
        id: auth.user._id,
        text: "Đã thích bài viết của bạn.",
        recipients: [post.user._id],
        url: `/post/${post._id}`
      };
      dispatch(removeNotify({ msg, auth, socket }));
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const getPost =
  ({ detailPost, id, auth }) =>
  async (dispatch) => {
    if (detailPost.every((post) => post._id !== id)) {
      try {
        const res = await getDataAPI(`post/${id}`, auth.token);
        dispatch({ type: POST_TYPES.GET_POST, payload: res.data.post });
      } catch (err) {
        toast.error(err.response.data.msg);
      }
    }
  };
export const deletePost =
  ({ post, auth, socket }) =>
  async (dispatch) => {
    try {
      const res = await deleteDataAPI(`post/${post._id}`, auth.token);
      // Notify
      const msg = {
        id: post._id,
        text: "Đã đăng 1 bài viết.",
        recipients: res.data.newPost.user.followers,
        url: `/post/${post._id}`
      };
      dispatch(removeNotify({ msg, auth, socket }));
      const id = auth.user._id;
      dispatch(getProfileUsers({ id, auth }));
      toast.success(res.data.msg);
      dispatch({ type: POST_TYPES.DELETE_POST, payload: post });
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const savePost =
  ({ post, auth }) =>
  async (dispatch) => {
    const newUser = { ...auth.user, saved: [...auth.user.saved, post._id] };
    dispatch({ type: GLOBALTYPES.AUTH, payload: { ...auth, user: newUser } });

    try {
      await patchDataAPI(`savePost/${post._id}`, null, auth.token);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const unSavePost =
  ({ post, auth }) =>
  async (dispatch) => {
    const newUser = {
      ...auth.user,
      saved: auth.user.saved.filter((id) => id !== post._id)
    };
    dispatch({ type: GLOBALTYPES.AUTH, payload: { ...auth, user: newUser } });

    try {
      await patchDataAPI(`unSavePost/${post._id}`, null, auth.token);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const report =
  ({ reason, post, auth, socket }) =>
  async (dispatch) => {
    try {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });

      const res = await postDataAPI(
        "report",
        { reason, postId: post._id },
        auth.token
      );

      toast.success(res.data.msg);

      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
    } catch (err) {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });

      toast.error(err.response.data.msg);
    }
  };
