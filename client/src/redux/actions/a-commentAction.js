import { GLOBALTYPES } from "./globalTypes";
import valid from "../../utils/valid";
import { POST_TYPES } from "../../redux/actions/postAction";
import { createNotify } from "../../redux/actions/notifyAction";
import {
  getDataAPI,
  postDataAPI,
  patchDataAPI,
  deleteDataAPI
} from "../../utils/fetchData";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

export const A_COMMENT_TYPES = {
  A_CREATE_COMMENT: "A_CREATE_COMMENT",
  A_LOADING_COMMENT: "A_LOADING_COMMENT",
  A_GET_COMMENTS: "A_GET_COMMENTS",
  A_UPDATE_COMMENT: "A_UPDATE_COMMENT",
  A_GET_COMMENT: "A_GET_COMMENT",
  A_DELETE_COMMENT: "A_DELETE_COMMENT"
};

export const getAllComments = (token) => async (dispatch) => {
  try {
    dispatch({ type: A_COMMENT_TYPES.A_LOADING_COMMENT, payload: true });

    const res = await getDataAPI(`comments`, token);

    dispatch({ type: A_COMMENT_TYPES.A_GET_COMMENTS, payload: res.data });

    dispatch({ type: A_COMMENT_TYPES.A_LOADING_COMMENT, payload: false });
  } catch (err) {
    toast.error(err.response.data.msg);
  }
};

export const createComment =
  (post, newComment, auth, socket) => async (dispatch) => {
    const newPost = { ...post, comments: [...post.comments, newComment] };
    dispatch({ type: POST_TYPES.UPDATE_POST, payload: newPost });
    try {
      const data = {
        ...newComment,
        postId: post._id,
        postUserId: post.user._id
      };

      const res = await postDataAPI("adminComment", data, auth.token);
      toast.success("ADMIN-Nhận xét thành công");
      dispatch(getAllComments(auth.token));

      const newData = { ...res.data.newComment, user: newComment.user };
      const newPost = { ...post, comments: [...post.comments, newData] };
      dispatch({ type: POST_TYPES.UPDATE_POST, payload: newPost });
      // Socket
      socket.emit("createComment", newPost);
      // Notify
      const msg = {
        id: res.data.newComment._id,
        text: newComment.reply
          ? `Đã gắn thẻ bạn vào 1 lời nhận xét: "${newComment.content.slice(
              0,
              20
            )}..."`
          : `Đã nhận xét vào bài viết của bạn: "${newComment.content.slice(
              0,
              20
            )}..."`,
        recipients: newComment.reply ? [newComment.tag._id] : [post.user._id],
        url: `/post/${post._id}`,
        content: post.content,
        image: post.images[0].url
      };
      const userNotify = newComment.user;
      dispatch(createNotify({ msg, userNotify, auth, socket }));
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const deleteComment =
  ({ comment, auth }) =>
  async (dispatch) => {
    try {
      const res = await deleteDataAPI(`comment/${comment._id}`, auth.token);

      dispatch(getAllComments(auth.token));

      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };
export const updateComment =
  ({ comment, content, auth }) =>
  async (dispatch) => {
    try {
      const res = await patchDataAPI(
        `comment/${comment._id}`,
        { content },
        auth.token
      );

      dispatch(getAllComments(auth.token));

      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };
