import { GLOBALTYPES } from "./globalTypes";
import { getProfileUsers } from "../../redux/actions/profileAction";
import { imageUpload } from "../../utils/imageUpload";
import { PROFILE_TYPES } from "./profileAction";
import {
  postDataAPI,
  getDataAPI,
  patchDataAPI,
  deleteDataAPI
} from "../../utils/fetchData";
import { createNotify, removeNotify } from "./notifyAction";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

export const ALBUM_TYPES = {
  CREATE_ALBUM: "CREATE_ALBUM",
  LOADING_ALBUM: "LOADING_ALBUM",
  GET_ALBUMS: "GET_ALBUMS",
  UPDATE_ALBUM: "UPDATE_ALBUM",
  GET_ALBUM: "GET_ALBUM",
  DELETE_ALBUM: "DELETE_ALBUM"
};

export const createAlbum =
  ({ content, images, mode, myFriend, auth }) =>
  async (dispatch) => {
    let myArrayFriends = [];
    let media = [];
    try {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });
      if (images.length > 0) media = await imageUpload(images);
      if (myFriend.length > 0)
        myFriend.map((fr) => myArrayFriends.push(fr._id));

      const res = await postDataAPI(
        "album",
        {
          name: content,
          images: media,
          visibility: mode,
          friend: myArrayFriends
        },
        auth.token
      );
      const res2 = getDataAPI(`/user_albums/${auth.user._id}`, auth.token);
      const albums = await res2;
      // const id = auth.user._id;
      // dispatch(getProfileUsers({ id, auth }));
      // dispatch({
      //   type: PROFILE_TYPES.CREATE_ALBUM,
      //   payload: myData
      // });
      dispatch({
        type: PROFILE_TYPES.GET_ALBUMS,
        payload: { ...albums.data, _id: auth.user._id, page: 2 }
      });
      toast.success(res.data.msg);

      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
    } catch (err) {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });

      toast.error(err.response.data.msg);
    }
  };

// export const getPosts = (token) => async (dispatch) => {
//   try {
//     dispatch({ type: POST_TYPES.LOADING_POST, payload: true });
//     const res = await getDataAPI("posts", token);
//     dispatch({
//       type: POST_TYPES.GET_POSTS,
//       payload: { ...res.data, page: 2 }
//     });

//     dispatch({ type: POST_TYPES.LOADING_POST, payload: false });
//   } catch (err) {
//     dispatch({ type: POST_TYPES.LOADING_POST, payload: false });

//     toast.error(err.response.data.msg);
//   }
// };

export const updateAlbum =
  ({ id, status, content, images, mode, myFriend, auth }) =>
  async (dispatch) => {
    let media = [];
    const imgNewUrl = images.filter((img) => !img.url);
    const imgOldUrl = images.filter((img) => img.url);
    if (mode === "public" || mode === "private") myFriend = [];
    if (
      status.content === content &&
      imgNewUrl.length === 0 &&
      imgOldUrl.length === status.images.length
    )
      return;
    try {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });
      if (imgNewUrl.length > 0) media = await imageUpload(imgNewUrl);
      const res = await patchDataAPI(
        `album/${id}`,
        {
          name: content,
          images: [...imgOldUrl, ...media],
          visibility: mode,
          friend: myFriend
        },
        auth.token
      );

      const res2 = getDataAPI(`/user_albums/${auth.user._id}`, auth.token);
      const albums = await res2;

      dispatch({
        type: PROFILE_TYPES.GET_ALBUMS,
        payload: { ...albums.data, _id: auth.user._id, page: 2 }
      });
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
      toast.success(res.data.msg);
    } catch (err) {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });

      toast.error(err.response.data.msg);
    }
  };

export const likeAlbum =
  ({ album, auth, socket }) =>
  async (dispatch) => {
    const newAlbum = { ...album, likes: [...album.likes, auth.user] };

    dispatch({ type: PROFILE_TYPES.UPDATE_ALBUM, payload: newAlbum });

    dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });

    socket.emit("likePost", newAlbum);
    // !SOCKET HERE
    try {
      await patchDataAPI(`album/${album._id}/like`, null, auth.token);
      const res2 = getDataAPI(`/user_albums/${auth.user._id}`, auth.token);
      const albums = await res2;

      dispatch({
        type: PROFILE_TYPES.GET_ALBUMS,
        payload: { ...albums.data, _id: auth.user._id, page: 2 }
      });
      // Notify
      const msg = {
        id: auth.user._id,
        text: "liked your album.",
        recipients: [album.user._id],
        url: `/profile/${album.user._id}`,
        content: album.name,
        image: album.images[0].url
      };

      dispatch(createNotify({ msg, auth, socket }));
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

export const unLikeAlbum =
  ({ album, auth, socket }) =>
  async (dispatch) => {
    const newAlbum = {
      ...album,
      likes: album.likes.filter((like) => like._id !== auth.user._id)
    };
    dispatch({ type: PROFILE_TYPES.UPDATE_ALBUM, payload: newAlbum });

    dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });

    socket.emit("unLikePost", newAlbum);
    //! SOCKET HERE
    try {
      await patchDataAPI(`album/${album._id}/unlike`, null, auth.token);
      const res2 = getDataAPI(`/user_albums/${auth.user._id}`, auth.token);
      const albums = await res2;

      dispatch({
        type: PROFILE_TYPES.GET_ALBUMS,
        payload: { ...albums.data, _id: auth.user._id, page: 2 }
      });
      // Notify
      const msg = {
        id: auth.user._id,
        text: "liked your album.",
        recipients: [album.user._id],
        url: `/profile/${album.user._id}`,
        content: album.name,
        image: album.images[0].url
      };
      dispatch(removeNotify({ msg, auth, socket }));
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };

// export const getPost =
//   ({ detailPost, id, auth }) =>
//   async (dispatch) => {
//     if (detailPost.every((post) => post._id !== id)) {
//       try {
//         const res = await getDataAPI(`post/${id}`, auth.token);
//         dispatch({ type: POST_TYPES.GET_POST, payload: res.data.post });
//       } catch (err) {
//         toast.error(err.response.data.msg);
//       }
//     }
//   };

export const deleteAlbum =
  ({ album, auth }) =>
  async (dispatch) => {
    try {
      const res = await deleteDataAPI(`album/${album._id}`, auth.token);
      toast.success(res.data.msg);

      const res2 = getDataAPI(`/user_albums/${auth.user._id}`, auth.token);

      const albums = await res2;

      dispatch({
        type: PROFILE_TYPES.GET_ALBUMS,
        payload: { ...albums.data, _id: auth.user._id, page: 2 }
      });
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };
