import { GLOBALTYPES } from "./globalTypes";
import valid from "../../utils/valid";
import {
  getDataAPI,
  postDataAPI,
  patchDataAPI,
  deleteDataAPI
} from "../../utils/fetchData";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

export const A_USER_TYPES = {
  A_CREATE_USER: "A_CREATE_USER",
  A_LOADING_USER: "A_LOADING_USER",
  A_GET_USERS: "A_GET_USERS",
  A_UPDATE_USER: "A_UPDATE_USER",
  A_GET_USER: "A_GET_USER",
  A_DELETE_USER: "A_DELETE_USER"
};

export const getAllUsers = (token) => async (dispatch) => {
  try {
    dispatch({ type: A_USER_TYPES.A_LOADING_USER, payload: true });

    const res = await getDataAPI(`users`, token);
    dispatch({ type: A_USER_TYPES.A_GET_USERS, payload: res.data });
    dispatch({ type: A_USER_TYPES.A_LOADING_USER, payload: false });
  } catch (err) {
    toast.error(err.response.data.msg);
  }
};
export const deleteUser =
  ({ user, auth }) =>
  async (dispatch) => {
    try {
      const res = await deleteDataAPI(`user/${user._id}`, auth.token);
      dispatch(getAllUsers(auth.token));
      toast.success(res.data.msg);
    } catch (err) {
      toast.error(err.response.data.msg);
    }
  };
