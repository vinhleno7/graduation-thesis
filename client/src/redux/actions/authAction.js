import { GLOBALTYPES } from "./globalTypes";
import { postDataAPI } from "../../utils/fetchData";
import valid from "../../utils/valid";
import { imageUpload } from "../../utils/imageUpload";
import { getAllUsers } from "./a-userAction";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

export const login = (data) => async (dispatch) => {
  try {
    dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });
    const res = await postDataAPI("login", data);
    dispatch({
      type: GLOBALTYPES.AUTH,
      payload: {
        token: res.data.access_token,
        user: res.data.user
      }
    });

    localStorage.setItem("firstLogin", true);
    dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });

    toast.success(res.data.msg);
  } catch (err) {
    dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
    toast.error(err.response.data.msg);
  }
};

export const refreshToken = () => async (dispatch) => {
  const firstLogin = localStorage.getItem("firstLogin");
  const preventAuth = localStorage.getItem("preventRefreshAuth");
  const check = preventAuth;
  localStorage.removeItem("preventRefreshAuth");

  if (check) {
    return toast.error("Phiên đăng nhập đã hết hạn. Vui lòng đăng nhập lại !");
  }
  if (firstLogin) {
    dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });

    try {
      const res = await postDataAPI("refresh_token");
      dispatch({
        type: GLOBALTYPES.AUTH,
        payload: {
          token: res.data.access_token,
          user: res.data.user
        }
      });

      dispatch({ type: GLOBALTYPES.ALERT, payload: {} });
    } catch (err) {
      // dispatch({
      //   type: GLOBALTYPES.ALERT,
      //   payload: {
      //     error: err.response.data.msg
      //   }
      // });
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });

      toast.error(err.response.data.msg);
    }
  }
};

export const register = (data) => async (dispatch) => {
  const check = valid(data);
  if (check.errLength > 0)
    return dispatch({ type: GLOBALTYPES.ALERT, payload: check.errMsg });

  try {
    dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });

    const res = await postDataAPI("register", data);
    dispatch({ type: GLOBALTYPES.VERIFY, payload: res.data.msg });

    // dispatch({
    //   type: GLOBALTYPES.AUTH,
    //   payload: {
    //     token: res.data.access_token,
    //     user: res.data.user
    //   }
    // });

    // localStorage.setItem("firstLogin", true);
    dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });

    toast.success(res.data.msg);
  } catch (err) {
    dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
    toast.error(err.response.data.msg);
  }
};

export const verify = (id) => async (dispatch) => {
  const res = await postDataAPI("verify", { id });

  try {
    dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });

    dispatch({
      type: GLOBALTYPES.AUTH,
      payload: {
        token: res.data.access_token,
        user: res.data.user
      }
    });

    localStorage.setItem("firstLogin", true);
    dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });

    toast.success(res.data.msg);
  } catch (err) {
    dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
    toast.error(err.response.data.msg);
  }
};
export const forgot_password = (emailForgot) => async (dispatch) => {
  const res = await postDataAPI("forgot_password", { email: emailForgot });

  try {
    dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });

    dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });

    toast.success(res.data.msg);
  } catch (err) {
    dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
    toast.error(err.response.data.msg);
  }
};
export const reset_password =
  ({ newPass, newConformPass, token }) =>
  async (dispatch) => {
    try {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });
      const res = await postDataAPI(
        "reset_password",
        { newPass, newConformPass },
        token
      );
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
      toast.success(res.data.msg);
    } catch (err) {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
      toast.error(err.response.data.msg);
    }
  };

export const createUser =
  ({ userData, picture, auth }) =>
  async (dispatch) => {
    if (picture === "")
      picture =
        "https://res.cloudinary.com/devatchannel/image/upload/v1602752402/avatar/avatar_cugq40.png";
    let media;
    dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });

    if (picture !== "") media = await imageUpload([picture]);
    const check = valid(userData);
    if (check.errLength > 0)
      return dispatch({ type: GLOBALTYPES.ALERT, payload: check.errMsg });

    dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
    const giveData = { ...userData, avatar: media[0].url };

    try {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: true } });
      const res = await postDataAPI("register", giveData);
      localStorage.setItem("firstLogin", false);
      localStorage.setItem("preventRefreshAuth", true);

      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
      toast.success(res.data.msg);
      dispatch(getAllUsers(auth.token));
    } catch (err) {
      dispatch({ type: GLOBALTYPES.ALERT, payload: { loading: false } });
      toast.error(err.response.data.msg);
    }
  };

export const logout = () => async (dispatch) => {
  try {
    localStorage.removeItem("firstLogin");
    await postDataAPI("logout");
    window.location.href = "/";
    toast.success("Đăng xuất thành công ");
  } catch (err) {
    // dispatch({
    //   type: GLOBALTYPES.ALERT,
    //   payload: {
    //     error: err.response.data.msg
    //   }
    // });
    toast.error(err.response.data.msg);
  }
};
