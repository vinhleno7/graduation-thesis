import { A_POST_TYPES } from "../actions/a-postAction";

const initialState = {
  loading: false,
  posts: [],
  reports: []
};

const aPostReducer = (state = initialState, action) => {
  switch (action.type) {
    case A_POST_TYPES.A_LOADING_POST:
      return {
        ...state,
        loading: action.payload
      };
    case A_POST_TYPES.A_GET_POSTS:
      return {
        ...state,
        count: action.payload.result,
        posts: action.payload.posts
      };
    case A_POST_TYPES.A_GET_REPORT:
      return {
        ...state,
        countRP: action.payload.reports.length,
        reports: action.payload.reports
      };
    default:
      return state;
  }
};

export default aPostReducer;
