import { GLOBALTYPES } from "../actions/globalTypes";

const statusReducer = (state = false, action) => {
  switch (action.type) {
    case GLOBALTYPES.STATUS:
      return action.payload;
    case GLOBALTYPES.EDIT_ALBUM:
      return action.payload;
    default:
      return state;
  }
};

export default statusReducer;
