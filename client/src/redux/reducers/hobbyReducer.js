import { HOBBY_TYPES } from "../actions/hobbyAction";
import { EditData, DeleteData } from "../actions/globalTypes";

const initialState = {
  loading: false,
  count: 0,
  hobbies: [],
  check: [],
  myHobby: []
};

const hobbyReducer = (state = initialState, action) => {
  switch (action.type) {
    case HOBBY_TYPES.LOADING_HOBBY:
      return {
        ...state,
        loading: action.payload
      };
    case HOBBY_TYPES.GET_ALL_HOBBY:
      return {
        ...state,
        count: action.payload.result,
        hobbies: action.payload.hobbies
      };
    case HOBBY_TYPES.CHECK_HOBBY:
      return {
        ...state,
        check: [...state.check, action.payload]
      };
    case HOBBY_TYPES.UN_CHECK_HOBBY:
      return {
        ...state,
        check: state.check.filter((hb) => hb !== action.payload)
      };
    case HOBBY_TYPES.GET_MY_HOBBY:
      return {
        ...state,
        myHobby: action.payload.myHobbies.hobbies
      };
    default:
      return state;
  }
};

export default hobbyReducer;
