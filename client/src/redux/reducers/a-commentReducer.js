import { A_COMMENT_TYPES } from "../actions/a-commentAction";

const initialState = {
  loading: false,
  comments: []
};

const aCommentReducer = (state = initialState, action) => {
  switch (action.type) {
    case A_COMMENT_TYPES.A_LOADING_COMMENT:
      return {
        ...state,
        loading: action.payload
      };
    case A_COMMENT_TYPES.A_GET_COMMENTS:
      return {
        ...state,
        count: action.payload.result,
        comments: action.payload.comments
      };
    default:
      return state;
  }
};

export default aCommentReducer;
