import { A_STORY_TYPES } from "../actions/a-storyAction";

const initialState = {
  loading: false,
  stories: []
};

const aStoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case A_STORY_TYPES.A_LOADING_STORY:
      return {
        ...state,
        loading: action.payload
      };
    case A_STORY_TYPES.A_GET_STORIES:
      return {
        ...state,
        count: action.payload.result,
        stories: action.payload.stories
      };
    default:
      return state;
  }
};

export default aStoryReducer;
