import { A_USER_TYPES } from "../actions/a-userAction";

const initialState = {
  loading: false,
  users: []
};

const aUserReducer = (state = initialState, action) => {
  switch (action.type) {
    case A_USER_TYPES.A_LOADING_USER:
      return {
        ...state,
        loading: action.payload
      };
    case A_USER_TYPES.A_GET_USERS:
      return {
        ...state,
        count: action.payload.result,
        users: action.payload.users
      };
    default:
      return state;
  }
};

export default aUserReducer;
