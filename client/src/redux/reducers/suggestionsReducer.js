import { SUGGEST_TYPES } from "../actions/suggestionsAction";

const initialState = {
  loading: false,
  users: []
};

const suggestionsReducer = (state = initialState, action) => {
  switch (action.type) {
    case SUGGEST_TYPES.LOADING:
      return {
        ...state,
        loading: action.payload
      };
    case SUGGEST_TYPES.GET_USERS:
      // function getIndexOfMember(memberArray, member) {
      //   return memberArray.findIndex((m) => m._id === member._id);
      // }

      // const uniqueValues = action.payload.newArrKNN.filter(
      //   (m, index, ms) => getIndexOfMember(ms, m) === index
      // );

      return {
        ...state,
        users: action.payload.newArrHobby
      };
    default:
      return state;
  }
};

export default suggestionsReducer;
