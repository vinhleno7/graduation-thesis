const valid = ({ fullname, username, email, password, cf_password }) => {
  const err = {};
  if (!fullname) {
    err.fullname = "Làm ơn điền họ và tên";
  } else if (fullname.length > 25) {
    err.fullname = "Họ tên không được quá 25 ký tự";
  }

  if (!username) {
    err.username = "Làm ơn điền tên tài khoản.";
  } else if (username.replace(/ /g, "").length > 25) {
    err.username = "Tên tài khoản không được quá 25 ký tự.";
  }

  if (!email) {
    err.email = "Làm ơn điền địa chỉ email.";
  } else if (!validateEmail(email)) {
    err.email = "Địa chỉ email không đúng định dạng.";
  }

  if (!password) {
    err.password = "Làm ơn điền mật khẩu.";
  } else if (password.length < 6) {
    err.password = "Mật khẩu phải hơn 6 ký tự.";
  }

  if (password !== cf_password) {
    err.cf_password = "Mật khẩu xác nhận không khớp.";
  }
  if (!cf_password) {
    err.cf_password = "Làm ơn điền mật khẩu xác nhận.";
  }
  return {
    errMsg: err,
    errLength: Object.keys(err).length
  };
};

function validateEmail(email) {
  // eslint-disable-next-line
  const re =
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

export default valid;
