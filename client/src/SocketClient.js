import React, { useEffect, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { POST_TYPES } from "./redux/actions/postAction";
import { GLOBALTYPES } from "./redux/actions/globalTypes";

import { PROFILE_TYPES, getProfileUsers } from "./redux/actions/profileAction";
import { NOTIFY_TYPES, removeNotify } from "./redux/actions/notifyAction";
import { MESS_TYPES } from "./redux/actions/messageAction";

import notifyIphone from "./audio/SMSIPhoneRingtone.mp3";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

const SocketClient = () => {
  // ! Create notify in Notify PC desktop
  const spawnNotification = (body, icon, url, title) => {
    let options = {
      body,
      icon
    };
    let n = new Notification(title, options);

    n.onClick = (e) => {
      e.preventDefault();
      window.open(url, "_blank");
    };
  };

  const audioRef = useRef();

  const { auth, socket, notify, online, call } = useSelector((state) => state);
  const dispatch = useDispatch();

  // joinUser
  useEffect(() => {
    socket.emit("joinUser", auth.user);
  }, [socket, auth.user]);

  // Likes
  useEffect(() => {
    socket.on("likeToClient", (newPost) => {
      dispatch({ type: POST_TYPES.UPDATE_POST, payload: newPost });
    });
    return () => socket.off("likeToClient");
  }, [socket, dispatch]);

  // Un Likes
  useEffect(() => {
    socket.on("unLikeToClient", (newPost) => {
      dispatch({ type: POST_TYPES.UPDATE_POST, payload: newPost });
    });
    return () => socket.off("unLikeToClient");
  }, [socket, dispatch]);

  // Comments
  useEffect(() => {
    socket.on("createCommentToClient", (newPost) => {
      dispatch({ type: POST_TYPES.UPDATE_POST, payload: newPost });
    });
    return () => socket.off("createCommentToClient");
  }, [socket, dispatch]);

  useEffect(() => {
    socket.on("deleteCommentToClient", (newPost) => {
      dispatch({ type: POST_TYPES.UPDATE_POST, payload: newPost });
    });
    return () => socket.off("deleteCommentToClient");
  }, [socket, dispatch]);

  // FOLLOW

  useEffect(() => {
    socket.on("followToClient", (newArr) => {
      dispatch({
        type: GLOBALTYPES.AUTH,
        payload: { ...auth, user: newArr.newUser }
      });
      dispatch({
        type: PROFILE_TYPES.FOLLOW,
        payload: newArr.updateUser
      });
    });
    return () => socket.off("followToClient");
  }, [socket, dispatch, auth]);

  useEffect(() => {
    socket.on("unFollowToClient", (newArr) => {
      dispatch({
        type: GLOBALTYPES.AUTH,
        payload: { ...auth, user: newArr.updateUser }
      });

      dispatch({
        type: PROFILE_TYPES.UNFOLLOW,
        payload: newArr.newUser
      });

      // Notify
      const msg = {
        id: newArr.newUser._id,
        text: "Đã chấp nhận yêu cầu theo dõi của bạn.",
        recipients: [newArr.updateUser._id],
        url: `/profile/${newArr.newUser._id}`
      };

      dispatch(removeNotify({ msg, auth, socket }));
    });
    return () => socket.off("unFollowToClient");
  }, [socket, dispatch, auth]);

  // Notification
  useEffect(() => {
    socket.on("createNotifyToClient", (msg) => {
      dispatch({ type: NOTIFY_TYPES.CREATE_NOTIFY, payload: msg });

      if (notify.sound) {
        var playedPromise = audioRef.current.play();
        if (playedPromise) {
          playedPromise
            .catch((e) => {
              console.log(e);
              if (
                e.name === "NotAllowedError" ||
                e.name === "NotSupportedError"
              ) {
                console.log(e.name);
              }
            })
            .then(() => {
              console.log("playing sound !!!");
            });
        }
      }
      spawnNotification(
        msg.user.username + " " + msg.text,
        msg.user.avatar,
        msg.url,
        "LV Social"
      );
    });

    return () => socket.off("createNotifyToClient");
  }, [socket, dispatch, notify.sound]);

  useEffect(() => {
    socket.on("removeNotifyToClient", (msg) => {
      dispatch({ type: NOTIFY_TYPES.REMOVE_NOTIFY, payload: msg });
    });

    return () => socket.off("removeNotifyToClient");
  }, [socket, dispatch]);

  // Message
  useEffect(() => {
    socket.on("addMessageToClient", (msg) => {
      dispatch({ type: MESS_TYPES.ADD_MESSAGE, payload: msg });

      dispatch({
        type: MESS_TYPES.ADD_USER,
        payload: {
          ...msg.user,
          text: msg.text,
          media: msg.media
        }
      });
      audioRef.current.play();
    });
    return () => socket.off("addMessageToClient");
  }, [socket, dispatch]);

  // Check User Online / Offline
  useEffect(() => {
    socket.emit("checkUserOnline", auth.user);
  }, [socket, auth.user]);

  useEffect(() => {
    socket.on("checkUserOnlineToMe", (data) => {
      data.forEach((item) => {
        if (!online.includes(item.id)) {
          dispatch({ type: GLOBALTYPES.ONLINE, payload: item.id });
        }
      });
    });

    return () => socket.off("checkUserOnlineToMe");
  }, [socket, dispatch, online]);

  useEffect(() => {
    socket.on("checkUserOnlineToClient", (id) => {
      if (!online.includes(id)) {
        dispatch({ type: GLOBALTYPES.ONLINE, payload: id });
      }
    });

    return () => socket.off("checkUserOnlineToClient");
  }, [socket, dispatch, online]);

  // Check User Offline
  useEffect(() => {
    socket.on("CheckUserOffline", (id) => {
      dispatch({ type: GLOBALTYPES.OFFLINE, payload: id });
    });

    return () => socket.off("CheckUserOffline");
  }, [socket, dispatch]);

  // Call User
  useEffect(() => {
    socket.on("callUserToClient", (data) => {
      dispatch({ type: GLOBALTYPES.CALL, payload: data });
    });

    return () => socket.off("callUserToClient");
  }, [socket, dispatch]);

  useEffect(() => {
    socket.on("userBusy", (data) => {
      toast.error(`${call.username} is busy!`);
    });

    return () => socket.off("userBusy");
  }, [socket, dispatch, call]);

  return (
    <>
      <audio
        // loop
        // muted
        autoPlay
        controls
        ref={audioRef}
        style={{ display: "none" }}
      >
        <source src={notifyIphone} type="audio/mp3" />
      </audio>
    </>
  );
};

export default SocketClient;
