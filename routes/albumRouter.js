const router = require("express").Router();
const albumCtrl = require("../controllers/albumCtrl");
const auth = require("../middleware/auth");

router.route("/albums").get(auth, albumCtrl.getAlbums);

router.route("/album").post(auth, albumCtrl.createAlbum);

router
  .route("/album/:id")
  .patch(auth, albumCtrl.updateAlbum)
  .get(auth, albumCtrl.getAlbum)
  .delete(auth, albumCtrl.deleteAlbum);

router.patch("/album/:id/like", auth, albumCtrl.likeAlbum);
router.patch("/album/:id/unlike", auth, albumCtrl.unLikeAlbum);

router.get("/user_albums/:id", auth, albumCtrl.getUserAlbum);

module.exports = router;
