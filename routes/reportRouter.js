const router = require("express").Router();
const auth = require("../middleware/auth");
const reportCtrl = require("../controllers/reportCtrl");

router.post("/report", auth, reportCtrl.create);

router.post("/report/response", auth, reportCtrl.response);

router.get("/report/getAll", auth, reportCtrl.getAll);

// router.patch("/isReadNotify/:id", auth, notifyCtrl.isReadNotify);

// router.delete("/deleteAllNotify", auth, notifyCtrl.deleteAllNotifies);

module.exports = router;
