const router = require("express").Router();
const auth = require("../middleware/auth");
const storyCtrl = require("../controllers/storyCtrl");

router
  .route("/story")
  .post(auth, storyCtrl.createStory)
  .get(auth, storyCtrl.getStories);

router.get("/stories", auth, storyCtrl.getAllStories);

router
  .route("/story/:id")
  .get(auth, storyCtrl.getStoriesByUser)
  .delete(auth, storyCtrl.deleteStory);

router.patch("/story/:id/like", auth, storyCtrl.likeStory);
router.patch("/story/:id/unlike", auth, storyCtrl.unLikeStory);

router.patch("/isReadStory/:id", auth, storyCtrl.isReadStory);

module.exports = router;
