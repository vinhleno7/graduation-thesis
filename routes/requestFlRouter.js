const router = require("express").Router();
const auth = require("../middleware/auth");
const requestFlCtrl = require("../controllers/requestFlCtrl");

router.post("/requestFl", auth, requestFlCtrl.createRequestFl);

router.post("/requestFl/response", auth, requestFlCtrl.responseRequest);

router.delete("/requestFl/:id", auth, requestFlCtrl.removeRequestFl);

router.get("/requestFl/:id", auth, requestFlCtrl.checkRequest);

// router.patch("/isReadNotify/:id", auth, notifyCtrl.isReadNotify);

// router.delete("/deleteAllNotify", auth, notifyCtrl.deleteAllNotifies);

module.exports = router;
