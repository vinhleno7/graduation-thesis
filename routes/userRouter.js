const router = require("express").Router();
const auth = require("../middleware/auth");
const userCtrl = require("../controllers/userCtrl");
const authCtrl = require("../controllers/authCtrl");

router.get("/search", auth, userCtrl.searchUser);

router.get("/user/:id", auth, userCtrl.getUser);

router.delete("/user/:id", auth, userCtrl.deleteUser);

router.get("/users", auth, userCtrl.getAllUsers);

router.patch("/user", auth, userCtrl.updateUser);

router.patch("/user/:id/follow", auth, userCtrl.follow);

router.patch("/user/:id/unfollow", auth, userCtrl.unfollow);

router.get("/suggestionsUser", auth, userCtrl.suggestionsUser);

router.post("/reset_password", auth, authCtrl.resetPassword);

module.exports = router;
