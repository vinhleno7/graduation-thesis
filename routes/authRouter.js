const router = require("express").Router();
const authCtrl = require("../controllers/authCtrl");
const auth = require("../middleware/auth");

router.post("/register", authCtrl.register);

router.post("/verify", authCtrl.activateEmail);

router.post("/login", authCtrl.login);

router.post("/logout", authCtrl.logout);

router.post("/refresh_token", authCtrl.generateAccessToken);

router.post("/forgot_password", authCtrl.forgotPassword);

router.post("/change_password", auth, authCtrl.changPassword);

module.exports = router;
