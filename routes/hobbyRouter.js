const router = require("express").Router();
const auth = require("../middleware/auth");
const hobbyCtrl = require("../controllers/hobbyCtrl");

router.post("/hobby/create", auth, hobbyCtrl.create);

router.post("/hobby/setHobbyUser", auth, hobbyCtrl.setHobbyUser);

router.get("/hobby/getAll", auth, hobbyCtrl.getAllHobbies);

router.get("/hobby/getUser", auth, hobbyCtrl.getUserHobby);

module.exports = router;
