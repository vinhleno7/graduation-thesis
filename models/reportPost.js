const mongoose = require("mongoose");

const reportSchema = new mongoose.Schema(
  {
    reason: String,
    post: { type: mongoose.Types.ObjectId, ref: "post" },
    author: { type: mongoose.Types.ObjectId, ref: "user" },
    status: Boolean
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("report", reportSchema);
