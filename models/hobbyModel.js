const mongoose = require("mongoose");

const hobbySchema = new mongoose.Schema(
  {
    name: String,
    avatar: String
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("hobby", hobbySchema);
