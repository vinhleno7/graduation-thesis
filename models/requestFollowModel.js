const mongoose = require("mongoose");

const requestFlSchema = new mongoose.Schema(
  {
    content: String,
    author: { type: mongoose.Types.ObjectId, ref: "user" },
    user: { type: mongoose.Types.ObjectId, ref: "user" },
    status: Boolean
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("requestFl", requestFlSchema);
