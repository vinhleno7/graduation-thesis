const mongoose = require("mongoose");

const storySchema = new mongoose.Schema(
  {
    content: String,
    images: {
      type: Array,
      required: true
    },
    likes: [{ type: mongoose.Types.ObjectId, ref: "user" }],
    // comments: [{ type: mongoose.Types.ObjectId, ref: "comment" }],
    isRead: [{ type: mongoose.Types.ObjectId, ref: "user" }],
    user: { type: mongoose.Types.ObjectId, ref: "user" }
    // expireAt: {
    //   type: Date,
    //   default: Date.now,
    //   index: { expires: 86400 }
    // }
    // expireAt: { type: Date, default: Date.now, expires: "31h" },
    // createdAt: { type: Date, default: Date.now, expires: "1h" }
  },
  {
    timestamps: true
  }
);
storySchema.index({ createdAt: 1 }, { expireAfterSeconds: 60 * 60 * 24 });
module.exports = mongoose.model("story", storySchema);
