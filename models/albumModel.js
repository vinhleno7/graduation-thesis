const mongoose = require("mongoose");

const albumSchema = new mongoose.Schema(
  {
    name: String,
    images: {
      type: Array,
      required: true
    },
    likes: [{ type: mongoose.Types.ObjectId, ref: "user" }],
    comments: [{ type: mongoose.Types.ObjectId, ref: "comment" }],
    user: { type: mongoose.Types.ObjectId, ref: "user" },
    friend: [{ type: mongoose.Types.ObjectId, ref: "user" }],
    visibility: {
      type: String,
      enum: ["public", "private", "friend"],
      default: "public"
    }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("album", albumSchema);
