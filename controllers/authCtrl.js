const Users = require("../models/userModel");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const sendMail = require("../utils/sendMail");

const { CLIENT_URL } = process.env;

const authCtrl = {
  register: async (req, res) => {
    try {
      const {
        avatar,
        role,
        mobile,
        story,
        address,
        website,
        fullname,
        username,
        email,
        password,
        gender
      } = req.body;
      let newUserName = username.toLowerCase().replace(/ /g, "");

      const user_name = await Users.findOne({ username: newUserName });
      if (user_name)
        return res.status(400).json({ msg: "Đã tồn tại tên tài khoản." });

      const user_email = await Users.findOne({ email });
      if (user_email) return res.status(400).json({ msg: "Email đã tồn tại" });

      if (password.length < 6)
        return res.status(400).json({ msg: "Mật khẩu phải có hơn 6 ký tự" });

      const passwordHash = await bcrypt.hash(password, 12);

      const newUser = new Users({
        avatar,
        fullname,
        username: newUserName,
        email,
        role,
        website,
        mobile,
        story,
        address,
        password: passwordHash,
        gender
      });

      // const access_token = createAccessToken({ id: newUser._id });
      // const refresh_token = createRefreshToken({ id: newUser._id });

      // res.cookie("refreshtoken", refresh_token, {
      //   httpOnly: true,
      //   path: "/api/refresh_token",
      //   maxAge: 30 * 24 * 60 * 60 * 1000 // 30days
      // });

      // await newUser.save();

      // res.json({
      //   msg: "Register Success!",
      //   access_token,
      //   user: {
      //     ...newUser._doc,
      //     password: ""
      //   }
      // });
      const id = createActivationToken(newUser);

      const url = `${
        CLIENT_URL ? CLIENT_URL : "https://lv-social.herokuapp.com"
      }/verify/${id}`;
      sendMail(email, url, "Xác minh địa chỉ email của bạn");

      res.json({
        msg: ` Verify ${email}`
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  activateEmail: async (req, res) => {
    try {
      const { id } = req.body;
      const user = jwt.verify(id, process.env.ACTIVATION_TOKEN_SECRET);

      const {
        avatar,
        fullname,
        username,
        email,
        role,
        website,
        mobile,
        story,
        address,
        password,
        gender
      } = user;

      const newUser = new Users({
        avatar,
        fullname,
        username,
        email,
        role,
        website,
        mobile,
        story,
        address,
        password,
        gender
      });

      const check = await Users.findOne({ email });

      if (check) return res.status(400).json({ msg: "Email đã tồn tại." });

      const access_token = createAccessToken({ id: newUser._id });
      const refresh_token = createRefreshToken({ id: newUser._id });

      res.cookie("refreshtoken", refresh_token, {
        httpOnly: true,
        path: "/api/refresh_token",
        maxAge: 30 * 24 * 60 * 60 * 1000 // 30days
      });

      await newUser.save();

      res.json({
        msg: "Account has been verified!",
        access_token,
        user: {
          ...newUser._doc,
          password: ""
        }
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  login: async (req, res) => {
    try {
      const { email, password } = req.body;

      const user = await Users.findOne({ email }).populate(
        "followers following stories hobbies",
        "avatar username fullname followers following content images"
      );

      if (!user) return res.status(400).json({ msg: "Đã tồn tại email này" });

      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch) return res.status(400).json({ msg: "Mật khẩu không đúng" });

      const access_token = createAccessToken({ id: user._id });
      const refresh_token = createRefreshToken({ id: user._id });

      res.cookie("refreshtoken", refresh_token, {
        httpOnly: true,
        path: "/api/refresh_token",
        maxAge: 30 * 24 * 60 * 60 * 1000 // 30days
      });

      res.json({
        msg: "Đăng nhập thành công!",
        access_token,
        user: {
          ...user._doc,
          password: ""
        }
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  logout: async (req, res) => {
    try {
      res.clearCookie("refreshtoken", { path: "/api/refresh_token" });
      return res.json({ msg: "Đã đăng xuất!" });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  forgotPassword: async (req, res) => {
    try {
      const { email } = req.body;
      const user = await Users.findOne({ email });
      if (!user) return res.status(400).json({ msg: "Đã tồn tại email này." });

      const id = createAccessToken({ id: user._id });
      const url = `${
        CLIENT_URL ? CLIENT_URL : "https://lv-social.herokuapp.com"
      }/reset/${id}`;

      sendMail(email, url, "Tạo lại mật khẩu");
      res.json({
        msg: "Đã gửi thư cài lại mật khẩu, vui lòng kiểm tra email."
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  resetPassword: async (req, res) => {
    try {
      const { newPass, newConformPass } = req.body;

      if (!newPass || !newConformPass || !req.user)
        return res
          .status(400)
          .json({ msg: "Có gì đó sai rồi. Vui lòng kiểm tra lại." });

      if (newPass !== newConformPass)
        return res.status(400).json({ msg: "Mật khẩu không khớp." });

      const passwordHash = await bcrypt.hash(newPass, 12);

      await Users.findOneAndUpdate(
        { _id: req.user.id },
        {
          password: passwordHash
        }
      );

      res.json({ msg: "Đổi mật khẩu thành công." });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },

  changPassword: async (req, res) => {
    try {
      const { oldPass, newPass, newConformPass } = req.body;
      if (!oldPass || !newPass || !newConformPass || !req.user)
        return res
          .status(400)
          .json({ msg: "Có gì đó sai rồi. Vui lòng kiểm tra lại." });

      const user = await Users.findOne({ _id: req.user._id }).populate(
        "followers following stories hobbies",
        "avatar username fullname followers following content images"
      );

      const isMatch = await bcrypt.compare(oldPass, user.password);

      if (!isMatch)
        return res.status(400).json({ msg: "Mật khẩu cũ của bạn không đúng" });

      if (newPass !== newConformPass)
        return res.status(400).json({ msg: "Mật khẩu mới không khớp." });

      const passwordHash = await bcrypt.hash(newPass, 12);

      await Users.findOneAndUpdate(
        { _id: req.user.id },
        {
          password: passwordHash
        }
      );

      res.json({ msg: "Đổi mật khẩu thành công." });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  generateAccessToken: async (req, res) => {
    //refresh token khi reload lai trang, de co du lieu nguoi dung.
    try {
      const rf_token = req.cookies.refreshtoken;
      if (!rf_token) return res.status(400).json({ msg: "Làm ơn đăng nhập" });

      jwt.verify(
        rf_token,
        process.env.REFRESH_TOKEN_SECRET,
        async (err, result) => {
          if (err) return res.status(400).json({ msg: "Làm ơn đăng nhập" });

          const user = await Users.findById(result.id)
            .select("-password")
            .populate(
              "followers following",
              "avatar username fullname followers following"
            );

          if (!user)
            return res.status(400).json({ msg: "Người dùng không tồn tại" });

          const access_token = createAccessToken({ id: result.id });

          res.json({
            access_token,
            user
          });
        }
      );
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  }
};

const createActivationToken = (payload) => {
  return jwt.sign(payload.toJSON(), process.env.ACTIVATION_TOKEN_SECRET, {
    expiresIn: "5m"
  });
};

// Tạo token dựa vào id người dùng
const createAccessToken = (payload) => {
  return jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: "1d"
  });
};

const createRefreshToken = (payload) => {
  return jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET, {
    expiresIn: "30d"
  });
};

module.exports = authCtrl;
