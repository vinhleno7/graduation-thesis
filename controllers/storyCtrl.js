const Stories = require("../models/storyModel");
const Users = require("../models/userModel");

const storyCtrl = {
  createStory: async (req, res) => {
    try {
      const { content, images, myUser } = req.body;
      if (!content)
        return res.status(400).json({ msg: "Làm ơn điền nội dung." });
      if (images.length === 0)
        return res.status(400).json({ msg: "Làm ơn điền ảnh." });
      if (myUser) {
        const newStory = new Stories({
          content,
          images,
          user: myUser._id
        });
        await newStory.save();
        await Users.findOneAndUpdate(
          {
            _id: myUser._id
          },
          {
            $push: { stories: newStory._id }
          },
          { new: true }
        );
        return res.json({
          msg: "Tạo câu chuyện bởi ADMIN !",
          newStory: {
            ...newStory._doc,
            user: myUser
          }
        });
      }
      const newStory = new Stories({
        content,
        images,
        user: req.user._id
      });
      await newStory.save();
      await Users.findOneAndUpdate(
        {
          _id: req.user._id
        },
        {
          $push: { stories: newStory._id }
        },
        { new: true }
      );
      res.json({
        msg: "Tạo câu chuyện thành công !",
        newStory: {
          ...newStory._doc,
          user: req.user
        }
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getAllStories: async (req, res) => {
    try {
      const stories = await Stories.find().sort("-updatedAt").populate("user");

      res.json({ msg: "Success!", result: stories.length, stories });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getStories: async (req, res) => {
    try {
      const myUser = await Users.findOne(req.user._id);
      let myArr = [req.user._id, ...myUser.following];

      let myArrUsers = [];

      const checkUsers = await Users.find({ _id: myArr }).populate("stories");

      const stories = checkUsers.filter((person) => person.stories.length > 0);

      // const stories = await Stories.find({
      //   user: myArr
      // }).populate("user");

      res.json({ stories });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getStoriesByUser: async (req, res) => {
    try {
      const checkUser = await Users.findOne({ username: req.params.id });

      const myStories = await Stories.find({ user: checkUser._id });

      res.json({ myStories });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },

  deleteStory: async (req, res) => {
    try {
      const story = await Stories.findOneAndDelete({
        _id: req.params.id,
        user: req.user._id
      });
      res.json({
        msg: "Xóa câu chuyện thành công",
        newStory: {
          ...story,
          user: req.user
        }
      });
    } catch (er) {
      return res.status(500).json({ msg: er.message });
    }
  },

  isReadStory: async (req, res) => {
    try {
      const story = await Stories.findOneAndUpdate(
        { _id: req.params.id },
        {
          $push: { isRead: req.user._id }
        }
      );

      return res.json({ story });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },

  likeStory: async (req, res) => {
    try {
      const story = await Stories.find({
        _id: req.params.id,
        likes: req.user._id
      });
      if (story.length > 0)
        return res.status(400).json({ msg: "Bạn đã thích câu chuyện này" });

      const like = await Stories.findOneAndUpdate(
        { _id: req.params.id },
        { $push: { likes: req.user._id } },
        { new: true }
      );

      if (!like)
        return res.status(400).json({ msg: "Câu chuyện này không tồn tại" });

      const newStory = await Stories.findOne({ _id: req.params.id }).populate(
        "user"
      );
      res.json({ msg: "Đã thích câu chuyện này", newStory });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  unLikeStory: async (req, res) => {
    try {
      const like = await Stories.findOneAndUpdate(
        { _id: req.params.id },
        { $pull: { likes: req.user._id } },
        { new: true }
      );

      if (!like)
        return res.status(400).json({ msg: "Câu chuyện này không tồn tại" });

      res.json({ msg: "Đã hủy thích câu chuyện" });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  }
};

module.exports = storyCtrl;
