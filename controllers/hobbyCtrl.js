const Hobbies = require("../models/hobbyModel");
const Users = require("../models/userModel");

const hobbyCtrl = {
  create: async (req, res) => {
    try {
      const { name, avatar } = req.body;

      if (!name)
        return res
          .status(400)
          .json({ msg: "Vui lòng điền tên sở thích của bạn" });

      if (!avatar)
        return res.status(400).json({ msg: "Vui lòng chọn ảnh cho sở thích" });

      const newHobby = new Hobbies({
        name,
        avatar
      });
      await newHobby.save();

      res.json({
        msg: "Tạo sở thích thành công !",
        newHobby: {
          ...newHobby._doc
        }
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getAllHobbies: async (req, res) => {
    try {
      const hobbies = await Hobbies.find().sort("name");

      res.json({
        msg: "Lấy tất cả sở thích thành công!",
        result: hobbies.length,
        hobbies
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getUserHobby: async (req, res) => {
    try {
      const myHobbies = await Users.findOne({ _id: req.user._id }).populate(
        "hobbies"
      );

      res.json({
        myHobbies
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  setHobbyUser: async (req, res) => {
    try {
      const { myHobby } = req.body;

      const hobbies = await Hobbies.find({ _id: myHobby }).sort("name");

      await Users.findOneAndUpdate({ _id: req.user._id }, { hobbies });

      res.json({
        msg: "Thiết lập sở thích thành công.",
        hobbies
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  }
};

module.exports = hobbyCtrl;
