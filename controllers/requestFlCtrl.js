const RequestFl = require("../models/requestFollowModel");
const Users = require("../models/userModel");

const requestFlCtrl = {
  createRequestFl: async (req, res) => {
    try {
      const { content, userId } = req.body;

      const authorId = req.user._id;

      if (!content)
        return res.status(400).json({ msg: "Vui lòng điền nội dung." });

      if (!userId)
        return res.status(400).json({ msg: "Vui lòng chọn người để theo dõi" });

      if (!authorId)
        return res.status(400).json({ msg: "Vui lòng xác thực người dùng" });

      const user = await Users.find({
        _id: userId,
        followers: req.user._id
      });
      if (user.length > 0)
        return res.status(500).json({ msg: "Bạn đã theo dõi người này." });

      const userFollow = await Users.findOne({ _id: userId });

      const newRequestFl = new RequestFl({
        content,
        author: authorId,
        user: userId
      });

      await newRequestFl.save();

      const getRequestResult = await RequestFl.findOne({
        _id: newRequestFl._id
      }).populate("author user");

      res.json({
        msg: `Đã gửi yêu cầu theo dõi đến ${userFollow._doc.username}`,
        getRequestResult
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  responseRequest: async (req, res) => {
    try {
      const { requestId, response } = req.body;

      if (!requestId)
        return res
          .status(400)
          .json({ msg: "Vui lòng chọn yêu cầu để đáp trả" });

      const checkRequestFl = await RequestFl.findById(requestId).populate(
        "author user"
      );

      if (!checkRequestFl)
        return res.status(404).json({ msg: "Yêu cầu không tồn tại !" });

      const user = await Users.find({
        _id: checkRequestFl.user._id,
        followers: checkRequestFl.author._id
      });
      if (user.length > 0)
        return res.status(500).json({
          msg: `${checkRequestFl.author.username} đã theo dõi bạn rồi.`
        });

      const requestFl = await RequestFl.findOneAndUpdate(
        {
          _id: requestId
        },
        { status: response }
      );

      if (response) {
        const updateUser = await Users.findOneAndUpdate(
          { _id: checkRequestFl.user._id },
          {
            $push: { followers: checkRequestFl.author._id }
          },
          { new: true }
        ).populate("followers following", "-password");

        const newUser = await Users.findOneAndUpdate(
          { _id: checkRequestFl.author._id },
          {
            $push: { following: checkRequestFl.user._id }
          },
          { new: true }
        ).populate("followers following", "-password");

        await RequestFl.findOneAndDelete({
          _id: requestId
        });

        return res.json({
          msg: `${response ? "Đồng ý" : "Từ chối"} yêu cầu theo dõi của ${
            checkRequestFl.author.username
          }`,
          requestFl,
          newUser,
          updateUser
        });
      } else {
        await RequestFl.findOneAndDelete({
          _id: requestId
        });

        return res.json({
          msg: `${response ? "Đồng ý" : "Từ chối"} yêu cầu theo dõi của ${
            checkRequestFl.author.username
          }`,
          requestFl
        });
      }
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  removeRequestFl: async (req, res) => {
    try {
      const checkRequestFl = await RequestFl.findById(req.params.id);

      if (!checkRequestFl)
        return res.status(404).json({ msg: "Yêu cầu không tồn tại !" });

      const requestFl = await RequestFl.findOneAndDelete({
        _id: req.params.id
      });

      return res.json({ msg: "Xóa yêu cầu thành công !", requestFl });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  checkRequest: async (req, res) => {
    try {
      const checkRequestFl = await RequestFl.findOne({
        user: req.params.id,
        author: req.user._id
      }).populate("author user");
      if (checkRequestFl)
        return res.json({ msg: checkRequestFl.user._id, checkRequestFl });
      else return res.json({ msg: "" });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  }
};

module.exports = requestFlCtrl;
