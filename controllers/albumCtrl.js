const Albums = require("../models/albumModel");
const Comments = require("../models/commentModel");
const Users = require("../models/userModel");
class APIfeatures {
  constructor(query, queryString) {
    this.query = query;
    this.queryString = queryString;
  }

  paginating() {
    const page = this.queryString.page * 1 || 1;
    const limit = this.queryString.limit * 1 || 9;
    const skip = (page - 1) * limit;
    this.query = this.query.skip(skip).limit(limit);
    return this;
  }
}

const albumCtrl = {
  createAlbum: async (req, res) => {
    try {
      const { name, images, visibility, friend } = req.body;

      if (!name)
        return res.status(400).json({ msg: "Vui lòng điền tên album." });
      if (!visibility)
        return res.status(400).json({ msg: "Vui lòng chọn chế độ đăng." });

      if (images.length === 0)
        return res.status(400).json({ msg: "Vui lòng chọn ảnh ." });

      if (friend && friend.length > 0) {
        const newAlbum = new Albums({
          name,
          images,
          visibility,
          friend,
          user: req.user._id
        });
        await newAlbum.save();
        res.json({
          msg: "Đã tạo album với chế độ bạn bè !",
          newAlbum: {
            ...newAlbum._doc,
            user: req.user
          }
        });
      } else {
        const newAlbum = new Albums({
          name,
          images,
          visibility,
          user: req.user._id
        });
        await newAlbum.save();
        res.json({
          msg: "Đã tạo album  !",
          newAlbum: {
            ...newAlbum._doc,
            user: req.user
          }
        });
      }
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getAlbums: async (req, res) => {
    try {
      const features = new APIfeatures(
        Albums.find({
          user: [...req.user.following, req.user._id]
        }),
        req.query
      ).paginating();

      const albums = await features.query
        .sort("-updatedAt")
        .populate(
          "user likes",
          "avatar username fullname followers story email mobile website followers following"
        )
        .populate({
          path: "comments",
          populate: {
            path: "user likes",
            select: "-password"
          }
        });

      res.json({
        msg: "Lấy danh sách album thành công!",
        result: albums.length,
        albums
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  updateAlbum: async (req, res) => {
    try {
      const { name, images, visibility, friend } = req.body;

      const album = await Albums.findOneAndUpdate(
        { _id: req.params.id },
        { name, images, visibility, friend }
      )
        .populate(
          "user likes",
          "avatar username fullname followers story email mobile website followers following"
        )
        .populate({
          path: "comments",
          populate: {
            path: "user likes",
            select: "-password"
          }
        });
      res.json({
        msg: "Chỉnh sửa album thành công!",
        newAlbum: { ...album._doc, name, visibility, friend, images }
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  likeAlbum: async (req, res) => {
    try {
      const album = await Albums.find({
        _id: req.params.id,
        likes: req.user._id
      });
      if (album.length > 0)
        return res.status(400).json({ msg: "Bạn đã thích album này rồi" });

      const like = await Albums.findOneAndUpdate(
        { _id: req.params.id },
        { $push: { likes: req.user._id } },
        { new: true }
      );

      if (!like)
        return res.status(400).json({ msg: "Album này không tồn tại." });

      res.json({ msg: "Thích album." });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  unLikeAlbum: async (req, res) => {
    try {
      const like = await Albums.findOneAndUpdate(
        { _id: req.params.id },
        { $pull: { likes: req.user._id } },
        { new: true }
      );

      if (!like)
        return res.status(400).json({ msg: "Album này không tồn tại." });

      res.json({ msg: "Đã hủy thích album" });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getUserAlbum: async (req, res) => {
    try {
      const public = "public";

      const features = new APIfeatures(
        Albums.find({
          user: req.params.id,
          $or: [
            { visibility: public },
            { friend: req.user._id },
            { user: req.user._id }
          ]
        }),
        req.query
      ).paginating();

      const albums = await features.query
        .sort("-createdAt")
        .populate("user likes friend");

      // const newAlbums = albums.filter(
      //   (album) =>
      //     album.visibility === "public" ||
      //     album.user === req.user._id ||
      //     album.friend.includes(req.user._id)
      // );
      res.json({ albums, result: albums.length });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getAlbum: async (req, res) => {
    try {
      const album = await Albums.findById(req.params.id)
        .populate(
          "user likes friend",
          "avatar username fullname followers story email mobile website followers following"
        )
        .populate({
          path: "comments",
          populate: {
            path: "user likes",
            select: "-password"
          }
        });

      if (!album)
        return res.status(400).json({ msg: "Album này không tồn tại." });

      res.json({
        album
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },

  deleteAlbum: async (req, res) => {
    try {
      const album = await Albums.findOneAndDelete({
        _id: req.params.id,
        user: req.user._id
      });
      if (album.comments.length > 0)
        await Comments.deleteMany({ _id: { $in: album.comments } });
      res.json({
        msg: "Xóa album thành công",
        newAlbum: {
          ...album,
          user: req.user
        }
      });
    } catch (er) {
      return res.status(500).json({ msg: er.message });
    }
  }
};

module.exports = albumCtrl;
