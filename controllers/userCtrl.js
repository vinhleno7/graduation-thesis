const Users = require("../models/userModel");
const Posts = require("../models/postModel");
const Albums = require("../models/albumModel");
const Comments = require("../models/commentModel");
const Stories = require("../models/storyModel");
const Notifies = require("../models/notifyModel");
const bcrypt = require("bcrypt");
const { distinct } = require("../models/userModel");

const userCtrl = {
  searchUser: async (req, res) => {
    try {
      const users = await Users.find({
        // username: { $regex: req.query.username }
        $or: [
          {
            username: {
              $regex: req.query.username,
              $options: "i"
            }
          },
          { fullname: { $regex: req.query.username, $options: "i" } },
          { address: { $regex: req.query.username, $options: "i" } },
          { mobile: { $regex: req.query.username } },
          { email: { $regex: req.query.username } }
        ]
      })
        .limit(10)
        .select("fullname username avatar");

      res.json({ users });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getUser: async (req, res) => {
    try {
      const user = await Users.findById(req.params.id)
        .select("-password")
        .populate("followers following stories", "-password");
      if (!user)
        return res.status(400).json({ msg: "Người dùng không tồn tại." });

      res.json({ user });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getAllUsers: async (req, res) => {
    try {
      const users = await Users.find()
        .select("-password")
        .populate("followers following stories", "-password");
      if (!users)
        return res.status(400).json({ msg: "Người dùng không tồn tại." });

      res.json({ msg: "Success!", result: users.length, users });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  updateUser: async (req, res) => {
    try {
      const {
        _id,
        role,
        avatar,
        username,
        fullname,
        mobile,
        address,
        story,
        website,
        password,
        gender
      } = req.body;
      if (!username)
        return res.status(400).json({ msg: "Vui lòng điền tên tài khoản." });
      let newUserName = username.toLowerCase().replace(/ /g, "");

      const user_name = await Users.find({ username: newUserName });

      if (user_name.length > 1)
        return res.status(400).json({ msg: "Tên tài khoản đã tồn tại." });
      if (!fullname)
        return res.status(400).json({ msg: "Vui lòng điền họ và tên." });

      if (password) {
        if (password.length < 6)
          return res.status(400).json({ msg: "Mật khẩu phải trên 6 ký tự." });

        const passwordHash = await bcrypt.hash(password, 12);

        await Users.findOneAndUpdate(
          { _id },
          {
            role,
            avatar,
            username,
            fullname,
            mobile,
            address,
            story,
            website,
            gender,
            password: passwordHash
          }
        );
      } else {
        await Users.findOneAndUpdate(
          { _id },
          {
            role,
            avatar,
            username,
            fullname,
            mobile,
            address,
            story,
            website,
            gender
          }
        );
      }

      res.json({ msg: "Cập nhật thành công !" });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  deleteUser: async (req, res) => {
    try {
      const checkUser = await Users.findOne({
        _id: req.params.id
      });

      await Users.findOneAndDelete({
        _id: req.params.id
      });
      await Users.findOneAndUpdate(
        { followers: req.params.id },
        { $pull: { followers: req.params.id } },
        { new: true }
      );
      await Users.findOneAndUpdate(
        { following: req.params.id },
        { $pull: { following: req.params.id } },
        { new: true }
      );

      await Posts.deleteMany({ user: req.params.id });
      await Posts.findOneAndUpdate(
        { likes: req.params.id },
        { $pull: { likes: req.params.id } },
        { new: true }
      );
      await Albums.deleteMany({ user: req.params.id });
      await Albums.findOneAndUpdate(
        { likes: req.params.id },
        { $pull: { likes: req.params.id } },
        { new: true }
      );

      const checkCmt = await Comments.find({ user: req.params.id });

      let myArray = [];
      checkCmt.forEach((cmt) => myArray.push(cmt._id));

      myArray.forEach(async (cmtId) => {
        await Posts.findOneAndUpdate(
          { comments: cmtId },
          { $pull: { comments: cmtId } },
          { new: true }
        );
      });

      await Comments.deleteMany({ user: req.params.id });
      await Comments.findOneAndUpdate(
        { likes: [req.params.id] },
        { $pull: { likes: req.params.id } },
        { new: true }
      );
      await Stories.deleteMany({ user: req.params.id });
      await Stories.findOneAndUpdate(
        { likes: req.params.id },
        { $pull: { likes: req.params.id } },
        { new: true }
      );
      await Notifies.deleteMany({ user: req.params.id });

      // await Notifies.findOneAndUpdate(
      //   { recipients: req.params.id },
      //   { $pull: { recipients: req.params.id } },
      //   { new: true }
      // );

      res.json({
        msg: `Đã xóa người dùng ${checkUser.username} `
      });
    } catch (er) {
      return res.status(500).json({ msg: er.message });
    }
  },
  follow: async (req, res) => {
    try {
      const user = await Users.find({
        _id: req.params.id,
        followers: req.user._id
      });
      if (user.length > 0)
        return res.status(500).json({ msg: "Bạn đã theo dõi người này." });

      const newUser = await Users.findOneAndUpdate(
        { _id: req.params.id },
        {
          $push: { followers: req.user._id }
        },
        { new: true }
      ).populate("followers following", "-password");

      await Users.findOneAndUpdate(
        { _id: req.user._id },
        {
          $push: { following: req.params.id }
        },
        { new: true }
      );

      res.json({ newUser });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  unfollow: async (req, res) => {
    try {
      const updateUser = await Users.findOneAndUpdate(
        { _id: req.params.id },
        {
          $pull: { followers: req.user._id }
        },
        { new: true }
      ).populate("followers following", "-password");

      const newUser = await Users.findOneAndUpdate(
        { _id: req.user._id },
        {
          $pull: { following: req.params.id }
        },
        { new: true }
      ).populate("followers following", "-password");

      res.json({ newUser, updateUser });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  suggestionsUser: async (req, res) => {
    try {
      const newArr = [...req.user.following, req.user._id];

      const num = req.query.num || 5;

      const otherUsers = await Users.find({ _id: { $nin: newArr } }).populate(
        "hobbies"
      );

      let newObj = {};
      let newArrHobby = [];
      for (let us of otherUsers) {
        if (us.hobbies.length > 0) {
          const sameHobbies = us.hobbies.filter((a) =>
            req.user.hobbies.map((b) => b._id).includes(a._id)
          );
          if (sameHobbies.length > 0) {
            newObj = {
              ...us._doc,
              sameHobbies,
              countSameHobbies: sameHobbies.length
            };
            newArrHobby.push(newObj);
          }
        }
      }

      // let objUser = new Object();
      // let newArrKNN = [];

      // if (req.user.following.length === 0) {
      //   newArrKNN = await Users.aggregate([
      //     { $match: { _id: { $nin: newArr } } },
      //     { $sample: { size: Number(num) } },
      //     {
      //       $lookup: {
      //         from: "users",
      //         localField: "followers",
      //         foreignField: "_id",
      //         as: "followers"
      //       }
      //     },
      //     {
      //       $lookup: {
      //         from: "users",
      //         localField: "following",
      //         foreignField: "_id",
      //         as: "following"
      //       }
      //     }
      //   ]).project("-password");
      // } else {
      //   for (let userFollowing of req.user.following) {
      //     let KNNOfFollowing = await Users.find({
      //       followers: userFollowing,
      //       _id: { $nin: newArr }
      //     });

      //     let KNNOfFollowers = await Users.find({
      //       following: userFollowing,
      //       _id: { $nin: newArr }
      //     });

      //     if (KNNOfFollowing.length > 0) {
      //       for (let us of KNNOfFollowing) {
      //         objUser = {
      //           ...us._doc,
      //           originFollowing: await Users.findOne({ _id: userFollowing })
      //         };
      //         newArrKNN.push(objUser);
      //       }
      //     }
      //     if (KNNOfFollowers.length > 0) {
      //       for (let us of KNNOfFollowers) {
      //         objUser = {
      //           ...us._doc,
      //           originFollower: await Users.findOne({ _id: userFollowing })
      //         };
      //         newArrKNN.push(objUser);
      //       }
      //     }
      //   }
      // }

      // const users = await Users.aggregate([
      //   { $match: { _id: { $nin: newArr } } },
      //   { $sample: { size: Number(num) } },
      //   {
      //     $lookup: {
      //       from: "users",
      //       localField: "followers",
      //       foreignField: "_id",
      //       as: "followers"
      //     }
      //   },
      //   {
      //     $lookup: {
      //       from: "users",
      //       localField: "following",
      //       foreignField: "_id",
      //       as: "following"
      //     }
      //   }
      // ]).project("-password");

      return res.json({
        // users,
        // newArrKNN: newArrKNN.sort(() => Math.random() - 0.75),
        newArrHobby: newArrHobby.sort(() => Math.random() - 0.75),
        result: newArrHobby.length
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  }
};

module.exports = userCtrl;
