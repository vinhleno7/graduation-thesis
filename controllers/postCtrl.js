const Posts = require("../models/postModel");
const Comments = require("../models/commentModel");
const Users = require("../models/userModel");
class APIfeatures {
  constructor(query, queryString) {
    this.query = query;
    this.queryString = queryString;
  }

  paginating() {
    const page = this.queryString.page * 1 || 1;
    const limit = this.queryString.limit * 1 || 9;
    const skip = (page - 1) * limit;
    this.query = this.query.skip(skip).limit(limit);
    return this;
  }
}

const postCtrl = {
  createPost: async (req, res) => {
    try {
      const { content, images } = req.body;

      if (!content)
        return res.status(400).json({ msg: "Vui lòng điền tâm trạng của bạn" });

      if (images.length === 0)
        return res.status(400).json({ msg: "Vui lòng chọn ảnh" });

      const newPost = new Posts({
        content,
        images,
        user: req.user._id
      });
      await newPost.save();

      res.json({
        msg: "Tạo bài viết thành công !",
        newPost: {
          ...newPost._doc,
          user: req.user
        }
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  adminCreatePost: async (req, res) => {
    try {
      const { content, images, myUser } = req.body;

      if (!content)
        return res.status(400).json({ msg: "Vui lòng điền tâm trạng của bạn" });

      if (!myUser)
        return res.status(400).json({ msg: "Please fill your user." });

      if (images.length === 0)
        return res.status(400).json({ msg: "Vui lòng chọn ảnh" });

      const newPost = new Posts({
        content,
        images,
        user: myUser._id
      });
      await newPost.save();

      res.json({
        msg: "Đã tạo bài viết !",
        newPost: {
          ...newPost._doc,
          user: req.user
        }
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getPosts: async (req, res) => {
    try {
      const features = new APIfeatures(
        Posts.find({
          user: [...req.user.following, req.user._id]
        }),
        req.query
      ).paginating();

      const posts = await features.query
        .sort("-updatedAt")
        .populate(
          "user likes",
          "avatar username fullname followers story email mobile website followers following"
        )
        .populate({
          path: "comments",
          populate: {
            path: "user likes",
            select: "-password"
          }
        });

      res.json({ msg: "Lấy tất cả bài viết!", result: posts.length, posts });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getAllPosts: async (req, res) => {
    try {
      const posts = await Posts.find()
        .sort("-updatedAt")
        .populate(
          "user likes",
          "avatar username fullname followers story email mobile website followers following"
        )
        .populate({
          path: "comments",
          populate: {
            path: "user likes",
            select: "-password"
          }
        });

      res.json({ msg: "Thành công.!", result: posts.length, posts });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  updatePost: async (req, res) => {
    try {
      const { content, images } = req.body;
      const post = await Posts.findOneAndUpdate(
        { _id: req.params.id },
        { content, images }
      )
        .populate(
          "user likes",
          "avatar username fullname followers story email mobile website followers following"
        )
        .populate({
          path: "comments",
          populate: {
            path: "user likes",
            select: "-password"
          }
        });
      res.json({
        msg: "Cập nhật bài viết thành công!",
        newPost: { ...post._doc, content, images }
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  likePost: async (req, res) => {
    try {
      const post = await Posts.find({
        _id: req.params.id,
        likes: req.user._id
      });
      if (post.length > 0)
        return res.status(400).json({ msg: "Bạn đã thích bài viết này" });

      const like = await Posts.findOneAndUpdate(
        { _id: req.params.id },
        { $push: { likes: req.user._id } },
        { new: true }
      );

      if (!like)
        return res.status(400).json({ msg: "Bài viết này không tồn tại" });

      res.json({ msg: "Đã thích bài viết này" });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  unLikePost: async (req, res) => {
    try {
      const like = await Posts.findOneAndUpdate(
        { _id: req.params.id },
        { $pull: { likes: req.user._id } },
        { new: true }
      );

      if (!like)
        return res.status(400).json({ msg: "Bài viết này không tồn tại." });

      res.json({ msg: "Đã hủy thích bài viết" });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getUserPosts: async (req, res) => {
    try {
      const features = new APIfeatures(
        Posts.find({ user: req.params.id }),
        req.query
      ).paginating();

      const posts = await features.query.sort("-createdAt");

      res.json({ posts, result: posts.length });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getPost: async (req, res) => {
    try {
      const post = await Posts.findById(req.params.id)
        .populate(
          "user likes",
          "avatar username fullname followers story email mobile website followers following"
        )
        .populate({
          path: "comments",
          populate: {
            path: "user likes",
            select: "-password"
          }
        });

      if (!post)
        return res.status(400).json({ msg: "Bài viết này không tồn tại." });

      res.json({
        post
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getPostsDiscover: async (req, res) => {
    try {
      const newArr = [...req.user.following, req.user._id];
      const postsTemp = await Posts.aggregate([
        { $match: { user: { $nin: newArr } } }
      ]);

      const features = new APIfeatures(
        Posts.aggregate([
          { $match: { user: { $nin: newArr } } }
          // { $sample: { size: Number(postsTemp.length) } }
        ]).sort("-createdAt"),
        req.query
      ).paginating();

      const posts = await features.query;
      // const posts = await features.query.sort("-createdAt");

      // const posts =  features

      // const num = req.query.num || 9;

      // const posts = await Posts.aggregate([
      //   { $match: { user: { $nin: newArr } } },
      //   { $sample: { size: Number(postsTemp.length) } }
      // ]);

      return res.json({
        msg: "Success!",
        result: posts.length,
        posts
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  deletePost: async (req, res) => {
    try {
      const post = await Posts.findOneAndDelete({
        _id: req.params.id,
        user: req.user._id
      });

      await Comments.deleteMany({ _id: { $in: post.comments } });
      res.json({
        msg: "Xóa bài viết thành công",
        newPost: {
          ...post,
          user: req.user
        }
      });
    } catch (er) {
      return res.status(500).json({ msg: er.message });
    }
  },
  deletePostAdmin: async (req, res) => {
    try {
      const post = await Posts.findOneAndDelete({
        _id: req.params.id
      });

      await Comments.deleteMany({ _id: { $in: post.comments } });
      res.json({
        msg: "ADMIN-Xóa bài viết thành công",
        newPost: {
          ...post
        }
      });
    } catch (er) {
      return res.status(500).json({ msg: er.message });
    }
  },
  savePost: async (req, res) => {
    try {
      const user = await Users.find({
        _id: req.user._id,
        saved: req.params.id
      });
      if (user.length > 0)
        return res.status(400).json({ msg: "Đã lưu bài viết này." });

      const save = await Users.findOneAndUpdate(
        { _id: req.user._id },
        {
          $push: { saved: req.params.id }
        },
        { new: true }
      );

      if (!save)
        return res.status(400).json({ msg: "Người dùng không tồn tại." });

      res.json({ msg: "Đã lưu bài viết này!" });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  unSavePost: async (req, res) => {
    try {
      const save = await Users.findOneAndUpdate(
        { _id: req.user._id },
        {
          $pull: { saved: req.params.id }
        },
        { new: true }
      );

      if (!save)
        return res.status(400).json({ msg: "Người dùng không tồn tại." });

      res.json({ msg: "Hủy lưu bài viết!" });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getSavePosts: async (req, res) => {
    try {
      const features = new APIfeatures(
        Posts.find({
          _id: { $in: req.user.saved }
        }),
        req.query
      ).paginating();

      const savePosts = await features.query.sort("-createdAt");

      res.json({
        savePosts,
        result: savePosts.length
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  }
};

module.exports = postCtrl;
