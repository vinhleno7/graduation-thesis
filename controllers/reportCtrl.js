const Reports = require("../models/reportPost");
const Users = require("../models/userModel");
const Posts = require("../models/postModel");

const reportCtrl = {
  create: async (req, res) => {
    try {
      const { reason, postId } = req.body;

      if (!reason) return res.status(400).json({ msg: "Vui lòng điền lý do." });

      if (!postId)
        return res
          .status(400)
          .json({ msg: "Vui lòng chọn bài viết để báo cáo" });

      const post = await Posts.find({
        _id: postId
      });

      if (!post)
        return res.status(500).json({ msg: "Bài viết này không tồn tại." });

      const newReport = new Reports({
        reason,
        post: postId,
        author: req.user._id
      });
      await newReport.save();

      res.json({
        msg: `Đã gửi báo cáo của bạn đến người quản lý. Hãy đợi phản hồi từ người quản lý.`,
        newReport
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  response: async (req, res) => {
    try {
      const { reportId, response } = req.body;

      if (!reportId)
        return res.status(400).json({ msg: "Vui lòng chọn báo cáo để xử lý" });

      const checkReport = await Reports.findById(reportId)
        .populate("post author")
        .populate({
          path: "post",
          populate: {
            path: "user likes",
            select: "-password"
          }
        });
      if (!checkReport)
        return res.status(404).json({ msg: "Báo cáo không tồn tại !" });

      await Reports.findOneAndUpdate(
        {
          _id: reportId
        },
        { status: response }
      );

      if (response) {
        const updatePost = await Posts.findOneAndDelete({
          _id: checkReport.post._id
        }).populate("user");
        await Reports.findOneAndDelete({
          _id: reportId
        });
        return res.json({
          msg: `${response ? "Đồng ý" : "Từ chối"} báo cáo của ${
            checkReport.author.username
          }`,
          updatePost,
          authorReport: checkReport.author
        });
      } else {
        return res.json({
          msg: `${response ? "Đồng ý" : "Từ chối"} báo cáo của ${
            checkReport.author.username
          }`,
          authorReport: checkReport.author
        });
      }
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getAll: async (req, res) => {
    try {
      const reports = await Reports.find()
        .populate("post author")
        .populate({
          path: "post",
          populate: {
            path: "user likes",
            select: "-password"
          }
        });

      return res.json({ reports });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  }
};

module.exports = reportCtrl;
