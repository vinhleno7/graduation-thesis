const Comments = require("../models/commentModel");
const Posts = require("../models/postModel");

const commentCtrl = {
  getAllComments: async (req, res) => {
    try {
      const comments = await Comments.find()
        .sort("-updatedAt")
        .populate("user likes tag postId postUserId reply")
        .populate({
          path: "reply postId",
          populate: {
            path: "user postUserId postId",
            select: "-password"
          }
        })
        .populate({
          path: "postId",
          populate: {
            path: "comments",
            populate: {
              path: "user postId"
            }
          }
        });

      res.json({
        msg: "Nhận xét thành công!",
        result: comments.length,
        comments
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  createComment: async (req, res) => {
    try {
      const { postId, content, tag, reply, postUserId } = req.body;

      const post = await Posts.findById(postId);
      if (!post)
        return res.status(400).json({ msg: "Bài viết này không tồn tại. " });
      if (reply) {
        const cm = await Comments.findById(reply);
        if (!cm)
          return res
            .status(400)
            .json({ msg: "Lời nhận xét này không tồn tại." });
      }

      const newComment = new Comments({
        user: req.user._id,
        content,
        tag,
        reply,
        postUserId,
        postId
      });

      await Posts.findOneAndUpdate(
        {
          _id: postId
        },
        {
          $push: { comments: newComment._id }
        },
        { new: true }
      );

      await newComment.save();

      res.json({ newComment });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  createCommentByAdmin: async (req, res) => {
    try {
      const { postId, content, user, tag, reply, postUserId } = req.body;

      const post = await Posts.findById(postId);
      if (!post)
        return res.status(400).json({ msg: "Bài viết này không tồn tại. " });
      if (reply) {
        const cm = await Comments.findById(reply);
        if (!cm)
          return res
            .status(400)
            .json({ msg: "Lời nhận xét này không tồn tại. " });
      }

      const newComment = new Comments({
        user: user._id,
        content,
        tag,
        reply,
        postUserId,
        postId
      });

      await Posts.findOneAndUpdate(
        {
          _id: postId
        },
        {
          $push: { comments: newComment._id }
        },
        { new: true }
      );

      await newComment.save();

      res.json({ newComment });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  updateComment: async (req, res) => {
    try {
      const { content } = req.body;
      const checkCmt = await Comments.findOne({ _id: req.params.id });
      if (!checkCmt) {
        return res
          .status(400)
          .json({ msg: "Lời nhận xét này không tồn tại. " });
      }
      if (req.user.role === "admin") {
        await Comments.findOneAndUpdate(
          {
            _id: req.params.id
          },
          { content }
        );
        return res.json({
          msg: `Cập nhật thành công. Nội dung mới: [${content}] !`
        });
      }
      await Comments.findOneAndUpdate(
        {
          _id: req.params.id,
          user: req.user._id
        },
        { content }
      );

      res.json({ msg: `Cập nhật thành công. Nội dung mới: [${content}] !` });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  likeComment: async (req, res) => {
    try {
      const comment = await Comments.find({
        _id: req.params.id,
        likes: req.user._id
      });
      if (comment.length > 0)
        return res.status(400).json({ msg: "Bạn đã thích bài viết này." });

      await Comments.findOneAndUpdate(
        { _id: req.params.id },
        { $push: { likes: req.user._id } },
        { new: true }
      );
      res.json({ msg: "Đã thích lời nhận xét !" });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  unLikeComment: async (req, res) => {
    try {
      await Comments.findOneAndUpdate(
        { _id: req.params.id },
        { $pull: { likes: req.user._id } },
        { new: true }
      );
      res.json({ msg: "Đã hủy thích lời nhận xét" });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  deleteComment: async (req, res) => {
    try {
      if (req.user.role === "admin") {
        const comment = await Comments.findOneAndDelete({
          _id: req.params.id
        });

        await Posts.findOneAndUpdate(
          { _id: comment.postId },
          {
            $pull: { comments: req.params.id }
          }
        );

        return res.json({ msg: "Đã xóa lời nhận xét!" });
      }

      const comment = await Comments.findOneAndDelete({
        _id: req.params.id,
        $or: [{ user: req.user._id }, { postUserId: req.user._id }]
      });

      await Posts.findOneAndUpdate(
        { _id: comment.postId },
        {
          $pull: { comments: req.params.id }
        }
      );

      res.json({ msg: "Đã xóa lời nhận xét!" });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  }
};

module.exports = commentCtrl;
